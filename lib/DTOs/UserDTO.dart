class UserDTO {
   int userId;
   String firstName;
   String lastName;
   String email;
   String userName;
   String profileImage;
   int isSubscribedZero;
   int isSubscribedOne;
   int isSubscribedTwo;
   int isSubscribedThree;
   bool isPremium;

  UserDTO({this.userId, this.firstName, this.lastName, this.email,this.userName,this.profileImage, this.isSubscribedZero, this.isSubscribedOne, this.isSubscribedTwo, this.isSubscribedThree, this.isPremium});


  factory UserDTO.fromJson(Map<String, dynamic> json) {
    json['userId'] = int.parse(json['userId']);
    json['isSubscribedZero'] = int.parse(json['isSubscribedZero']);
    json['isSubscribedOne'] = int.parse(json['isSubscribedOne']);
    json['isSubscribedTwo'] = int.parse(json['isSubscribedTwo']);
    json['isSubscribedThree'] = int.parse(json['isSubscribedThree']);
    // json['isPremium'] = bool.parse(json['isSubscribedThree']);
    return UserDTO(
        userId: json['userId'],
        firstName: json['firstName'],
        lastName: json['lastName'],
        email: json['email'],
        userName: json['username'],
        profileImage: json['profileImage'],
        isSubscribedZero: json['isSubscribedZero'],
        isSubscribedOne: json['isSubscribedOne'],
        isSubscribedTwo: json['isSubscribedTwo'],
        isSubscribedThree: json['isSubscribedThree'],
        isPremium: json['isPremium']
    );
  }
}