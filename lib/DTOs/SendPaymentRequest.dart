class SendPaymentRequest{
  String CustomerName ;
  String NotificationOption ;
  int InvoiceValue ;
  String DisplayCurrencyIso;
  String CallBackUrl;
  String ErrorUrl;
  String MobileCountryCode;
  String CustomerMobile;
  String CustomerEmail;


  SendPaymentRequest(this.CustomerName, this.NotificationOption,
      this.InvoiceValue, this.DisplayCurrencyIso, this.CallBackUrl,
      this.ErrorUrl, this.MobileCountryCode, this.CustomerMobile,
      this.CustomerEmail);

  Map<String, dynamic> toJson() =>
      {
        'CustomerName': CustomerName.toString(),
        'NotificationOption': NotificationOption,
        'InvoiceValue': InvoiceValue,
        'DisplayCurrencyIso': DisplayCurrencyIso,
        'CallBackUrl': CallBackUrl,
        'ErrorUrl' :ErrorUrl,
        'MobileCountryCode':MobileCountryCode,
        'CustomerMobile': CustomerMobile,
        'CustomerEmail':CustomerEmail
      };
}