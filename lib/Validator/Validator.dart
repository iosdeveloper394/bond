import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:flutter/material.dart';
class Validator{
  var context;
  Validator(this.context);

  bool isFirstNameTrue(String fname)
  {

    if (fname.isEmpty) {

      setStatus("First_Name_Empty_String");
      return false;
    }
    if (fname.length<=2)
    {
      setStatus("First_Name_Short_String");
      return false;
    }

    if (fname.length>20)
    {
      setStatus("First_Name_Long_String");
      return false;
    }
    else return true;
  }

  bool isLastNameTrue(String lname)
  {

    if (lname.isEmpty) {

      setStatus("Last_Name_Empty_String");
      return false;
    }
    if (lname.length<=3)
    {
      setStatus("Last_Name_Short_String");
      return false;
    }
    if (lname.length>20)
    {
      setStatus("Last_Name_Long_String");
      return false;
    }

    else return true;
  }
  bool isUrlTrue(String s)
  {

    if (s.isEmpty) {

      setStatus("Url_Empty_String");
      return false;
    }
    if (s.length<=4)
    {
      setStatus("Url_Short_String");
      return false;
    }
    if (s.length>30)
    {
      setStatus("Url_Long_String");
      return false;
    }

    else return true;
  }

  bool isEmailTrue(String email)
  {
    print(email+'aksj');
    if (email.trim().isEmpty) {
      setStatus("Email_Empty_String");
      return false;
    }
    if (!RegExp(r"^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email))
    {
      setStatus("Email_Not_Correct_String");
      return false;
    }
    else return true;
  }

    bool isPasswordTrue(String s)
  {
    if (s.isEmpty) {

      setStatus("Password_Empty_String");
      return false;
    }
    if (s.length<8)
    {
      setStatus("Password_Short_String");
      return false;
    }
    else return true;
  }

  bool isUserNameTrue(String uname)
  {

    if (uname.isEmpty) {

      setStatus("User_Name_Empty_String");
      return false;
    }
    if (uname.length<=4)
    {
      setStatus("User_Name_Short_String");
      return false;
    }
    if (uname.contains(" "))
    {
      setStatus("Space_Contain_String");
      return false;
    }
    else return true;
  }

   void setStatus (msg)
  {
    Scaffold.of(context).showSnackBar(
        new SnackBar(content: new Text(AppLocalizations.of(context).translate(msg))));
  }



}

extension EmailValidator on String {
  bool isValidEmail() {
    return RegExp(
            r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
        .hasMatch(this);
  }
}