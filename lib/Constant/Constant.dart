class Constant{
  static String KEY = 'starlet';
  static String url = 'https://www.bondscode.com';

  static String route = '/bond/';

  static String login = route+'login.php';

  static String registration = route+'registration.php';
  static String generate_qr_link = route+'generate_qr_link.php';
  static String qr_url  = 'https://bondscode.com/bond/readqr/public/readqr?key=';
  static String get_all_user = route+'get_all_users.php';
  static String search_user = route+'search_user.php';
  static String get_user_profile = route+'get_user_profile.php';
  static String delete_user_friend = route+'delete_user_friend.php';
  static String add_friend_for_user = route+'add_friend_for_user.php';
  static String get_my_friends = route+'get_my_friends.php';
  static String get_my_qr = route+'get_my_qr.php';
  static String show_results = route+'show_results.php';
  static String update_profile = route+'update_profile.php';
  static String update_password = route+'update_password.php';
  static String get_qr_scanned_location = route+'get_qr_scanned_locations.php';
  static String delete_qr = route+ 'delete_qr.php';
  static String get_user_detail = route + 'get_user_info.php';
  static String forgot_password = route + 'forgot_password.php';
}

//?qrId=2&&hash=starlet