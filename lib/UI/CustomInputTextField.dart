import 'package:flutter/material.dart';
import 'CustomColors.dart';

class CustomInputTextField extends StatelessWidget{
  Icon fieldIcon;
  String hitText;
  bool isSecure = false ;
  TextInputType textInputType;
  var myController = TextEditingController();

 // CustomInputTextField(this.fieldIcon, this.hitText, this.isSecure, this.textInputType);
  CustomInputTextField(this.fieldIcon, this.hitText, this.isSecure, this.textInputType,this.myController);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width: MediaQuery.of(context).size.width-20,
      child: Material(
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
          elevation: 4.0,
          color:  Color(CustomColors().editTextBgColor()),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width-20,
                child: Padding(
                  padding: const EdgeInsets.only(left: 10),
                  child: TextFormField(
                    controller: myController,
                    obscureText: isSecure,
                    keyboardType:textInputType,
                    decoration: InputDecoration(
                        fillColor: Color(CustomColors().editTextBgColor()),
                        filled: true,
                        border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(10.0))),
                        hintText: hitText,
                        icon: fieldIcon,
                    ),
                    style: TextStyle(
                      fontSize: 20.0,
                      color: Color(CustomColors().mainColorHard()),
                    ),
                  ),
                ),
              )
            ],
          )
      ),
    );
  }

}