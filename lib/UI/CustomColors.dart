import 'package:flutter/material.dart';

final themeColor = Color(0xfff5a623);
final primaryColor = Color(0xff203152);
final greyColor = Color(0xffaeaeae);
final greyColor2 = Color(0xffE8E8E8);

class CustomColors extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return null;
  }

  mainColorHard(){
  return 0xff184449;
  }
  mainColorMedium(){
    return 0xff006766;
  }
  mainColorLow(){
    return 0xff00807f;
  }
  logoBgColor(){
    return 0xff174346;
  }
  editTextBgColor(){
    return 0xffeff7fb;
  }
  dimGreyColor(){
    return 0xffefefef;
  }
  dimWhiteColor(){
    return 0xfff9fafb;
  }
  redDim(){
    return 0xffea816f;
  }
  dinGreyBG(){
    return 0xffeeeeee;
  }
  lightGreyBG(){
    return 0xfff0f0f0;
  }

}