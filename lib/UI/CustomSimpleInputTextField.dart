import 'package:flutter/material.dart';
import 'CustomColors.dart';

class CustomSimpleInputTextField extends StatelessWidget{
  String hitText;
  bool isSecure = false ;
  TextInputType textInputType;
  var myController = TextEditingController();

  CustomSimpleInputTextField( this.hitText, this.isSecure, this.textInputType,this.myController);

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      width:  MediaQuery.of(context).size.width,
      child: Material(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                width:  MediaQuery.of(context).size.width,
                  child: Padding(
                    padding: const EdgeInsets.only(left: 20,right: 20,bottom:10),
                    child: TextField(
                      controller: myController,
                      obscureText: isSecure,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(borderRadius: BorderRadius.all(Radius.circular(8.0))),
                        fillColor: Color(CustomColors().dimGreyColor()),
                          filled: true,
                          hintText: hitText,
                      ),
                      keyboardType:textInputType,
                      style: TextStyle(
                        height: 0.3,
                        fontSize: 14.0,
                        color: Color(CustomColors().mainColorHard()),
                    ),
                ),
                  ),
              )
            ],
          )
      ),
    );
  }

}