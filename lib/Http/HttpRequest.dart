import 'dart:convert';
import 'dart:io';

import 'package:http/http.dart' as http;
import 'dart:async';

class HttpRequest {
  Future<http.Response> fetchPost(route,params) async {
    try{
      var uri = Uri.http('www.bondscode.com', route, params);
      print(uri);
//    http.Response response = await http.get(uri,headers: );
      http.Response response = await http.get(uri).timeout(const Duration(seconds: 120));
  //  print(response.body);
    return response;}
    catch(e){
      print(e);
      throw('Internet Failed');
    }
  }
  Future<HttpClientResponse> paymentRequest(route,jsonMap) async {
    try {
    HttpClient httpClient = new HttpClient();
    HttpClientRequest request = await httpClient.postUrl(Uri.parse(route));
    print(route);
    print(jsonMap);
    request.headers.set('content-type', 'application/json');
    request.headers.set('Authorization', 'bearer rLtt6JWvbUHDDhsZnfpAhpYk4dxYDQkbcPTyGaKp2TYqQgG7FGZ5Th_WD53Oq8Ebz6A53njUoo1w3pjU1D4vs_ZMqFiz_j0urb_BH9Oq9VZoKFoJEDAbRZepGcQanImyYrry7Kt6MnMdgfG5jn4HngWoRdKduNNyP4kzcp3mRv7x00ahkm9LAK7ZRieg7k1PDAnBIOG3EyVSJ5kK4WLMvYr7sCwHbHcu4A5WwelxYK0GMJy37bNAarSJDFQsJ2ZvJjvMDmfWwDVFEVe_5tOomfVNt6bOg9mexbGjMrnHBnKnZR1vQbBtQieDlQepzTZMuQrSuKn-t5XZM7V6fCW7oP-uXGX-sMOajeX65JOf6XVpk29DP6ro8WTAflCDANC193yof8-f5_EYY-3hXhJj7RBXmizDpneEQDSaSz5sFk0sV5qPcARJ9zGG73vuGFyenjPPmtDtXtpx35A-BVcOSBYVIWe9kndG3nclfefjKEuZ3m4jL9Gg1h2JBvmXSMYiZtp9MR5I6pvbvylU_PP5xJFSjVTIz7IQSjcVGO41npnwIxRXNRxFOdIUHn0tjQ-7LwvEcTXyPsHXcMD8WtgBh-wxR8aKX7WPSsT1O8d8reb2aR7K3rkV3K82K_0OgawImEpwSvp9MNKynEAJQS6ZHe_J_l77652xwPNxMRTMASk1ZsJL');

    request.add(utf8.encode(jsonMap));
    HttpClientResponse response = await request.close();
    request.close();
    return response;}
    catch (e){
      throw('Internet Failed');
    }
  }

}