 import 'package:flutter/material.dart';
import 'Activity/LoginScreen.dart';
import 'Localization/app_localizations.dart';
import 'SessionManager/SessionManager.dart';
import 'Activity/DashboardScreen.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() {
  Locale myLocale;
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) async {
    if (await SessionManager().CheckIfSessionExist()) {
      runApp(MaterialApp(
        initialRoute: '/',
        routes: {
          '/dashboard': (context) => DashboardScreen(),
        },
        title: "BOND",

        // List all of the app's supported locales here
        supportedLocales: [
          Locale('en', ''),
          Locale('ar', ''),
        ],
        // These delegates make sure that the localization data for the proper language is loaded
        localizationsDelegates: [
          // A class which loads the translations from JSON files
          AppLocalizations.delegate,
          // Built-in localization of basic text for Material widgets
          GlobalMaterialLocalizations.delegate,
          // Built-in localization for text direction LTR/RTL
          GlobalWidgetsLocalizations.delegate,
        ],
        // Returns a locale which will be used by the app
        localeResolutionCallback: (locale, supportedLocales) {
          // Check if the current device locale is supported
          // for (var supportedLocale in supportedLocales) {
          //   if (supportedLocale.languageCode == locale.languageCode) {
          //     print(supportedLocale.countryCode + locale.countryCode);
          //     return supportedLocale;
          //   }
          // }
          // If the locale of the device is not supported, use the first one
          // from the list (English, in this case).
          return supportedLocales.first;
        },

        home: DashboardScreen(),
      ));
    } else {
      runApp(MaterialApp(
        debugShowCheckedModeBanner: false,
        title: "BOND",

        // List all of the app's supported locales here
        supportedLocales: [
          Locale('en', ''),
          Locale('ar', ''),
        ],
        // These delegates make sure that the localization data for the proper language is loaded
        localizationsDelegates: [
          // A class which loads the translations from JSON files
          AppLocalizations.delegate,
          // Built-in localization of basic text for Material widgets
          GlobalMaterialLocalizations.delegate,
          // Built-in localization for text direction LTR/RTL
          GlobalWidgetsLocalizations.delegate,
        ],
        // Returns a locale which will be used by the app
        localeResolutionCallback: (locale, supportedLocales) {
          // Check if the current device locale is supported
          // for (var supportedLocale in supportedLocales) {
          //   if (supportedLocale.languageCode == locale.languageCode) {
          //     print(supportedLocale.countryCode + locale.countryCode);
          //     return supportedLocale;
          //   }
          // }
          // If the locale of the device is not supported, use the first one
          // from the list (English, in this case).
          return supportedLocales.first;
        },

        home: LoginScreen(),
      ));
    }
  });
}