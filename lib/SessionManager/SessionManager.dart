
import 'package:shared_preferences/shared_preferences.dart';

class SessionManager{

  int _userId, _isSubscribedZero, _isSubscribedOne, _isSubscribedTwo, _isSubscribedThree;
  String _firstName,_lastName,_userName,_email,_userImage;
  bool _isPremium;

  getUserInfo() async
  {
    final prefs = await SharedPreferences.getInstance();
    _userId = prefs.getInt('userId');
    _firstName= prefs.getString('firstName');
    _lastName = prefs.getString('lastName');
    _userName = prefs.getString('userName');
    _email = prefs.getString('email');
    _isSubscribedZero = prefs.getInt('isSubscribedZero');
    _isSubscribedOne = prefs.getInt('isSubscribedOne');
    _isSubscribedTwo = prefs.getInt('isSubscribedTwo');
    _isSubscribedThree = prefs.getInt('isSubscribedThree');
    _isPremium = prefs.getBool('isPremium');
  }

  setUserInfo(userId,firstName,lastName,userName,email, isSubscribedZero, isSubscribedOne, isSubscribedTwo, isSubscribedThree, isPremium) async
  {
    var prefs = await SharedPreferences.getInstance();
     prefs.setInt('userId',userId);
     prefs.setString('firstName',firstName);
     prefs.setString('lastName',lastName);
     prefs.setString('userName',userName);
     prefs.setString('email',email);
     prefs.setInt('isSubscribedZero', isSubscribedZero);
     prefs.setInt('isSubscribedOne', isSubscribedOne);
     prefs.setInt('isSubscribedTwo', isSubscribedTwo);
     prefs.setInt('isSubscribedThree', isSubscribedThree);
     prefs.setBool('isPremium', isPremium);
  }

  Logout() async{
    final prefs = await SharedPreferences.getInstance();
    await prefs.clear();
  }

  get isSubscribedThree async {
    return _isSubscribedThree;
  }

  set isSubscribedThree(value) {
    _isSubscribedThree = value;
  }

  get isSubscribedTwo async {
    return _isSubscribedTwo;
  }

  set isSubscribedTwo(value) {
    _isSubscribedTwo = value;
  }

  get isSubscribedOne async {
    return _isSubscribedOne;
  }

  set isSubscribedOne(value) {
    _isSubscribedOne = value;
  }

  get isSubscribedZero async {
    return _isSubscribedZero;
  }

  set isSubscribedZero(value) {
    _isSubscribedZero = value;
  }

  get email async {
    return _email;
  }

  set email(value) {
    _email = value;
  }

  get userName => _userName;

  set userName(value) {
    _userName = value;
  }

  get lastName => _lastName;

  set lastName(value) {
    _lastName = value;
  }

  String get firstName => _firstName;

  set firstName(String value) {
    _firstName = value;
  }

  int get userId {
    return _userId;
  }

  Future<int> getIsSubscribedThree () async {
    final prefs = await SharedPreferences.getInstance();
    var _isSubscribedThree = prefs.getInt('isSubscribedThree');
    return _isSubscribedThree;
  }

  setIsSubscribedThree(isSubscribedThree) async
  {
    var prefs = await SharedPreferences.getInstance();
    prefs.setInt('isSubscribedThree',isSubscribedThree);
  }

  Future<int> getIsSubscribedTwo () async {
    final prefs = await SharedPreferences.getInstance();
    var _isSubscribedTwo = prefs.getInt('isSubscribedTwo');
    return _isSubscribedTwo;
  }

  setIsSubscribedTwo(isSubscribedTwo) async
  {
    var prefs = await SharedPreferences.getInstance();
    prefs.setInt('isSubscribedTwo',isSubscribedTwo);
  }

  Future<int> getIsSubscribedOne () async {
    final prefs = await SharedPreferences.getInstance();
    var _isSubscribedOne = prefs.getInt('isSubscribedOne');
    print(_isSubscribedOne);
    return _isSubscribedOne;
  }

  setIsSubscribedOne(isSubscribedOne) async
  {
    var prefs = await SharedPreferences.getInstance();
    print(isSubscribedOne);
    prefs.setInt('isSubscribedOne',isSubscribedOne);
  }

  Future<int> getIsSubscribedZero () async {
    final prefs = await SharedPreferences.getInstance();
    var _isSubscribedOne = prefs.getInt('isSubscribedZero');
    return _isSubscribedOne;
  }

  setIsSubscribedZero(isSubscribedZero) async
  {
    var prefs = await SharedPreferences.getInstance();
    prefs.setInt('isSubscribedZero',isSubscribedZero);
  }

  Future<String> getUserImage () async {
    final prefs = await SharedPreferences.getInstance();
    var _userImage = prefs.getString('userImage');
    print(_userImage);
    return _userImage;
  }

  setUserImage(userImage) async
  {
    var prefs = await SharedPreferences.getInstance();
    prefs.setString('userImage',userImage);
  }

  Future<int> getUserId () async {
    final prefs = await SharedPreferences.getInstance();
    var _userId = prefs.getInt('userId');
    print(_userId);
    return _userId;
  }
  Future<String> getUserFullname () async {
    final prefs = await SharedPreferences.getInstance();
    var fullName = prefs.getString('firstName') + " "+ prefs.getString('lastName');
    print(fullName);
    return fullName;
  }
  Future<String> getUserEmail () async {
    final prefs = await SharedPreferences.getInstance();
    var _email = prefs.getString('email');
    print(_email);
    return _email;
  }

  Future<bool> getisPremium () async {
    final prefs = await SharedPreferences.getInstance();
    var _isPremium = prefs.getBool('isPremium');
    print(_isPremium);
    return _isPremium;
  }

  set userId(int value) {
    _userId = value;
  }

  Future<bool> CheckIfSessionExist() async{
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getInt('userId') !=null){
      return true;}
    else {
      return false;
    }
  }


}