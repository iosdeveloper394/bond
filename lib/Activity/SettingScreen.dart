import 'dart:collection';
import 'dart:convert';
import 'package:bond_flutter/Constant/Constant.dart';
import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:bond_flutter/SessionManager/SessionManager.dart';
import 'package:bond_flutter/UI/ProgressBar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:bond_flutter/Transitions/ScreenPageTransitions.dart';
import 'package:image_picker/image_picker.dart';
import '../UI/CustomColors.dart';
import '../Validator/Validator.dart';
import 'GenerateQRScreen.dart';
import '../UI/SnackBar.dart';
import '../Model/DataModel.dart';
import 'dart:io';
import 'package:path/path.dart' as Path;
import '../Http/HttpRequest.dart';
import 'dart:convert' as json;

class SettingScreen extends StatefulWidget{
  SettingState createState()=> SettingState();
}

class SettingState extends State<SettingScreen> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  bool isFile = false;
  File _image;
  String _uploadedFileURL = "No File Selected";
  Validator validator;
  final qrEmailController = TextEditingController();
  String profileImage ;
  String fullName = "";
  String email = "";
  int  userId = 0;
  String imageUrl = "";
  var password1 = TextEditingController();
  var password2 = TextEditingController();
  @override
  void initState() {
    super.initState();
    getUserdataFromCache();
  }


  getUserdataFromCache() async {
    fullName  = await SessionManager().getUserFullname();
    email = await SessionManager().getUserEmail();
    userId  = await SessionManager().getUserId();
    imageUrl = await SessionManager().getUserImage();
    setState(()  {
    });}
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        title: new Text(AppLocalizations.of(context).translate("Setting_String"),
          style: TextStyle(
              fontSize: 22
          ),),
        backgroundColor: Color(CustomColors().mainColorHard()),
      ),
      body:
      Builder(
        builder: (context)=> Center (
          child: Container(
            width: MediaQuery
                .of(context)
                .size
                .width,
            height: MediaQuery
                .of(context)
                .size
                .height,
            child: Material(
              child: Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: SingleChildScrollView(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: 400,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Image.asset(
                            'images/settings_icon.png',
                            width: 300,
                            height: 120,
                          ),
                        ),
//                        Container(
//                          decoration: BoxDecoration(
//                            border: Border.all(color: Color(CustomColors().dimGreyColor()), width: 2.0),
//                            color: Color(CustomColors().lightGreyBG()),
//                            borderRadius: BorderRadius.all(
//                              Radius.circular(10),
//                            ),
//                            boxShadow: <BoxShadow>[
//                              new BoxShadow(
//                                  blurRadius: 0.5
//                              ),
//                            ],
//                          ),
//
//                          height: 100,
//                          width: MediaQuery.of(context).size.width/1.1,
//                          child: Padding(
//                            padding: const EdgeInsets.all(8.0),
//                            child: TextField(
//                              keyboardType: TextInputType.emailAddress,
//                              maxLength: 40,
//                              maxLines: 1,
//                              controller: qrEmailController,
//                              decoration: InputDecoration(
//                                  border: InputBorder.none,
//                                  hintText: imageUrl
//                              ),
//                              style: TextStyle(
//                                fontSize: 12,
//                              ),
//                            ),
//                          ),
//                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: CachedNetworkImage(
                              imageUrl: "$imageUrl",
                              placeholder: (context, url) => CircularProgressIndicator(),
                              errorWidget: (context, url, error) => Icon(Icons.account_circle, size: 60.0,),
                              height: 150,
                              width: 150
                            ),
                          ),
                        ),
                        Container(
                          child: RaisedButton(child: Text(AppLocalizations.of(context).translate("Change_Profile_Image_String")),
                            onPressed: chooseFile,
                            color: Colors.white,
                            textColor: Colors.lightBlue,
                            padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                            splashColor: Colors.black,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Container(
                          child: Text(AppLocalizations.of(context).translate("Change_Password_String"), style: TextStyle(
                            fontSize: 29
                          ),)
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(color: Color(CustomColors().dimGreyColor()), width: 2.0),
                                color: Color(CustomColors().lightGreyBG()),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                boxShadow: <BoxShadow>[
                                  new BoxShadow(
                                      blurRadius: 0.5
                                  ),
                                ],
                              ),

                              height: 40,
                              width: MediaQuery.of(context).size.width/1.1,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: TextField(

                                  maxLines: 1,
                                  controller: password1,
                                  obscureText: true,
                                  decoration: InputDecoration(

                                      border: InputBorder.none,
                                      hintText: AppLocalizations.of(context).translate("New_Password_String")
                                  ),
                                  style: TextStyle(

                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(color: Color(CustomColors().dimGreyColor()), width: 2.0),
                                color: Color(CustomColors().lightGreyBG()),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                boxShadow: <BoxShadow>[
                                  new BoxShadow(
                                      blurRadius: 0.5
                                  ),
                                ],
                              ),

                              height: 40,
                              width: MediaQuery.of(context).size.width/1.1,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: TextField(

                                  maxLines: 1,
                                  controller: password2,
                                  obscureText: true,
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: AppLocalizations.of(context).translate("Confirm_Password_String")
                                  ),
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: InkWell(
                            onTap: () async {
                              validator = Validator(context);
                              if (!new Validator(context).isPasswordTrue(password1.text.toString()) )
                              {
                                return;
                              }

                              if (password1.text != password2.text)
                                {
                                  showSnackBar(context,AppLocalizations.of(context).translate("Confirm_Password_Not_Matched_String"));
                                  return;
                                }

                              progressShow(context);

                              try {
                                var fetchUserId = await SessionManager().getUserId();
                                var  userId = fetchUserId.toString();
                                var params = {
                                  'hash': Constant.KEY,
                                  'userId': userId,
                                  'password': password1.text
                                };

                                await HttpRequest().fetchPost(Constant.update_password,params).then((response) {

                                  print (response.statusCode);
                                  if (response.statusCode == 200) {
                                    try {
                                      print(response.body);
                                      setState(() {
                                        progressHide(context);
                                        password1.text = '';
                                        password2.text = '';
                                      });
                                      alertDialog();
                                    }
                                    catch (e){
                                      print(e);
                                      setState(() {
                                        progressHide(context);
                                        _scaffoldKey.currentState.showSnackBar(
                                            SnackBar(
                                              content: Text(AppLocalizations.of(context).translate("Failed_to_load_image_String")),
                                              duration: Duration(seconds: 3),
                                            ));
                                      });
                                    }
                                  }else {
                                    print(response.statusCode);
                                    progressHide(context);
                                    _scaffoldKey.currentState.showSnackBar(
                                        SnackBar(
                                          content: Text(AppLocalizations.of(context).translate("Failed_to_load_image_String")),
                                          duration: Duration(seconds: 3),
                                        ));
                                  }
                                });

                              }catch(e){
                                print(e);
                                progressHide(context);
                                _scaffoldKey.currentState.showSnackBar(
                                    SnackBar(
                                      content: Text(AppLocalizations.of(context).translate("Failed_to_load_image_String")),
                                      duration: Duration(seconds: 3),
                                    ));
                              }



                            },
                            child: Image.asset(
                              'images/generate_button_icon.png',
                              width: 50,
                              height: 50,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),),
        ),
      ),
    );
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return null;
  }
  Future chooseFile() async {
    await ImagePicker.pickImage(source: ImageSource.gallery).then((image) {
      setState(() {
        _image = image;
        _uploadedFileURL = Path.basename(_image.path);
        isFile = true;
        uploadFile();
      });
    });
  }

  Future uploadFile() async {
    progressShow(context);
    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child('uploads/${Path.basename(_image.path)}');
    StorageUploadTask uploadTask = storageReference.putFile(_image);
    await uploadTask.onComplete;
    print('File Uploaded');
    storageReference.getDownloadURL().then((fileURL) async {

      try {
        var fetchUserId = await SessionManager().getUserId();
        var  userId = fetchUserId.toString();
        var params = {
          'hash': Constant.KEY,
          'userId': userId,
          'imageUrl': fileURL.toString()
        };

        await HttpRequest().fetchPost(Constant.update_profile,params).then((response) {

          print (response.statusCode);
          if (response.statusCode == 200) {
            try {
              print(response.body);
              setState(() {
                setState(() {
                  _uploadedFileURL = fileURL;
                  SessionManager().setUserImage(fileURL);
                  this.imageUrl = fileURL;
                  progressHide(context);
                });

              });



            }
            catch (e){
              print(e);
              setState(() {
                progressHide(context);
                _scaffoldKey.currentState.showSnackBar(
                    SnackBar(
                      content: Text(AppLocalizations.of(context).translate("Failed_to_load_image_String")),
                      duration: Duration(seconds: 3),
                    ));
              });
            }
          }else {
            print(response.statusCode);
            progressHide(context);
            _scaffoldKey.currentState.showSnackBar(
                SnackBar(
                  content: Text(AppLocalizations.of(context).translate("Failed_to_load_image_String")),
                  duration: Duration(seconds: 3),
                ));
          }
        });

      }catch(e){
        print("My Errrror" +e);
        progressHide(context);
        _scaffoldKey.currentState.showSnackBar(
            SnackBar(
              content: Text(AppLocalizations.of(context).translate("Failed_to_load_image_String")),
              duration: Duration(seconds: 3),
            ));
      }


    });
  }

  void alertDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text("Password Changed successfully!"),
          content: new Text("Your password has been changed successfully"),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text("Okay"),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}