import 'dart:io';

import 'package:bond_flutter/Constant/Constant.dart';
import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:bond_flutter/SessionManager/SessionManager.dart';
import 'package:bond_flutter/UI/CustomColors.dart';
import 'package:bond_flutter/UI/SnackBar.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:webview_flutter/platform_interface.dart';
import 'package:webview_flutter/webview_flutter.dart';

class QrResultScreen extends StatefulWidget {
  String url;

  QrResultScreen(this.url);

  @override
  QrResultState createState() => new QrResultState(this.url);
}

class QrResultState extends State<QrResultScreen> {
  // InAppWebViewController webView;
  String url = "";
  double progress = 0;

  QrResultState(this.url);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // if (Platform.isAndroid) WebView.platform = SurfaceAndroidWebView();
    // print("ubaid $url");
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: new AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.keyboard_backspace,
              color: Colors.white,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          title: new Text(
            AppLocalizations.of(context).translate("QR_Result_String"),
            style: TextStyle(fontSize: 22),
          ),
          automaticallyImplyLeading: true,
          backgroundColor: Color(CustomColors().mainColorHard()),
        ),
        body: Container(
          child: Column(children: <Widget>[
            (progress != 1.0)
                ? LinearProgressIndicator(value: progress)
                : Container(),
            Expanded(
              child: InkWell(
                onTap: () {
                  print("My name is maaz: " + url);
                },
                child: Container(
                  margin: const EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.blueAccent)),
                  child: 
                  WebView(
                    // initialUrl: "https://flutter.dev",
                    initialUrl: geturl(url),
                    javascriptMode: JavascriptMode.unrestricted,
                    onPageStarted: (String url) {
                      print('Page started loading: $url');
                    },
                    onPageFinished: (String url) {
                      print('Page finished loading: $url');
                    },
                    onWebResourceError: (WebResourceError error) {
                      print('Page finished error: ${error.description}');
                    },
                  ),

                  // InAppWebView(
                  //   initialUrl: geturl(url),
                  //   initialHeaders: {},

                  //   onWebViewCreated: (InAppWebViewController controller) {
                  //     webView = controller;
                  //   },
                  //   onLoadStart: (InAppWebViewController controller, String url) {
                  //     print("started $url");
                  //   },
                  //   onProgressChanged: (InAppWebViewController controller, int progress) {
                  //     setState(() {
                  //       this.progress = progress/100;
                  //     });
                  //   },
                  //   onReceivedServerTrustAuthRequest:
                  //       (controller, challenge) async {
                  //     print(challenge);
                  //     return ServerTrustAuthResponse(
                  //         action: ServerTrustAuthResponseAction.PROCEED);
                  //   },
                  // ),
                ),
              ),
            ),
          ]
              // .where((Object o) => o != null).toList(),
              ),
        ),
      ),
    );
  }

  

  geturl(String url) {
    String concatedUrl = "";

    if (url.contains("bond")) {
      print("Jhakaaaaaaaaa " + url);
      return url;
    } else {
      concatedUrl = Constant.qr_url + url;
      print("Jhakaaaaaaaaa          " + concatedUrl);
      return concatedUrl;
    }
  }
}
