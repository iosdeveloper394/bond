import 'dart:convert';
import 'package:bond_flutter/Constant/Constant.dart';
import 'package:bond_flutter/DTOs/SendPaymentRequest.dart';
import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:bond_flutter/SessionManager/SessionManager.dart';
import 'package:bond_flutter/UI/CustomColors.dart';
import 'package:bond_flutter/UI/ProgressBar.dart';
import 'package:bond_flutter/UI/SnackBar.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../Http/HttpRequest.dart';
import 'PremiumScreen.dart';
import 'dart:io';

class SelectPackage extends StatefulWidget {
  @override
  State createState() => SelectPackageState();
}

class SelectPackageState extends State<SelectPackage> {
  int _radioValue1 = -1;
  String fullName = "";
  String email = "";
  int userId = 0;
  String profileImage = "";
  int payment = 0;
  int isSubscribedZero = 0,
      isSubscribedOne = 0,
      isSubscribedTwo = 0,
      isSubscribedThree = 0;
  bool _progressBarActive = true;

  String bullet = "\u2022";

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchUserDetail();
  }

  getUserdataFromCache() async {
    fullName = await SessionManager().getUserFullname();
    email = await SessionManager().getUserEmail();
    userId = await SessionManager().getUserId();
    profileImage = await SessionManager().getUserImage();

    int isSubscribedZero = await SessionManager().getIsSubscribedZero();
    int isSubscribedOne = await SessionManager().getIsSubscribedOne();
    int isSubscribedtwo = await SessionManager().getIsSubscribedTwo();
    int isSubscribedthree = await SessionManager().getIsSubscribedThree();

    print("One $isSubscribedOne         two $isSubscribedtwo             three $isSubscribedthree");

    setState(() {
      _progressBarActive = !_progressBarActive;
    });
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return new MaterialApp(
        home: new Scaffold(
            appBar: AppBar(
              leading: new IconButton(
                icon: new Icon(Icons.arrow_back, color: Colors.white),
                onPressed: () => Navigator.of(context).pop(),
              ),
              title: new Text('Continue To Payment'),
              centerTitle: true,
              backgroundColor: Color(CustomColors().mainColorHard()),
            ),
            body: _progressBarActive == true
                ? const Center(child: const CircularProgressIndicator())
                : new SingleChildScrollView(
                    padding: EdgeInsets.all(4.0),
                    child: new Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          new Divider(height: 5.0, color: Colors.black),
                          new Padding(
                            padding: new EdgeInsets.all(8.0),
                          ),
                          new Text(
                            'Select Package :',
                            style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.0,
                            ),
                          ),
                          new Padding(
                            padding: new EdgeInsets.all(8.0),
                          ),
                          new Divider(height: 5.0, color: Colors.black),
                          new Padding(
                            padding: new EdgeInsets.all(8.0),
                          ),
                          new Text(
                            'NORMAL (10 SAR):',
                            style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0,
                            ),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.symmetric(horizontal: 8.0,vertical: 5.0),
                            child: new Text(
                              'Chose one of any QR Codes EXCEPT (Invite QR, Vcard QR and Social Media QR) and you will Get access to:',
                              textAlign: TextAlign.center,
                              style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14.0,
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10,vertical: 2.0),
                              child: Text(
                                '• Change Background Image\n'
                                '• Number of Total Scans\n'
                                '• Number of Scans in Android Devices \n'
                                '• Number of Scans in iOS Devices \n'
                                '• Location of Scans',
                              ),
                            ),
                          ),
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              if (isSubscribedZero == 0)
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Container(
                                    height: 20.0,
                                    width: 20.0,
                                    child: new Radio(
                                      value: 0,
                                      groupValue: _radioValue1,
                                      onChanged: _handleRadioValueChange1,
                                    ),
                                  ),
                                ),
                              SizedBox(
                                width: 5.0,
                              ),
                              new Text(
                                isSubscribedZero == 0
                                    ? '10 SAR'
                                    : AppLocalizations.of(context)
                                        .translate("Already_Purchased"),
                                style: new TextStyle(
                                    fontSize:
                                        isSubscribedZero == 0 ? 16.0 : 20.0,
                                    color: isSubscribedZero == 0
                                        ? Colors.black
                                        : Colors.red),
                              ),
                            ],
                          ),
                          new Divider(
                            height: 5.0,
                            color: Colors.black,
                          ),
                          new Padding(
                            padding: new EdgeInsets.all(8.0),
                          ),
                          new Text(
                            'Business (16 SAR)',
                            style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 5.0),
                            child: new Text(
                              'Chose Vcard QR and get access to: ',
                              textAlign: TextAlign.center,
                              style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14.0,
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10,vertical: 2.0),
                              child: Text(
                                '• Change Background Image\n'
                                    '• Number of Total Scans\n'
                                    '• Number of Scans in Android Devices \n'
                                    '• Number of Scans in iOS Devices \n'
                                    '• Location of Scans',
                              ),
                            ),
                          ),
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              if (isSubscribedOne == 0)
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Container(
                                    height: 20.0,
                                    width: 20.0,
                                    child: new Radio(
                                      value: 1,
                                      groupValue: _radioValue1,
                                      onChanged: _handleRadioValueChange1,
                                    ),
                                  ),
                                ),
                              SizedBox(
                                width: 5.0,
                              ),
                              new Text(
                                isSubscribedOne == 0
                                    ? '16 SAR'
                                    : AppLocalizations.of(context)
                                        .translate("Already_Purchased"),
                                style: new TextStyle(
                                    fontSize:
                                        isSubscribedOne == 0 ? 16.0 : 20.0,
                                    color: isSubscribedOne == 0
                                        ? Colors.black
                                        : Colors.red),
                              ),
                            ],
                          ),
                          new Divider(
                            height: 5.0,
                            color: Colors.black,
                          ),
                          new Padding(
                            padding: new EdgeInsets.all(8.0),
                          ),
                          new Text(
                            'Social Media (20 SAR)',
                            style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 5.0),
                            child: new Text(
                              'Chose Social Media QR and get access to',
                              textAlign: TextAlign.center,
                              style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14.0,
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10,vertical: 2.0),
                              child: Text(
                                '• Change Background Image\n'
                                    '• Number of Total Scans\n'
                                    '• Number of Scans in Android Devices \n'
                                    '• Number of Scans in iOS Devices \n'
                                    '• Location of Scans',
                              ),
                            ),
                          ),
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              if (isSubscribedTwo == 0)
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Container(
                                    height: 20.0,
                                    width: 20.0,
                                    child: new Radio(
                                      value: 2,
                                      groupValue: _radioValue1,
                                      onChanged: _handleRadioValueChange1,
                                    ),
                                  ),
                                ),
                              SizedBox(
                                width: 5.0,
                              ),
                              new Text(
                                isSubscribedTwo == 0
                                    ? '20 SAR'
                                    : AppLocalizations.of(context)
                                        .translate("Already_Purchased"),
                                style: new TextStyle(
                                    fontSize:
                                        isSubscribedTwo == 0 ? 16.0 : 20.0,
                                    color: isSubscribedTwo == 0
                                        ? Colors.black
                                        : Colors.red),
                              ),
                            ],
                          ),
                          new Divider(
                            height: 5.0,
                            color: Colors.black,
                          ),
                          new Padding(
                            padding: new EdgeInsets.all(8.0),
                          ),
                          new Text(
                            'Invitation',
                            style: new TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20.0,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 8.0,vertical: 5.0),
                            child: new Text(
                              'Chose Invite QR and get access to',
                              textAlign: TextAlign.center,
                              style: new TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 14.0,
                              ),
                            ),
                          ),
                          Align(
                            alignment: Alignment.centerLeft,
                            child: Padding(
                              padding: EdgeInsets.symmetric(horizontal: 10,vertical: 2.0),
                              child: Text(
                                '• Change Background Image\n'
                                    '• Number of Total Scans\n'
                                    '• Number of Scans in Android Devices \n'
                                    '• Number of Scans in iOS Devices \n'
                                    '• Location of Scans',
                              ),
                            ),
                          ),
                          new Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              if (isSubscribedThree == 0)
                                Padding(
                                  padding: const EdgeInsets.symmetric(vertical: 8.0),
                                  child: Container(
                                    height: 20.0,
                                    width: 20.0,
                                    child: new Radio(
                                      value: 3,
                                      groupValue: _radioValue1,
                                      onChanged: _handleRadioValueChange1,
                                    ),
                                  ),
                                ),
                              SizedBox(
                                width: 5.0,
                              ),
                              new Text(
                                isSubscribedThree == 0
                                    ? '89 SAR'
                                    : AppLocalizations.of(context)
                                        .translate("Already_Purchased"),
                                style: new TextStyle(
                                    fontSize:
                                        isSubscribedThree == 0 ? 16.0 : 20.0,
                                    color: isSubscribedThree == 0
                                        ? Colors.black
                                        : Colors.red),
                              ),
                            ],
                          ),
                          new Divider(
                            height: 5.0,
                            color: Colors.black,
                          ),
                          new Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                new Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                    new Padding(
                                      padding: EdgeInsets.all(4.0),
                                    ),
                                    new RaisedButton(
                                      onPressed: () => sendPayment(),
                                      child: new Text(
                                        'Select Package',
                                        style: new TextStyle(
                                            fontWeight: FontWeight.normal,
                                            fontSize: 16.0,
                                            color: Colors.white),
                                      ),
                                      color:
                                          Color(CustomColors().mainColorHard()),
                                      shape: new RoundedRectangleBorder(
                                          borderRadius:
                                              new BorderRadius.circular(20.0)),
                                    )
                                  ],
                                )
                              ])
                        ]))));
  }

  void _handleRadioValueChange1(int value) {
    setState(() {
      _radioValue1 = value;

      if (_radioValue1 == 0) {
        payment = 10;
      } else if (_radioValue1 == 1) {
        payment = 16;
      } else if (_radioValue1 == 2) {
        payment = 20;
      } else if (_radioValue1 == 3) {
        payment = 89;
      }
    });
  }

  void fetchUserDetail() async {
    try {
      var fetchUserId = await SessionManager().getUserId();
      var userId = fetchUserId.toString();
      var params = {'hash': Constant.KEY, 'userId': userId};
      await HttpRequest()
          .fetchPost(Constant.get_user_detail, params)
          .then((response) {
        if (response.statusCode == 200) {
          try {
            print("Print ${response.body}");
            setState(() {
              var convertDataToJson = jsonDecode(response.body);
              isSubscribedZero =
                  int.parse(convertDataToJson['isSubscribedZero']);
              isSubscribedOne = int.parse(convertDataToJson['isSubscribedOne']);
              isSubscribedTwo = int.parse(convertDataToJson['isSubscribedTwo']);
              isSubscribedThree =
                  int.parse(convertDataToJson['isSubscribedThree']);
              getUserdataFromCache();
            });
          } catch (e) {
            print(e);
          }
        } else {
          print(response.statusCode);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  void  sendPayment() async {
    if (_radioValue1 >= 0) {
      String url = null;
      progressShow(context);

      String redirectUrl =
          "http://www.bondscode.com/bond/web/payment.php?userId=" +
              userId.toString() +
              "&isSubscribed=" +
              _radioValue1.toString();
      print("Maz URL $redirectUrl");
      var payLoad = new SendPaymentRequest(fullName, "ALL", payment, 'SAR',
          redirectUrl, redirectUrl, '+92', '3482612281', email);
      var payLoad2 = jsonEncode(payLoad.toJson());
      print("pay load 2 $payLoad2");

      await HttpRequest()
          .paymentRequest('https://apitest.myfatoorah.com/v2/SendPayment', payLoad2)
          .then((response) async {
        progressHide(context);
        if (response.statusCode == 200) {
//
//          Fluttertoast.showToast(
//              msg: "Response is 200",
//              toastLength: Toast.LENGTH_SHORT,
//              gravity: ToastGravity.CENTER,
//              timeInSecForIos: 1
//          );

          String reply = await response.transform(utf8.decoder).join();
          var parsedJson = json.decode(reply);
          if (parsedJson['IsSuccess']) {
            var invoiceUrl = parsedJson['Data']['InvoiceURL'];
            url = invoiceUrl;

            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => PremiumScreen(url),),
            );
          } else {
            showSnackBar(
                context,
                AppLocalizations.of(context)
                    .translate("Failed_this_time_String"));
          }
        } else {
          showSnackBar(
              context,
              AppLocalizations.of(context)
                  .translate("Failed_this_time_String"));
        }
      });
    } else {
      Fluttertoast.showToast(
          msg: "Select Payment Method", toastLength: Toast.LENGTH_SHORT);
    }
  }
}

class GroupModel {
  String text;
  int index;

  GroupModel({this.text, this.index});
}
