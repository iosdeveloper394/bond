import 'dart:async';
import 'dart:convert';
import 'package:bond_flutter/Constant/Constant.dart';
import 'package:bond_flutter/Http/HttpRequest.dart';
import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:bond_flutter/SessionManager/SessionManager.dart';
import 'package:flutter/material.dart';
import 'package:bond_flutter/Transitions/ScreenPageTransitions.dart';
import '../UI/CustomColors.dart';
import 'GenerateContactQrScreen.dart';
import 'GenerateEmailQrScreen.dart';
import 'GenerateInviteQrScreen.dart';
import 'GenerateSocialMediaQrScreen.dart';
import 'GenerateTextQrScreen.dart';
import 'GenerateSMSQrScreen.dart';
import 'GenerateVCardQrScreen.dart';
import 'GenerateWebsiteQrScreen.dart';

class ChooseOptionScreen extends StatefulWidget {
  ChooseOptionState createState() => ChooseOptionState();
}

class ChooseOptionState extends State<ChooseOptionScreen> {
  int isSubscribedZero;
  int isSubscribedOne;
  int isSubscribedTwo;
  int isSubscribedThree;
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool _progressBarActive = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();

    fetchUserDetail();
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      key: _scaffoldKey,
      appBar: new AppBar(
        title: new Text(
          AppLocalizations.of(context).translate("QR_Codes_String"),
          style: TextStyle(fontSize: 22),
        ),
        backgroundColor: Color(CustomColors().mainColorHard()),
      ),
      body: _progressBarActive == true
          ? const Center(child: const CircularProgressIndicator())
          : Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Material(
                color: Color(CustomColors().lightGreyBG()),
                child: Padding(
                  padding: const EdgeInsets.only(top: 0.0),
                  child: SingleChildScrollView(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minHeight: MediaQuery.of(context).size.height - 100,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(top: 0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Material(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20),
                                  ),
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2.7,
                                    height: 120.0,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Color(
                                              CustomColors().dimGreyColor()),
                                          width: 2.0),
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                      boxShadow: <BoxShadow>[
                                        new BoxShadow(blurRadius: 0.5),
                                      ],
                                    ),
                                    child: Material(
                                      borderRadius: BorderRadius.circular(20),
                                      child: InkWell(
                                        onTap: () {
                                          moveToTextQR(context);
                                        },
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              child: Image.asset(
                                                'images/qr_text_icon.png',
                                                width: 30,
                                                height: 30,
                                                color: Color(CustomColors()
                                                    .mainColorHard()),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 4.0),
                                              child: Text(
                                                AppLocalizations.of(context)
                                                    .translate(
                                                        "Text_QR_String"),
                                                style: TextStyle(fontSize: 12),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Material(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20),
                                  ),
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2.7,
                                    height: 120.0,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Color(
                                              CustomColors().dimGreyColor()),
                                          width: 2.0),
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                      boxShadow: <BoxShadow>[
                                        new BoxShadow(blurRadius: 0.5),
                                      ],
                                    ),
                                    child: Material(
                                      borderRadius: BorderRadius.circular(20),
                                      child: InkWell(
                                        onTap: () {
                                          moveToSMSQR(context);
                                        },
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              child: Image.asset(
                                                'images/qr_sms_icon.png',
                                                width: 30,
                                                height: 30,
                                                color: Color(CustomColors()
                                                    .mainColorHard()),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 4.0),
                                              child: Text(
                                                AppLocalizations.of(context)
                                                    .translate("SMS_QR_String"),
                                                style: TextStyle(fontSize: 12),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Material(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20),
                                  ),
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2.7,
                                    height: 120.0,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Color(
                                              CustomColors().dimGreyColor()),
                                          width: 2.0),
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                      boxShadow: <BoxShadow>[
                                        new BoxShadow(blurRadius: 0.5),
                                      ],
                                    ),
                                    child: Material(
                                      borderRadius: BorderRadius.circular(20),
                                      child: InkWell(
                                        onTap: () {
                                          moveToEmailQR(context);
                                        },
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              child: Image.asset(
                                                'images/qr_email_icon.png',
                                                width: 30,
                                                height: 30,
                                                color: Color(CustomColors()
                                                    .mainColorHard()),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 4.0),
                                              child: Text(
                                                AppLocalizations.of(context)
                                                    .translate(
                                                        "Email_QR_String"),
                                                style: TextStyle(fontSize: 12),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Material(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20),
                                  ),
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2.7,
                                    height: 120.0,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Color(
                                              CustomColors().dimGreyColor()),
                                          width: 2.0),
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                      boxShadow: <BoxShadow>[
                                        new BoxShadow(blurRadius: 0.5),
                                      ],
                                    ),
                                    child: Material(
                                      borderRadius: BorderRadius.circular(20),
                                      child: InkWell(
                                        onTap: () {
                                          moveToWebsiteQR(context);
                                        },
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              child: Image.asset(
                                                'images/qr_website_icon.png',
                                                width: 30,
                                                height: 30,
                                                color: Color(CustomColors()
                                                    .mainColorHard()),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 4.0),
                                              child: Text(
                                                AppLocalizations.of(context)
                                                    .translate(
                                                        "Website_QR_String"),
                                                style: TextStyle(fontSize: 12),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Material(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20),
                                  ),
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2.7,
                                    height: 120.0,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Color(
                                              CustomColors().dimGreyColor()),
                                          width: 2.0),
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                      boxShadow: <BoxShadow>[
                                        new BoxShadow(blurRadius: 0.5),
                                      ],
                                    ),
                                    child: Material(
                                      borderRadius: BorderRadius.circular(20),
                                      child: InkWell(
                                        onTap: () {
                                          moveToContactQR(context);
                                        },
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              child: Image.asset(
                                                'images/qr_contact_icon.png',
                                                width: 30,
                                                height: 30,
                                                color: Color(CustomColors()
                                                    .mainColorHard()),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 4.0),
                                              child: Text(
                                                AppLocalizations.of(context)
                                                    .translate(
                                                        "Contact_QR_String"),
                                                style: TextStyle(fontSize: 12),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Material(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20),
                                  ),
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2.7,
                                    height: 120.0,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Color(
                                              CustomColors().dimGreyColor()),
                                          width: 2.0),
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                      boxShadow: <BoxShadow>[
                                        new BoxShadow(blurRadius: 0.5),
                                      ],
                                    ),
                                    child: Material(
                                      borderRadius: BorderRadius.circular(20),
                                      child: InkWell(
                                        onTap: () {
                                          moveToVCardQR(context);
                                        },
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              child: Image.asset(
                                                'images/qr_vcard_icon.png',
                                                width: 30,
                                                height: 30,
                                                color: Color(CustomColors()
                                                    .mainColorHard()),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 4.0),
                                              child: Text(
                                                AppLocalizations.of(context)
                                                    .translate(
                                                        "VCard_QR_String"),
                                                style: TextStyle(fontSize: 12),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Material(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20),
                                  ),
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2.7,
                                    height: 120.0,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Color(
                                              CustomColors().dimGreyColor()),
                                          width: 2.0),
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                      boxShadow: <BoxShadow>[
                                        new BoxShadow(blurRadius: 0.5),
                                      ],
                                    ),
                                    child: Material(
                                      borderRadius: BorderRadius.circular(20),
                                      child: InkWell(
                                        onTap: () {
                                          moveToSocialMediaQR(context);
                                        },
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              child: Image.asset(
                                                'images/qr_social_icon.png',
                                                width: 30,
                                                height: 30,
                                                color: Color(CustomColors()
                                                    .mainColorHard()),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 4.0),
                                              child: Text(
                                                AppLocalizations.of(context)
                                                    .translate(
                                                        "Social_Media_QR_String"),
                                                style: TextStyle(fontSize: 12),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Material(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(20),
                                  ),
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width / 2.7,
                                    height: 120.0,
                                    decoration: BoxDecoration(
                                      border: Border.all(
                                          color: Color(
                                              CustomColors().dimGreyColor()),
                                          width: 2.0),
                                      color: Colors.white,
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(20),
                                      ),
                                      boxShadow: <BoxShadow>[
                                        new BoxShadow(blurRadius: 0.5),
                                      ],
                                    ),
                                    child: Material(
                                      borderRadius: BorderRadius.circular(20),
                                      child: InkWell(
                                        onTap: () {
                                          moveToInitationQR(context);
                                        },
                                        child: Column(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Container(
                                              child: Image.asset(
                                                'images/qr_invite_icon.png',
                                                width: 30,
                                                height: 30,
                                                color: Color(CustomColors()
                                                    .mainColorHard()),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                  top: 4.0),
                                              child: Text(
                                                AppLocalizations.of(context)
                                                    .translate(
                                                        "Invite_QR_String"),
                                                style: TextStyle(fontSize: 12),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
    );
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return null;
  }

  void fetchUserDetail() async {
    var fetchUserId = await SessionManager().getUserId();

    if (fetchUserId == 0) {
      setState(() {
        isSubscribedZero = 1;
        isSubscribedOne = 1;
        isSubscribedTwo = 1;
        isSubscribedThree = 1;
        _progressBarActive = !_progressBarActive;
      });
    } else {
      try {
        var userId = fetchUserId.toString();
        var params = {'hash': Constant.KEY, 'userId': userId};
        await HttpRequest()
            .fetchPost(Constant.get_user_detail, params)
            .then((response) {
          print(response.statusCode);
          if (response.statusCode == 200) {
            try {
              print(response.body);
              setState(() {
                var convertDataToJson = jsonDecode(response.body);
                isSubscribedZero =
                    int.parse(convertDataToJson['isSubscribedZero']);
                isSubscribedOne =
                    int.parse(convertDataToJson['isSubscribedOne']);
                isSubscribedTwo =
                    int.parse(convertDataToJson['isSubscribedTwo']);
                isSubscribedThree =
                    int.parse(convertDataToJson['isSubscribedThree']);


                SessionManager().setIsSubscribedZero(isSubscribedZero);
                SessionManager().setIsSubscribedOne(isSubscribedOne);
                SessionManager().setIsSubscribedTwo(isSubscribedTwo);
                SessionManager().setIsSubscribedThree(isSubscribedThree);



                _progressBarActive = !_progressBarActive;
              });
            } catch (e) {
              print(e);
            }
          } else {
            print(response.statusCode);
          }
        });
      } catch (e) {
        print(e);
      }

      int isSubscribedZeroSession = await SessionManager().getIsSubscribedZero();
      int isSubscribedOneSession = await SessionManager().getIsSubscribedOne();
      int isSubscribedtwoSession = await SessionManager().getIsSubscribedTwo();
      int isSubscribedthreeSession = await SessionManager().getIsSubscribedThree();
      print("From Session      Zero $isSubscribedZero                         One $isSubscribedOne         two $isSubscribedTwo             three $isSubscribedThree");
      print("From Session      Zero $isSubscribedZeroSession                         One $isSubscribedOneSession         two $isSubscribedtwoSession             three $isSubscribedthreeSession");
    }
  }

  moveToTextQR(BuildContext context) {
    Navigator.push(context, SlideRightRoute(page: GenerateTextQrScreen()));
  }

  moveToSMSQR(BuildContext context) {
    Navigator.push(context, SlideRightRoute(page: GenerateSMSQrScreen()));
  }

  moveToEmailQR(BuildContext context) {
    Navigator.push(context, SlideRightRoute(page: GenerateEmailQrScreen()));
  }

  moveToWebsiteQR(BuildContext context) {
    Navigator.push(context, SlideRightRoute(page: GenerateWebsiteQrScreen()));
  }

  moveToContactQR(BuildContext context) {
    Navigator.push(context, SlideRightRoute(page: GenerateContactQrScreen()));
  }

  moveToVCardQR(BuildContext context) {
    Navigator.push(context, SlideRightRoute(page: GenerateVCardQrScreen()));
  }

  moveToSocialMediaQR(BuildContext context) {
    Navigator.push(
        context, SlideRightRoute(page: GenerateSocialMediaQrScreen()));
  }

  moveToInitationQR(BuildContext context) {
//    if (isSubscribedThree == 1) {
//      Navigator.push(context, SlideRightRoute(page: GenerateInviteQrScreen()));
//    } else {
//      final snackBar = SnackBar(
//          content:
//              Text(AppLocalizations.of(context).translate('Buy_Invite_Plan')));
//      _scaffoldKey.currentState.showSnackBar(snackBar);
//    }
    Navigator.push(
      context,
      SlideRightRoute(
        page: GenerateInviteQrScreen(),
      ),
    );
  }
}
