import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:bond_flutter/Transitions/ScreenPageTransitions.dart';
import '../UI/CustomColors.dart';
import '../Validator/Validator.dart';
import 'GenerateQRScreen.dart';
import '../UI/SnackBar.dart';
import 'package:bond_flutter/SessionManager/SessionManager.dart';
import 'SelectPackage.dart';
import '../Model/DataModel.dart';
import 'dart:collection';

class GenerateVCardQrScreen extends StatefulWidget {
  GenerateVCardQrState createState() => GenerateVCardQrState();
}

class GenerateVCardQrState extends State<GenerateVCardQrScreen> {
  Validator validator;
  final qrBusinessNameController = TextEditingController();
  final qrBusinessTypeController = TextEditingController();
  // final qrTimingController = TextEditingController();
  final qrMobileController = TextEditingController();
  final qrFaxController = TextEditingController();
  final qrPhoneController = TextEditingController();
  final qrEmailController = TextEditingController();
  final qrWebsiteController = TextEditingController();
  final qrAddressController = TextEditingController();
  @override
  void initState() {
    super.initState();
    // Create an empty document or load existing if you have one.
    // Here we create an empty document:
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        title: new Text(
          AppLocalizations.of(context).translate("Generate_VCard_QR_String"),
          style: TextStyle(fontSize: 22),
        ),
        backgroundColor: Color(CustomColors().mainColorHard()),
      ),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).unfocus();
        },
        child: Builder(
          builder: (context) => Center(
            child: Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Material(
                child: Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: SingleChildScrollView(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minHeight: 400,
                      ),
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: Image.asset(
                              'images/generate_text_qr_icon.png',
                              width: 300,
                              height: 120,
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Color(CustomColors().dimGreyColor()),
                                    width: 2.0),
                                color: Color(CustomColors().lightGreyBG()),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                boxShadow: <BoxShadow>[
                                  new BoxShadow(blurRadius: 0.5),
                                ],
                              ),
                              height: 50,
                              width: MediaQuery.of(context).size.width / 1.1,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 15.0, left: 8),
                                child: TextField(
                                  maxLength: 50,
                                  maxLines: 1,
                                  controller: qrBusinessNameController,
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: AppLocalizations.of(context)
                                          .translate("Business_Name_String")),
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Color(CustomColors().dimGreyColor()),
                                    width: 2.0),
                                color: Color(CustomColors().lightGreyBG()),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                boxShadow: <BoxShadow>[
                                  new BoxShadow(blurRadius: 0.5),
                                ],
                              ),
                              height: 50,
                              width: MediaQuery.of(context).size.width / 1.1,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, left: 8),
                                child: TextField(
                                  maxLength: 50,
                                  maxLines: 1,
                                  controller: qrBusinessTypeController,
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: AppLocalizations.of(context)
                                          .translate("Business_Type_String")),
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          // Padding(
                          //   padding: const EdgeInsets.only(top:8.0),
                          //   child: Container(

                          //     decoration: BoxDecoration(
                          //       border: Border.all(color: Color(CustomColors().dimGreyColor()), width: 2.0),
                          //       color: Color(CustomColors().lightGreyBG()),
                          //       borderRadius: BorderRadius.all(
                          //         Radius.circular(10),
                          //       ),
                          //       boxShadow: <BoxShadow>[
                          //         new BoxShadow(
                          //             blurRadius: 0.5
                          //         ),
                          //       ],
                          //     ),

                          //     height: 50,
                          //     width: MediaQuery.of(context).size.width/1.1,
                          //     child: Padding(
                          //       padding: const EdgeInsets.only(top:8.0,left:8),
                          //       child: TextField(
                          //         maxLength: 50,
                          //         maxLines: 1,
                          //         controller: qrTimingController,
                          //         decoration: InputDecoration(
                          //             border: InputBorder.none,
                          //             hintText: AppLocalizations.of(context).translate("Timing_String"),
                          //         ),
                          //         style: TextStyle(

                          //           fontSize: 12,
                          //         ),
                          //       ),
                          //     ),
                          //   ),
                          // ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Color(CustomColors().dimGreyColor()),
                                    width: 2.0),
                                color: Color(CustomColors().lightGreyBG()),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                boxShadow: <BoxShadow>[
                                  new BoxShadow(blurRadius: 0.5),
                                ],
                              ),
                              height: 50,
                              width: MediaQuery.of(context).size.width / 1.1,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, left: 8),
                                child: TextField(
                                  keyboardType: TextInputType.phone,
                                  maxLength: 12,
                                  maxLines: 1,
                                  controller: qrMobileController,
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: AppLocalizations.of(context)
                                          .translate("Mobile_Number_String")),
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Color(CustomColors().dimGreyColor()),
                                    width: 2.0),
                                color: Color(CustomColors().lightGreyBG()),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                boxShadow: <BoxShadow>[
                                  new BoxShadow(blurRadius: 0.5),
                                ],
                              ),
                              height: 50,
                              width: MediaQuery.of(context).size.width / 1.1,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, left: 8),
                                child: TextField(
                                  keyboardType: TextInputType.phone,
                                  maxLength: 12,
                                  maxLines: 1,
                                  controller: qrPhoneController,
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: AppLocalizations.of(context)
                                          .translate("Phone_Number_String")),
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Color(CustomColors().dimGreyColor()),
                                    width: 2.0),
                                color: Color(CustomColors().lightGreyBG()),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                boxShadow: <BoxShadow>[
                                  new BoxShadow(blurRadius: 0.5),
                                ],
                              ),
                              height: 50,
                              width: MediaQuery.of(context).size.width / 1.1,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, left: 8),
                                child: TextField(
                                  keyboardType: TextInputType.phone,
                                  maxLength: 12,
                                  maxLines: 1,
                                  controller: qrFaxController,
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: AppLocalizations.of(context)
                                          .translate("Fax_Number_String")),
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Color(CustomColors().dimGreyColor()),
                                    width: 2.0),
                                color: Color(CustomColors().lightGreyBG()),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                boxShadow: <BoxShadow>[
                                  new BoxShadow(blurRadius: 0.5),
                                ],
                              ),
                              height: 50,
                              width: MediaQuery.of(context).size.width / 1.1,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, left: 8),
                                child: TextField(
                                  keyboardType: TextInputType.emailAddress,
                                  maxLength: 50,
                                  maxLines: 1,
                                  controller: qrEmailController,
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: AppLocalizations.of(context)
                                          .translate("Email_String")),
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Color(CustomColors().dimGreyColor()),
                                    width: 2.0),
                                color: Color(CustomColors().lightGreyBG()),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                boxShadow: <BoxShadow>[
                                  new BoxShadow(blurRadius: 0.5),
                                ],
                              ),
                              height: 50,
                              width: MediaQuery.of(context).size.width / 1.1,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, left: 8),
                                child: TextField(
                                  keyboardType: TextInputType.url,
                                  maxLength: 50,
                                  maxLines: 1,
                                  controller: qrWebsiteController,
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: AppLocalizations.of(context)
                                          .translate("Website_String")),
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: Container(
                              decoration: BoxDecoration(
                                border: Border.all(
                                    color: Color(CustomColors().dimGreyColor()),
                                    width: 2.0),
                                color: Color(CustomColors().lightGreyBG()),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                boxShadow: <BoxShadow>[
                                  new BoxShadow(blurRadius: 0.5),
                                ],
                              ),
                              height: 50,
                              width: MediaQuery.of(context).size.width / 1.1,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(top: 8.0, left: 8),
                                child: TextField(
                                  maxLength: 50,
                                  maxLines: 1,
                                  controller: qrAddressController,
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: AppLocalizations.of(context)
                                          .translate("Address_String")),
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),
                          ),

                          Padding(
                            padding: const EdgeInsets.all(15.0),
                            child: InkWell(
                              onTap: () async {
                                // int subscribedOne = await SessionManager().getIsSubscribedOne();
                                // int subscribedTwo = await SessionManager().getIsSubscribedTwo();
                                // int subscribedThree = await SessionManager().getIsSubscribedThree();
                                //             if(subscribedOne == 1 || subscribedThree == 1 || subscribedTwo == 1){
                                validator = new Validator(context);
                                if (qrBusinessNameController.text.isEmpty ||
                                    qrBusinessNameController.text.length < 1 ||
                                    qrBusinessNameController.text.length >
                                        100) {
                                  showSnackBar(
                                      context,
                                      AppLocalizations.of(context).translate(
                                          "Invalid_Business_name_String"));
                                  return;
                                }
                                if (qrBusinessTypeController.text.isEmpty ||
                                    qrBusinessTypeController.text.length < 1 ||
                                    qrBusinessTypeController.text.length >
                                        100) {
                                  showSnackBar(
                                      context,
                                      AppLocalizations.of(context).translate(
                                          "Invalid_Business_type_String"));
                                  return;
                                }
                                // if (qrTimingController.text.isEmpty || qrTimingController.text.length<5|| qrTimingController.text.length >100 )
                                // {
                                //   showSnackBar(context, AppLocalizations.of(context).translate("Invalid_timings_String"));
                                //   return;
                                // }
                                if (qrPhoneController.text.isEmpty ||
                                    qrPhoneController.text.length != 12 ||
                                    !qrPhoneController.text.startsWith('+')) {
                                  showSnackBar(
                                      context,
                                      AppLocalizations.of(context).translate(
                                          "Invalid_Phone_No_String"));
                                  return;
                                }
                                if (qrMobileController.text.isEmpty ||
                                    qrMobileController.text.length != 12 ||
                                    !qrMobileController.text.startsWith('+')) {
                                  showSnackBar(
                                      context,
                                      AppLocalizations.of(context).translate(
                                          "Invalid_Mobile_No_String"));
                                  return;
                                }
                                if (qrFaxController.text.isEmpty ||
                                    qrFaxController.text.length != 12 ||
                                    !qrFaxController.text.startsWith('+')) {
                                  showSnackBar(
                                      context,
                                      AppLocalizations.of(context)
                                          .translate("Invalid_Fax_No_String"));
                                  return;
                                }
                                if (!validator.isEmailTrue(
                                    qrEmailController.text.toString())) {
                                  return;
                                }
                                if (qrWebsiteController.text.isEmpty ||
                                    qrWebsiteController.text.length < 1 ||
                                    qrWebsiteController.text.length > 100) {
                                  showSnackBar(
                                      context,
                                      AppLocalizations.of(context)
                                          .translate("Invalid_Website_String"));
                                  return;
                                }
                                if (qrAddressController.text.isEmpty ||
                                    qrAddressController.text.length < 5 ||
                                    qrAddressController.text.length > 100) {
                                  showSnackBar(
                                      context,
                                      AppLocalizations.of(context)
                                          .translate("Invalid_Address_String"));
                                  return;
                                }
                                HashMap<String, String> list = new HashMap();
                                list['businessName'] =
                                    (qrBusinessNameController.text);
                                list['businessType'] =
                                    (qrBusinessTypeController.text);
                                // list['timing']=(qrTimingController.text);
                                list['mobile'] = (qrMobileController.text);
                                list['phone'] = (qrPhoneController.text);
                                list['fax'] = (qrFaxController.text);
                                list['email'] = (qrEmailController.text);
                                list['website'] = (qrWebsiteController.text);
                                list['address'] = (qrAddressController.text);

                                DataModel dataModel =
                                    new DataModel(6, "vcard", list);

                                Navigator.push(
                                    context,
                                    SlideRightRoute(
                                        page: GenerateQRScreen(
                                            dataModel, false)));
//                              }
//                              else{
//                                showSnackBar(
//                                    context,
//                                    AppLocalizations.of(context)
//                                        .translate("Buy_Any_Plan"));
//                                Navigator.push(context, MaterialPageRoute(builder: (context) => SelectPackage()));
//                              }
                              },
                              child: Image.asset(
                                'images/generate_button_icon.png',
                                width: 50,
                                height: 50,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return null;
  }
}
