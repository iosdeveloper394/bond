import 'dart:io';
import 'package:bond_flutter/Constant/Constant.dart';
import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:bond_flutter/SessionManager/SessionManager.dart';
import 'package:bond_flutter/Transitions/ScreenPageTransitions.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../UI/CustomColors.dart';
import '../Http/HttpRequest.dart';
import 'dart:convert' as json;
import '../UI/SnackBar.dart';
import 'ChatScreen.dart';
import 'ShowUserProfileScreen.dart';

class MyFriendScreen extends StatefulWidget {
  @override
  State createState() => MyFriendState();
}

class MyFriendState extends State<MyFriendScreen> {
  List list = new List();
  bool isLoading = false;
  var searchTextController = new TextEditingController();
  int count = 0;
  String totalFriends = "0";

  Future<int> getAllUsers() async {
    int statuscode = 401;
    setState(() {
      isLoading = true;
    });
    try {
      var fetchUserId = await SessionManager().getUserId();
      var userId = fetchUserId.toString();
      var params = {'hash': Constant.KEY, 'userId': userId};
      await HttpRequest()
          .fetchPost(Constant.get_my_friends, params)
          .then((response) {
        setState(() {
          isLoading = false;
        });
        print(response.statusCode);
        if (response.statusCode == 200) {
          try {
            print(response.body);
            setState(() {
              var convertDataToJson = json.jsonDecode(response.body);
              list = convertDataToJson['result'];
              if (list != null) totalFriends = list.length.toString();
              statuscode = 200;
            });
          } catch (e) {
            print(e);
            statuscode = 401;
          }
        } else if (response.statusCode == 404) {
          statuscode = 401;
        } else {
          print(response.statusCode);
          statuscode = 401;
        }
      });
    } catch (e) {
      print(e);
      statuscode = 401;
    }
    return statuscode;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAllUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          title: new Text(
            AppLocalizations.of(context).translate("My_Friends_String"),
            style: TextStyle(fontSize: 22),
          ),
          backgroundColor: Color(CustomColors().mainColorHard()),
          automaticallyImplyLeading: true,
        ),
        body: Builder(
          builder: (context) => Center(
            child: Container(
                child: Stack(
              children: <Widget>[
               (list.length > 0) ? Container(
                  color: greyColor2,
                  child: Padding(
                    padding: const EdgeInsets.only(top: 60.0),
                    child: ListView.builder(
                        itemCount: list.length == null ? 0 : list.length,
                        itemBuilder: (BuildContext context, int index) {
                          //  totalFriends = list.length.toString();

                          return Material(
                            child: InkWell(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => Chat(
                                              peerId: list[index]['userId'],
                                              peerAvatar: list[index]
                                                  ['profileImage'],
                                            )));
                              },
                              child: new Container(
                                child: Center(
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.stretch,
                                    children: <Widget>[
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Card(
                                          shape: RoundedRectangleBorder(
                                            borderRadius:
                                                BorderRadius.circular(20.0),
                                          ),
                                          child: Container(
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              children: <Widget>[
                                                Row(
                                                  children: <Widget>[
                                                    Material(
                                                      child: FadeInImage
                                                          .assetNetwork(
                                                        height: 75.0,
                                                        width: 75.0,
                                                        placeholder:
                                                            'images/profile_icon.png',
                                                        image: list[index]
                                                            ['profileImage'],
                                                      ),
                                                      // list[index]['profileImage'] != null
                                                      //     ? CachedNetworkImage(
                                                      //   placeholder: (context, url) => Container(
                                                      //     child: CircularProgressIndicator(
                                                      //       strokeWidth: 1.0,
                                                      //       valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                                                      //     ),
                                                      //     width: 75.0,
                                                      //     height: 75.0,
                                                      //     padding: EdgeInsets.all(15.0),
                                                      //   ),
                                                      //   imageUrl: list[index]['profileImage'],
                                                      //   width: 75.0,
                                                      //   height: 75.0,
                                                      //   fit: BoxFit.cover,
                                                      // )
                                                      //     : Icon(
                                                      //   Icons.account_circle,
                                                      //   size: 75.0,
                                                      //   color: Colors.white,
                                                      // ),
                                                      borderRadius:
                                                          BorderRadius.all(
                                                              Radius.circular(
                                                                  35.0)),
                                                      clipBehavior:
                                                          Clip.hardEdge,
                                                      color: Color(
                                                          CustomColors()
                                                              .mainColorHard()),
                                                    ),
                                                    Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                              left: 12.0),
                                                      child: Text(
                                                        list[index]
                                                                ['firstName'] +
                                                            " " +
                                                            list[index]
                                                                ['lastName'],
                                                        style: TextStyle(
                                                            color: Color(
                                                                CustomColors()
                                                                    .mainColorHard()),
                                                            fontSize: 18),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Icon(
                                                  Icons.arrow_forward,
                                                  size: 20.0,
                                                  color: Colors.grey,
                                                )
                                              ],
                                            ),
                                            margin: const EdgeInsets.all(10.0),
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          );
                        }),
                  ),
                ) : 
                Positioned(
                  child: Container(
                          child: Center(
                            child: Text("You have no Friends yet."),
                        ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Align(
                    alignment: Alignment.topCenter,
                    child: Text(
                      "Total Friends " + totalFriends,
                      style: TextStyle(
                        color: Color(CustomColors().mainColorHard()),
                        fontSize: 30,
                      ),
                    ),
                  ),
                ),
                Positioned(
                  child: isLoading
                      ? Container(
                          child: Center(
                            child: CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(themeColor)),
                          ),
                          color: Colors.white.withOpacity(0.8),
                        )
                      : Container(),
                ),
              ],
            )),
          ),
        ));
  }
}
