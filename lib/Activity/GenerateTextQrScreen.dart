import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:bond_flutter/Transitions/ScreenPageTransitions.dart';
import '../UI/CustomColors.dart';
import '../Validator/Validator.dart';
import 'GenerateQRScreen.dart';
import '../UI/SnackBar.dart';
import '../Model/DataModel.dart';
import 'dart:collection';
class GenerateTextQrScreen extends StatefulWidget{
  GenerateTextQrState createState()=> GenerateTextQrState();
}

class GenerateTextQrState extends State<GenerateTextQrScreen> {

  Validator validator;
  final qrTextController = TextEditingController();
  @override
  void initState() {
    super.initState();
    // Create an empty document or load existing if you have one.
    // Here we create an empty document:
  }


  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        title: new Text(AppLocalizations.of(context).translate("Generate_Text_QR_String"),
          style: TextStyle(
              fontSize: 22
          ),),
        backgroundColor: Color(CustomColors().mainColorHard()),
      ),
      body:

      Builder(
        builder: (context)=> Center (
          child: Container(
            width: MediaQuery
                .of(context)
                .size
                .width,
            height: MediaQuery
                .of(context)
                .size
                .height,
            child: Material(
              child: Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: SingleChildScrollView(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: 400,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Image.asset(
                            'images/generate_text_qr_icon.png',
                            width: 300,
                            height: 120,
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                            border: Border.all(color: Color(CustomColors().dimGreyColor()), width: 2.0),
                            color: Color(CustomColors().lightGreyBG()),
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                            boxShadow: <BoxShadow>[
                              new BoxShadow(
                                  blurRadius: 0.5
                              ),
                            ],
                          ),

                          height: 300,
                          width: MediaQuery.of(context).size.width/1.1,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: TextField(
                        maxLength: 100,
                             maxLines: 6,
                             textInputAction: TextInputAction.go,
                             controller: qrTextController,
                              decoration: InputDecoration(
                              border: InputBorder.none,
                                hintText: AppLocalizations.of(context).translate("Max_100_Characters_String")
                              ),
                              style: TextStyle(
                                fontSize: 12,
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: InkWell(
                            onTap: (){
                              if (qrTextController.text.isEmpty || qrTextController.text.length<1|| qrTextController.text.length >100 )
                                {
                                  showSnackBar(context, AppLocalizations.of(context).translate("Invalid_Text_for_QR_String"));
                                  return;
                                }
                              HashMap<String,String> list = new HashMap();
                              list['info']=(qrTextController.text);
                              DataModel dataModel = new DataModel(1,"text",list);

                              Navigator.push(context, SlideRightRoute(page: GenerateQRScreen(dataModel,false)));
                            },
                            child: Image.asset(
                              'images/generate_button_icon.png',
                              width: 50,
                              height: 50,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),),
        ),
      ),
    );
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return null;
  }

}