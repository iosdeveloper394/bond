import 'package:bond_flutter/UI/CustomColors.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class CurrentLocationScreen extends StatefulWidget {
  var lat, long;
  CurrentLocationScreen(this.lat, this.long);
  CurrentLocationState createState() => CurrentLocationState();
}

class CurrentLocationState extends State<CurrentLocationScreen> {

  var initialLatlong;
  Set<Marker> markers;

  // static final CameraPosition _kGooglePlex = CameraPosition(
  //   target: LatLng(-8.591204, 116.116208),
  //   zoom: 14.4746,
  // );
  
  @override
  void initState() {
    super.initState();
    markers = Set.from([]);
  }

  @override
  Widget build(BuildContext context) {

    createMarker();

    return Scaffold(
      appBar: new AppBar(
        title: new Text(
          "Location",
          style: TextStyle(fontSize: 22),
        ),
        backgroundColor: Color(CustomColors().mainColorHard()),
        automaticallyImplyLeading: true,
      ),
      body: GoogleMap(
        myLocationButtonEnabled: false,
        myLocationEnabled: false,
        onMapCreated: (GoogleMapController controller) {},
        initialCameraPosition:
            CameraPosition(target: LatLng(double.parse(widget.lat), double.parse(widget.long)), zoom: 12),
            markers: markers,
      ),
    );
  }

  createMarker() {
    var point = Marker(markerId: MarkerId(1.toString()),position: LatLng(double.parse(widget.lat), double.parse(widget.long)));
    markers.add(point);
  }
}
