import 'dart:convert';

import 'package:bond_flutter/Activity/DashboardScreen.dart';
import 'package:bond_flutter/Constant/Constant.dart';
import '../Http/HttpRequest.dart';
import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:bond_flutter/SessionManager/SessionManager.dart';
import 'package:bond_flutter/UI/CustomColors.dart';
import 'package:flutter/material.dart';
//import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:webview_flutter/webview_flutter.dart';

class PremiumScreen extends StatefulWidget {
  String url;

  PremiumScreen(this.url);

  @override
  PremiumState createState() => new PremiumState(this.url);
}

class PremiumState extends State<PremiumScreen> {
//  InAppWebViewController webView;
  String url = "";
  double progress = 0;

  PremiumState(this.url);

  final PageRouteBuilder _homeRoute = new PageRouteBuilder(
    pageBuilder: (BuildContext context, _, __) {
      return DashboardScreen();
    },
  );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: new AppBar(
          title: new Text(
            AppLocalizations.of(context).translate("Premium_String"),
            style: TextStyle(fontSize: 22),
          ),
          actions: <Widget>[
            FlatButton(
              textColor: Colors.white,
              onPressed: () { cacheDataAndGotoDashboard(); },
              child: Text("Done"),
              shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
            ),
          ],
          automaticallyImplyLeading: true,
          backgroundColor: Color(CustomColors().mainColorHard()),
        ),
        body: Container(
          child: Column(
            children: <Widget>[
              (progress != 1.0)
                  ? LinearProgressIndicator(value: progress)
                  : null,
              Expanded(
                child: Container(
                  margin: const EdgeInsets.all(10.0),
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.blueAccent),),
                  child:
                  WebView(
                    initialUrl: url,
                    javascriptMode: JavascriptMode.unrestricted,
                  ),
//                  InAppWebView(
//                    initialUrl: url,
//                    initialHeaders: {},
////                    initialOptions: InAppWebViewWidgetOptions(),
//                    onWebViewCreated: (InAppWebViewController controller) {
//                      webView = controller;
//                    },
//                    onLoadStart:
//                        (InAppWebViewController controller, String url) {
//                      print("started $url");
//                    },
//                    onProgressChanged:
//                        (InAppWebViewController controller, int progress) {
//                      setState(() {
//                        this.progress = progress / 100;
//                      });
//                    },
//                  ),
                ),
              ),
            ].where((Object o) => o != null).toList(),
          ),
        ),
      ),
    );
  }

  void cacheDataAndGotoDashboard() {
    print("I am here");

    Navigator.pushAndRemoveUntil(
        context, _homeRoute, (Route<dynamic> r) => false);
//    getSubscribedData();
  }

  void getSubscribedData() async {
    try {
      var fetchUserId = await SessionManager().getUserId();
      var userId = fetchUserId.toString();
      var params = {'hash': Constant.KEY, 'userId': userId};
      await HttpRequest()
          .fetchPost(Constant.get_user_detail, params)
          .then((response) {
        if (response.statusCode == 200) {
          try {
            print(response.body);
            setState(() async {
              var convertDataToJson = jsonDecode(response.body);
              int isSubscribedZero, isSubscribedTwo, isSubscribedOne, isSubscribedThree;


              isSubscribedZero =
                  int.parse(convertDataToJson['isSubscribedZero']);
              isSubscribedOne = int.parse(convertDataToJson['isSubscribedOne']);
              isSubscribedTwo = int.parse(convertDataToJson['isSubscribedTwo']);
              isSubscribedThree =
                  int.parse(convertDataToJson['isSubscribedThree']);


              int isSubscribedZeroSessionOld = await SessionManager().getIsSubscribedZero();
              int isSubscribedOneSessionOld = await SessionManager().getIsSubscribedOne();
              int isSubscribedtwoSessionOld = await SessionManager().getIsSubscribedTwo();
              int isSubscribedthreeSessionOld = await SessionManager().getIsSubscribedThree();


              if(isSubscribedZero == 1){
                SessionManager().setIsSubscribedZero(isSubscribedZero);
              }
              if(isSubscribedOne == 1 ){
                SessionManager().setIsSubscribedOne(isSubscribedOne);
              }
              if(isSubscribedTwo == 1){
                SessionManager().setIsSubscribedTwo(isSubscribedTwo);
              }
              if(isSubscribedThree == 1 ){
                SessionManager().setIsSubscribedThree(isSubscribedThree);
              }


              int isSubscribedZeroSession = await SessionManager().getIsSubscribedZero();
              int isSubscribedOneSession = await SessionManager().getIsSubscribedOne();
              int isSubscribedtwoSession = await SessionManager().getIsSubscribedTwo();
              int isSubscribedthreeSession = await SessionManager().getIsSubscribedThree();


              print("Convert data to json $convertDataToJson");
              print("Zero $isSubscribedZero                         One $isSubscribedOne         two $isSubscribedTwo             three $isSubscribedThree");
              print("From Session      Zero $isSubscribedZero                         One $isSubscribedOne         two $isSubscribedTwo             three $isSubscribedThree");
              print("From SessionOLD      Zero $isSubscribedZeroSessionOld                         One $isSubscribedOneSessionOld         two $isSubscribedtwoSessionOld             three $isSubscribedthreeSessionOld");
              print("From Session      Zero $isSubscribedZeroSession                         One $isSubscribedOneSession         two $isSubscribedtwoSession             three $isSubscribedthreeSession");

            });
          } catch (e) {
            print(e);
          }
        } else {
          print(response.statusCode);
        }
      });
    } catch (e) {
      print(e);
    }
  }
}
