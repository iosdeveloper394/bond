import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:bond_flutter/Model/Item.dart';
import 'package:bond_flutter/SessionManager/SessionManager.dart';
import 'package:bond_flutter/UI/ProgressBar.dart';
import 'package:flutter/material.dart';
import 'package:bond_flutter/Transitions/ScreenPageTransitions.dart';
import 'package:place_picker/place_picker.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../UI/CustomColors.dart';
import '../Validator/Validator.dart';
import 'GenerateQRScreen.dart';
import '../UI/SnackBar.dart';
import '../Model/DataModel.dart';
import 'dart:collection';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart'; // For File Upload To Firestore
import 'package:image_picker/image_picker.dart'; // For Image Picker
import 'package:path/path.dart' as Path;
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';

import 'SelectPackage.dart';
// import 'package:google_map_location_picker/google_map_location_picker.dart';

class GenerateInviteQrScreen extends StatefulWidget {
  GenerateInviteQrState createState() => GenerateInviteQrState();
}

class GenerateInviteQrState extends State<GenerateInviteQrScreen> {
  DateTime now = new DateTime.now();
  bool isLocation = false, isDatetime = false;
  double lat, lng;
  File _image;
  String _uploadedFileURL = "No File Selected";
  String datetime = new DateTime.now().toString();
  String location = 'No Location selected';
  Item selectedItem1, selectedItem2, selectedItem3, selectedItem4;
  List<Item> users = <Item>[
    const Item(
        'Vimeo',
        Icon(
          FontAwesomeIcons.vimeo,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'DailyMotion',
        Icon(
          FontAwesomeIcons.dailymotion,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Youtube',
        Icon(
          FontAwesomeIcons.youtube,
          color: const Color(0xFF167F67),
        )),
  ];

  Validator validator;
  final textController = TextEditingController();

//  final selectedItem2Controller = TextEditingController();
  final selectedItem3Controller = TextEditingController();
  final locationController = TextEditingController();
//  final selectedItem4Controller = TextEditingController();

  bool isFile = false;
  String stringValue;

  @override
  void initState() {
    super.initState();
    // Create an empty document or load existing if you have one.
    // Here we create an empty document:
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        title: new Text(
          AppLocalizations.of(context).translate("Generate_Invite_QR_String"),
          style: TextStyle(fontSize: 22),
        ),
        backgroundColor: Color(CustomColors().mainColorHard()),
      ),
      body: Builder(
        builder: (context) => Center(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Material(
              child: Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: SingleChildScrollView(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: 400,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Image.asset(
                            'images/generate_text_qr_icon.png',
                            width: 300,
                            height: 120,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color(CustomColors().dimGreyColor()),
                                  width: 2.0),
                              color: Color(CustomColors().lightGreyBG()),
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                              boxShadow: <BoxShadow>[
                                new BoxShadow(blurRadius: 0.5),
                              ],
                            ),
                            height: 220,
                            width: MediaQuery.of(context).size.width / 1.1,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: 5.0, horizontal: 8),
                              child: TextField(
                                maxLength: 450,
                                maxLines: 15,
                                textInputAction: TextInputAction.go,
                                controller: textController,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: AppLocalizations.of(context)
                                        .translate("Text_String")),
                                style: TextStyle(
                                  fontSize: 12,
                                ),
                              ),
                            ),
                          ),
                        ),

                        GestureDetector(
                          onTap: () {
                            FocusScope.of(context)
                                .requestFocus(new FocusNode());
                            DatePicker.showDatePicker(context,
                                showTitleActions: true,
                                minTime: DateTime(now.year, 1, 1),
                                maxTime: DateTime(now.year + 2, 12, 31),
                                onChanged: (date) {}, onConfirm: (date) {
                              setState(() {
                                datetime = date.toString();
                                isDatetime = true;
                              });
                            },
                                currentTime: DateTime.now(),
                                locale: LocaleType.en);
                          },
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              child: Container(
                                alignment: Alignment.centerLeft,
                                height: 40,
                                width: MediaQuery.of(context).size.width / 1.5,
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      color: Color(
                                        CustomColors().dimGreyColor(),
                                      ),
                                      width: 2.0),
                                  color: Color(
                                    CustomColors().lightGreyBG(),
                                  ),
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(10),
                                  ),
                                  boxShadow: <BoxShadow>[
                                    new BoxShadow(blurRadius: 0.5),
                                  ],
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.only(
                                    left: 8,
                                  ),
                                  child: Text(
                                    datetime.substring(0, 10),
                                    maxLines: 1,
                                    style: TextStyle(
                                      fontSize: 12,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        // GestureDetector(
                        //   onTap: () {
                        //     showPlacePicker();
                        //   },
                        //   child: Padding(
                        //     padding: const EdgeInsets.all(8.0),
                        //     child: Container(
                        //       child: Container(
                        //         alignment: Alignment.centerLeft,
                        //         height: 40,
                        //         width: MediaQuery.of(context).size.width / 1.5,
                        //         decoration: BoxDecoration(
                        //           border: Border.all(
                        //             color: Color(
                        //               CustomColors().dimGreyColor(),
                        //             ),
                        //             width: 2.0,
                        //           ),
                        //           color: Color(CustomColors().lightGreyBG()),
                        //           borderRadius: BorderRadius.all(
                        //             Radius.circular(10),
                        //           ),
                        //           boxShadow: <BoxShadow>[
                        //             new BoxShadow(blurRadius: 0.5),
                        //           ],
                        //         ),
                        //         child: Padding(
                        //           padding: const EdgeInsets.only(
                        //             left: 8,
                        //           ),
                        //           child: Text(
                        //             location,
                        //             // AppLocalizations.of(context).translate("No_Location_selected_String"),
                        //             maxLines: 1,
                        //             style: TextStyle(
                        //               fontSize: 12,
                        //             ),
                        //           ),
                        //         ),
                        //       ),
                        //     ),
                        //   ),
                        // ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            child: Container(
                              alignment: Alignment.centerLeft,
                              height: 40,
                              width: MediaQuery.of(context).size.width / 1.5,
                              decoration: BoxDecoration(
                                border: Border.all(
                                  color: Color(
                                    CustomColors().dimGreyColor(),
                                  ),
                                  width: 2.0,
                                ),
                                color: Color(CustomColors().lightGreyBG()),
                                borderRadius: BorderRadius.all(
                                  Radius.circular(10),
                                ),
                                boxShadow: <BoxShadow>[
                                  new BoxShadow(blurRadius: 0.5),
                                ],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(
                                  left: 8,
                                  bottom: 3.0,
                                ),
                                child: TextField(
                                  maxLines: 1,
                                  controller: locationController,
                                  decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: "Enter location"),
                                  style: TextStyle(
                                    fontSize: 12,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                              child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              DropdownButton<Item>(
                                hint: Text(AppLocalizations.of(context)
                                    .translate("Select_item_String")),
                                value: selectedItem3,
                                onChanged: (Item Value) {
                                  FocusScope.of(context)
                                      .requestFocus(new FocusNode());
                                  setState(() {
                                    selectedItem3 = Value;
                                  });
                                },
                                items: users.map((Item user) {
                                  return DropdownMenuItem<Item>(
                                    value: user,
                                    child: Row(
                                      children: <Widget>[
                                        user.icon,
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          user.name,
                                          style: TextStyle(color: Colors.black),
                                        ),
                                      ],
                                    ),
                                  );
                                }).toList(),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Color(
                                            CustomColors().dimGreyColor()),
                                        width: 2.0),
                                    color: Color(CustomColors().lightGreyBG()),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                    boxShadow: <BoxShadow>[
                                      new BoxShadow(blurRadius: 0.5),
                                    ],
                                  ),
                                  height: 40,
                                  width: 200,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      bottom: 0.0,
                                      left: 8,
                                    ),
                                    child: TextField(
                                      maxLines: 1,
                                      controller: selectedItem3Controller,
                                      decoration: InputDecoration(
                                          contentPadding:
                                              const EdgeInsets.symmetric(
                                                  vertical: 12.0,
                                                  horizontal: 0),
                                          border: InputBorder.none,
                                          hintText: "@abdulsamad"),
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(10.0),
                          child: Container(
                            child: Row(
                              mainAxisSize: MainAxisSize.max,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                RaisedButton(
                                  child: Text(
                                    AppLocalizations.of(context)
                                        .translate("Upload_File_String"),
                                    style: TextStyle(color: Colors.white),
                                  ),
                                  onPressed: chooseFile,
                                  color: Colors.blueAccent,
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 8.0, left: 20),
                                  child: Container(
                                    alignment: Alignment.center,
                                    height: 140,
                                    width: 200,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                        left: 0,
                                      ),
                                      child: Container(
                                        child: _image != null
                                            ? Image.file(
                                                File(_image.path),
                                                height: 150,
                                              )
                                            // Image.asset(
                                            //     _image.path,
                                            //     height: 150,
                                            //   )
                                            : Container(height: 150),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
//
//                        Text('Selected Image'),
//                        _image != null
//                            ? Image.asset(
//                          _image.path,
//                          height: 150,
//                        )
//                            : Container(height: 150),
//                        _image == null
//                            ? RaisedButton(
//                          child: Text('Choose File'),
//                          onPressed: chooseFile,
//                          color: Colors.cyan,
//                        )
//                            : Container(),
//                        _image != null
//                            ? RaisedButton(
//                          child: Text('Upload File'),
//                          onPressed: chooseFile,
//                          color: Colors.cyan,
//                        )
//                            : Container(),
//                        _image != null
//                            ? RaisedButton(
//                          child: Text('Clear Selection'),
//                    //      onPressed: clearSelection,
//                        )
//                            : Container(),
//                        Text('Uploaded Image'),
//                        _uploadedFileURL != null
//                            ? Image.network(
//                          _uploadedFileURL,
//
//                          height: 150,
//                        )
//                            : Container(),
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: InkWell(
                            onTap: () async {
                              saveValue();

                              int isSubscribedThree =
                                  await SessionManager().getIsSubscribedThree();
                              if (isSubscribedThree == 1) {
                                validator = new Validator(context);
                                if (textController.text.isEmpty ||
                                    textController.text.length < 1 ||
                                    textController.text.length > 250) {
                                  showSnackBar(
                                    context,
                                    AppLocalizations.of(context)
                                        .translate("Invalid_Text_String"),
                                  );
                                  return;
                                }

                                HashMap<String, String> list = new HashMap();
                                list['text'] = (textController.text);

                                if (selectedItem3 != null) {
                                  if (selectedItem3Controller.text.isEmpty ||
                                      selectedItem3Controller.text.length < 1 ||
                                      selectedItem3Controller.text.length >
                                          25) {
                                    showSnackBar(
                                        context,
                                        AppLocalizations.of(context).translate(
                                                "Invalid_empty_String") +
                                            selectedItem3.name.toString());
                                    return;
                                  } else
                                    list[selectedItem3.name.toString()] =
                                        selectedItem3Controller.text;
                                }

                                if (isDatetime) {
                                  list['datetime'] = datetime;
                                }
                                // if (isLocation) {
                                //   list['location'] =
                                //       lat.toString() + ',' + lng.toString();
                                // }

                                if (locationController.text.isNotEmpty) {
                                  if (locationController.text.length >= 5) {
                                    list['location'] = locationController.text;
                                  } else {
                                    showSnackBar(
                                        context,
                                        AppLocalizations.of(context).translate(
                                            "location_text_error_string"));
                                    return;
                                  }
                                }

                                if (isFile) {
                                  uploadFile().then(
                                    (onComplete) async {
                                      progressHide(context);
                                      debugPrint(_uploadedFileURL);
                                      list['image'] = _uploadedFileURL;

                                      DataModel dataModel =
                                          new DataModel(8, "invite", list);
                                      print(list);

                                      Navigator.push(
                                          context,
                                          SlideRightRoute(
                                              page: GenerateQRScreen(
                                                  dataModel, true)));
                                    },
                                  );
                                } else {
                                  DataModel dataModel =
                                      new DataModel(8, "invite", list);

                                  print(list);

                                  Navigator.push(
                                      context,
                                      SlideRightRoute(
                                          page: GenerateQRScreen(
                                              dataModel, true)));
                                }
                                // }
                                // else{
                                //   showSnackBar(
                                //       context,
                                //       AppLocalizations.of(context)
                                //           .translate("Buy_Any_Plan"));
                                //   Navigator.push(context, MaterialPageRoute(builder: (context) => SelectPackage()));
                                // }
                              } else {
                                showSnackBar(
                                    context,
                                    AppLocalizations.of(context)
                                        .translate("Buy_Any_Plan"));
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => SelectPackage(),
                                  ),
                                );
                              }
                            },
                            child: Image.asset(
                              'images/generate_button_icon.png',
                              width: 50,
                              height: 50,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return null;
  }

  Future chooseFile() async {
    await ImagePicker.pickImage(source: ImageSource.gallery).then((image) {
      setState(() {
        _image = image;
        _uploadedFileURL = Path.basename(_image.path);
        isFile = true;
      });
    });
  }

  Future uploadFile() async {
    progressShow(context);
    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child('uploads/${Path.basename(_image.path)}');
    StorageUploadTask uploadTask = storageReference.putFile(_image);
    await uploadTask.onComplete;
    print('File Uploaded');
    storageReference.getDownloadURL().then((fileURL) {
      setState(() {
        _uploadedFileURL = fileURL;
        print(_uploadedFileURL);
        // progressHide(context);
      });
    });
  }

  void showPlacePicker() async {
    // LocationResult result = await showLocationPicker(context, "AIzaSyCxoMdHOVPaOHjqhxeyupZJPkPpW-WpEpQ");
    // print(result);

    LocationResult result = await Navigator.of(context).push(MaterialPageRoute(
        //AIzaSyC4E4HphP6fXiI7MOfqyPppHDhi4DnD800
        builder: (context) =>
            PlacePicker("AIzaSyAbkcxvdnMFc2y9fWGJU8KVLDvwyBVK-Wg")));

    print(result);
    setState(() {
      location = result.formattedAddress;
      isLocation = true;
      lat = result.latLng.latitude;
      lng = result.latLng.longitude;
    });
  }

  saveValue() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('key', "abcd1234");
  }
}
