import 'dart:convert';
import 'dart:typed_data';
import 'dart:ui';

import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:bond_flutter/SessionManager/SessionManager.dart';
import 'package:bond_flutter/UI/ProgressBar.dart';
import 'package:bond_flutter/UI/SnackBar.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:path_provider/path_provider.dart';
import 'package:progress_hud/progress_hud.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:screenshot/screenshot.dart';
import '../UI/CustomColors.dart';
import '../Model/DataModel.dart';
import '../Constant/Constant.dart';
import '../Http/HttpRequest.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:url_launcher/url_launcher.dart';
import 'dart:io';

import 'DashboardScreen.dart';

class QRPreviewAndLinkGenerateScreen extends StatefulWidget {
  DataModel dataModel;
  var result;
  File _image;
  File _backgroundImg;
  bool isImageSet = false;
  int isPremiumUser;
  QRPreviewAndLinkGenerateScreen(this.dataModel, this.isPremiumUser,
      this.result, this._image, this._backgroundImg,
      {Key key})
      : super(key: key); //add

  QRPreviewAndLinkGenerateState createState() =>
      QRPreviewAndLinkGenerateState();
}

class QRPreviewAndLinkGenerateState
    extends State<QRPreviewAndLinkGenerateScreen> {
//  Color foregroundColor = Colors.blue.shade300;
//  Color foregroundColor = Colors.transparent;
  String dataToShow = "abcd";
  bool qrLinkVisibility = false;

  int isSubscribedZero = 0,
      isSubscribedOne = 0,
      isSubscribedTwo = 0,
      isSubscribedThree = 0;

  final PageRouteBuilder _homeRoute = new PageRouteBuilder(
    pageBuilder: (BuildContext context, _, __) {
      return DashboardScreen();
    },
  );

  GlobalKey globalKey = new GlobalKey();

  @override
  Widget build(BuildContext context) {
    print("hadeed ${widget._backgroundImg}");
    print("Hadeed Color ${widget.dataModel.foregroundColor}");
    print("Hadeed Color ${widget.dataModel.backgroundColor}");
    // TODO: implement build
    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.white),
          onPressed: () => Navigator.of(context).pop(),
        ),
        actions: <Widget>[
          FlatButton(
            textColor: Colors.white,
            onPressed: () => Navigator.pushAndRemoveUntil(
                context, _homeRoute, (Route<dynamic> r) => false),
            child:
                Text(AppLocalizations.of(context).translate("Finish_String")),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
        ],
        automaticallyImplyLeading: false,
        backgroundColor: Color(CustomColors().mainColorHard()),
      ),
      body: Builder(
        builder: (context) => Center(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Material(
              child: Padding(
                padding: const EdgeInsets.only(top: 50.0),
                child: SingleChildScrollView(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: 400,
                    ),
                    child: Column(
                      children: <Widget>[
                        Padding(
                            padding: const EdgeInsets.all(5.0),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                GestureDetector(
                                  onTap: () => mymethod(Constant.qr_url +
                                      widget.result['id'].toString()),
                                  child: RepaintBoundary(
                                    key: globalKey,
                                    child: Stack(
                                      children: <Widget>[
//                                         Container(
//                                           height: 300,
//                                           width: 300,
//                                           child: FittedBox(
//                                         fit: BoxFit.fill,
//                                           child: widget._backgroundImg != null
//                                               ? Image.asset(
//                                                   widget._backgroundImg.path)
//                                               : Container(
//                                             height: 300,
//                                             width: 300,
// //                                            color: Color(widget
// //                                              .dataModel.foregroundColor
// //                                          ),
//                                             color: Color(Colors.transparent.value
//                                             ),
//                                           ),
//                                         ),
//                                         ),
                                        Container(

                                          child: widget.result['image_url'] ==
                                                  null
                                              ? QrImage(
                                                  gapless: true,
                                                  version: QrVersions.auto,
                                                  backgroundColor: Color(widget
                                                      .dataModel
                                                      .foregroundColor),
                                                  foregroundColor: Color(widget
                                                      .dataModel
                                                      .backgroundColor),
//                                            embeddedImage: widget._image == null
//                                                ? AssetImage(
//                                                    'images/logo_main.png')
//                                                : FileImage(widget._image),
                                                  embeddedImageStyle:
                                                      QrEmbeddedImageStyle(
                                                    size: Size(40, 40),
                                                  ),
                                                  data: Constant.qr_url +
                                                      widget.result['id']
                                                          .toString(),
                                                  size: 300.0,
//                                      errorCorrectionLevel:
//                                          QrErrorCorrectLevel.L,
                                                )
                                              : Container(
                                                  width: MediaQuery.of(context)
                                                          .size
                                                          .width -
                                                      50,
                                                  child: CachedNetworkImage(
                                                    imageUrl: widget
                                                        .result['image_url'],
                                                    placeholder: (context,
                                                            url) =>
                                                        new CircularProgressIndicator(),
                                                    errorWidget: (context, url,
                                                            error) =>
                                                        new Icon(Icons.error),
                                                  ),
                                                ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            )),
                        Material(
                          borderRadius: BorderRadius.circular(20),
                          child: InkWell(
                            onTap: () async {},
                            child: InkWell(
                              onTap: () {
                                fetchUserDetail(context);
                              },
                              child: Container(
                                margin: const EdgeInsets.only(top: 19),
                                child: Material(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10.0)),
                                  color: Color(CustomColors().mainColorHard()),
                                  child: Padding(
                                      padding: const EdgeInsets.all(15.0),
                                      child: Text(
                                        AppLocalizations.of(context)
                                            .translate("Generate_Link_String"),
                                        style: TextStyle(color: Colors.white),
                                      )),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Visibility(
                          visible: qrLinkVisibility,
                          child: Container(
                            margin: const EdgeInsets.only(
                                left: 30.0, right: 30.0, top: 20.0),
                            width: MediaQuery.of(context).size.width,
                            height: 70,
                            decoration: new BoxDecoration(
                              border: new Border.all(
                                  color: Color(CustomColors().mainColorHard())),
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20.0)),
                            ),
                            child: Material(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20.0)),
                              color: Color(CustomColors().editTextBgColor()),
                              child: InkWell(
                                onTap: () {},
                                child: Padding(
                                  padding: const EdgeInsets.all(15.0),
                                  child: Linkify(
                                    textAlign: TextAlign.center,
                                    onOpen: (link) async {
                                      if (await canLaunch(link.url)) {
                                        await launch(link.url);
                                      } else {
                                        throw AppLocalizations.of(context)
                                                .translate(
                                                    "Could_not_launch_String") +
                                            '$link';
                                      }
                                    },
                                    text: Constant.qr_url +
                                        widget.result['id'].toString(),
                                    style: TextStyle(fontSize: 15),
                                    linkStyle: TextStyle(
                                      color:
                                          Color(CustomColors().mainColorHard()),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        Visibility(
                            visible: qrLinkVisibility,
                            child: Container(
                              child: Material(
                                  child: new RaisedButton(
                                onPressed: () {
//                                  Share.share(Constant.qr_url +
//                                      widget.result.toString());
//                                   ScreenshotShareImage.takeScreenshotShareImage();
//                                  _takeScreenshotandShare();
                                  Share.text(
                                      'Share Url',
                                      Constant.qr_url +
                                          widget.result['id'].toString(),
                                      'text/plain');
                                },
                                child: new Text(
                                  'Share Link',
                                  style: new TextStyle(
                                      fontWeight: FontWeight.normal,
                                      fontSize: 16.0,
                                      color: Colors.white),
                                ),
                                color: Color(CustomColors().mainColorHard()),
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(20.0)),
                              )),
                            )),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  // Future<void> _captureAndSharePng() async {
  //   try {
  //     RenderRepaintBoundary boundary =
  //         globalKey.currentContext.findRenderObject();
  //     var image = await boundary.toImage();
  //     ByteData byteData = await image.toByteData(format: ImageByteFormat.png);
  //     Uint8List pngBytes = byteData.buffer.asUint8List();

  //     final tempDir = await getTemporaryDirectory();
  //     final file = await new File('${tempDir.path}/image.png').create();
  //     await file.writeAsBytes(pngBytes);

  //     final channel = const MethodChannel('channel:me.alfian.share/share');
  //     channel.invokeMethod('shareFile', 'image.png');
  //   } catch (e) {
  //     print(e.toString());
  //   }
  // }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return null;
  }

  void fetchUserDetail(BuildContext context) async {
    progressShow(context);
    try {
      var fetchUserId = await SessionManager().getUserId();
      var userId = fetchUserId.toString();
      var params = {'hash': Constant.KEY, 'userId': userId};
      await HttpRequest()
          .fetchPost(Constant.get_user_detail, params)
          .then((response) {
        progressHide(context);
        if (response.statusCode == 200) {
          try {
            print(response.body);
            setState(() {
              var convertDataToJson = jsonDecode(response.body);
              isSubscribedZero =
                  int.parse(convertDataToJson['isSubscribedZero']);
              isSubscribedOne = int.parse(convertDataToJson['isSubscribedOne']);
              isSubscribedTwo = int.parse(convertDataToJson['isSubscribedTwo']);
              isSubscribedThree =
                  int.parse(convertDataToJson['isSubscribedThree']);

              print("$isSubscribedOne");

              if (widget.dataModel.name == 'text' ||
                  widget.dataModel.name == 'sms' ||
                  widget.dataModel.name == 'email' ||
                  widget.dataModel.name == 'website' ||
                  widget.dataModel.name == 'contact') {
                setState(() {
                  qrLinkVisibility = true;
                });
              } else if (widget.dataModel.name == 'text' ||
                  widget.dataModel.name == 'sms' ||
                  widget.dataModel.name == 'email' ||
                  widget.dataModel.name == 'website' ||
                  widget.dataModel.name == 'contact' ||
                  widget.dataModel.name == 'vcard') {
                setState(() {
                  qrLinkVisibility = true;
                });
              } else if (widget.dataModel.name == 'text' ||
                  widget.dataModel.name == 'sms' ||
                  widget.dataModel.name == 'email' ||
                  widget.dataModel.name == 'website' ||
                  widget.dataModel.name == 'contact' ||
                  widget.dataModel.name == 'vcard' ||
                  widget.dataModel.name == 'socialmedia') {
                setState(() {
                  qrLinkVisibility = true;
                });
              } else if (widget.dataModel.name == 'text' ||
                  widget.dataModel.name == 'sms' ||
                  widget.dataModel.name == 'email' ||
                  widget.dataModel.name == 'website' ||
                  widget.dataModel.name == 'contact' ||
                  widget.dataModel.name == 'vcard' ||
                  widget.dataModel.name == 'socialmedia' ||
                  widget.dataModel.name == 'invite') {
                setState(() {
                  qrLinkVisibility = true;
                });
              } else {
                showSnackBar(context,
                    AppLocalizations.of(context).translate("Buy_Any_Plan"));
              }
            });
          } catch (e) {
            print(e);
          }
        } else {
          print(response.statusCode);
        }
      });
    } catch (e) {
      print(e);
    }
  }

// fetch user link back u p
//  void fetchUserDetail(BuildContext context) async {
//    progressShow(context);
//    try {
//      var fetchUserId = await SessionManager().getUserId();
//      var userId = fetchUserId.toString();
//      var params = {'hash': Constant.KEY, 'userId': userId};
//      await HttpRequest()
//          .fetchPost(Constant.get_user_detail, params)
//          .then((response) {
//        progressHide(context);
//        if (response.statusCode == 200) {
//          try {
//            print(response.body);
//            setState(() {
//              var convertDataToJson = jsonDecode(response.body);
//              isSubscribedZero =
//                  int.parse(convertDataToJson['isSubscribedZero']);
//              isSubscribedOne = int.parse(convertDataToJson['isSubscribedOne']);
//              isSubscribedTwo = int.parse(convertDataToJson['isSubscribedTwo']);
//              isSubscribedThree =
//                  int.parse(convertDataToJson['isSubscribedThree']);
//
//              print("$isSubscribedOne");
//
//              if (isSubscribedZero == 1 &&
//                  (widget.dataModel.name == 'text' ||
//                      widget.dataModel.name == 'sms' ||
//                      widget.dataModel.name == 'email' ||
//                      widget.dataModel.name == 'website' ||
//                      widget.dataModel.name == 'contact')) {
//                setState(() {
//                  qrLinkVisibility = true;
//                });
//              } else if (isSubscribedOne == 1 &&
//                  (widget.dataModel.name == 'text' ||
//                      widget.dataModel.name == 'sms' ||
//                      widget.dataModel.name == 'email' ||
//                      widget.dataModel.name == 'website' ||
//                      widget.dataModel.name == 'contact' ||
//                      widget.dataModel.name == 'vcard')) {
//                setState(() {
//                  qrLinkVisibility = true;
//                });
//              } else if (isSubscribedTwo == 1 &&
//                  (widget.dataModel.name == 'text' ||
//                      widget.dataModel.name == 'sms' ||
//                      widget.dataModel.name == 'email' ||
//                      widget.dataModel.name == 'website' ||
//                      widget.dataModel.name == 'contact' ||
//                      widget.dataModel.name == 'vcard' ||
//                      widget.dataModel.name == 'socialmedia')) {
//                setState(() {
//                  qrLinkVisibility = true;
//                });
//              } else if (isSubscribedThree == 1 &&
//                  (widget.dataModel.name == 'text' ||
//                      widget.dataModel.name == 'sms' ||
//                      widget.dataModel.name == 'email' ||
//                      widget.dataModel.name == 'website' ||
//                      widget.dataModel.name == 'contact' ||
//                      widget.dataModel.name == 'vcard' ||
//                      widget.dataModel.name == 'socialmedia' ||
//                      widget.dataModel.name == 'invite')) {
//                setState(() {
//                  qrLinkVisibility = true;
//                });
//              } else {
//                showSnackBar(context,
//                    AppLocalizations.of(context).translate("Buy_Any_Plan"));
//              }
//            });
//          } catch (e) {
//            print(e);
//          }
//        } else {
//          print(response.statusCode);
//        }
//      });
//    } catch (e) {
//      print(e);
//    }
//  }

  ScreenshotController screenshotController = ScreenshotController();
  File _imageFile;

  _takeScreenshotandShare() async {
    _imageFile = null;
    screenshotController
        .capture(delay: Duration(milliseconds: 10), pixelRatio: 2.0)
        .then((File image) async {
      setState(() {
        _imageFile = image;
      });
      final directory = (await getApplicationDocumentsDirectory()).path;
      Uint8List pngBytes = _imageFile.readAsBytesSync();
      File imgFile = new File('$directory/screenshot.png');
      imgFile.writeAsBytes(pngBytes);
      print("File Saved to Gallery to: " + directory);
      await Share.file('Anupam', 'screenshot.png', pngBytes, 'image/png');
    }).catchError((onError) {
      print(onError);
    });
  }

  mymethod(String a) {
    return print(a);
  }
}
