import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:fluttertoast/fluttertoast.dart';
import '../UI/CustomInputTextField.dart';
import '../UI/CustomColors.dart';
import 'SignUpScreen.dart';
import '../Transitions/ScreenPageTransitions.dart';
import '../Http/HttpRequest.dart';
import '../UI/ProgressBar.dart';
import '../Validator/Validator.dart';
import '../Constant/Constant.dart';
import '../DTOs/UserDTO.dart';
import 'dart:convert' as json;
import '../UI/SnackBar.dart';
import '../SessionManager/SessionManager.dart';
import 'DashboardScreen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

//import 'package:progress_hud/progress_hud.dart';

//class  extends StatefulWidget{
//  LoginScreenState createState()=> LoginScreenState();
//}

class LoginScreen extends StatefulWidget {
  LoginScreenState createState() => LoginScreenState();
}

class LoginScreenState extends State<LoginScreen> {
  bool isLoading = false;
  Validator validator;
  final emailTextFieldController = TextEditingController();
  final passwordTextFieldController = TextEditingController();
  final emailController = TextEditingController();
  bool _validate = false;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Builder(
        builder: (context) => Center(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Color(CustomColors().mainColorHard()),
            child: Center(
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Material(
                        color: Color(CustomColors().logoBgColor()),
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Image.asset(
                            'images/logo4.png',
                            width: 200,
                            height: 70,
                            color: Colors.white,
                          ),
                        )),
                    //for email
                    CustomInputTextField(
                        Icon(Icons.person,
                            color: Color(CustomColors().mainColorHard())),
                        AppLocalizations.of(context).translate("Email_String"),
                        false,
                        TextInputType.emailAddress,
                        emailTextFieldController),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: CustomInputTextField(
                          Icon(Icons.lock,
                              color: Color(CustomColors().mainColorHard())),
                          AppLocalizations.of(context)
                              .translate("Password_String"),
                          true,
                          TextInputType.text,
                          passwordTextFieldController),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Material(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        color: Color(CustomColors().mainColorHard()),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            width: 370,
                            height: 60,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(33.0))),
                              onPressed: () async {
                                validator = new Validator(context);
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());

                                String _email =
                                    emailTextFieldController.text.toString();
                                String _password =
                                    passwordTextFieldController.text.toString();

                                if (validator.isEmailTrue(_email) &&
                                    validator.isPasswordTrue(_password)) {
                                  progressShow(context);
                                  try {
                                    var params = {
                                      'email': _email,
                                      'password': _password,
                                      'hash': Constant.KEY
                                    };
                                    await HttpRequest()
                                        .fetchPost(Constant.login, params)
                                        .then((response) {
                                      progressHide(context);
                                      print(response.statusCode);
                                      if (response.statusCode == 200) {
                                        try {
                                          print(response.body);
                                          UserDTO userDTO = UserDTO.fromJson(
                                              json.jsonDecode(response.body));

                                          SessionManager().setUserInfo(
                                              userDTO.userId,
                                              userDTO.firstName,
                                              userDTO.lastName,
                                              userDTO.userName,
                                              userDTO.email,
                                              userDTO.isSubscribedZero,
                                              userDTO.isSubscribedOne,
                                              userDTO.isSubscribedTwo,
                                              userDTO.isSubscribedThree,
                                              userDTO.isPremium);
                                          SessionManager().setUserImage(
                                              userDTO.profileImage);
                                          Firestore.instance
                                              .collection('users')
                                              .document(
                                                  userDTO.userId.toString())
                                              .setData({
                                            'nickname': userDTO.firstName +
                                                " " +
                                                userDTO.lastName,
                                            'photoUrl': userDTO.profileImage,
                                            'id': userDTO.userId.toString(),
                                            'createdAt': DateTime.now()
                                                .millisecondsSinceEpoch
                                                .toString(),
                                            'chattingWith': null
                                          });

                                          Navigator.pushReplacement(
                                              context,
                                              SlideRightRoute(
                                                  page: DashboardScreen()));
                                        } catch (e) {
                                          showSnackBar(context, e.toString());
                                        }
                                      } else if (response.statusCode == 403) {
                                        showSnackBar(
                                            context, "Incorrect Password!");
                                      } else if (response.statusCode == 404) {
                                        showSnackBar(
                                            context, "User doesn't exist.");
                                      } else {
                                        showSnackBar(
                                            context,
                                            AppLocalizations.of(context)
                                                .translate(
                                                    "Login_Failed_String"));
                                      }
                                    });
                                  } catch (e) {
                                    showSnackBar(context, e.toString());
                                  }
                                }
                              },
                              color: Colors.white,
                              textColor: Color(CustomColors().mainColorHard()),
                              child: Text(
                                AppLocalizations.of(context)
                                    .translate("Login_String"),
                                style: TextStyle(fontSize: 26.0),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.only(top: 8.0),
                      child: Row(children: <Widget>[
                        Expanded(
                            child: Align(
                                alignment: Alignment.centerLeft,
                                child: InkWell(
                                  onTap: () {
                                    Navigator.push(context,
                                        SlideRightRoute(page: SignUpScreen()));
                                  },
                                  child: Padding(
                                    padding: const EdgeInsets.only(left: 25),
                                    child: Text(
                                      AppLocalizations.of(context)
                                          .translate("Register_String"),
                                      style: TextStyle(
                                          fontSize: 18.0, color: Colors.white),
                                    ),
                                  ),
                                ))),
                        Expanded(
                            child: Align(
                                alignment: Alignment.centerRight,
                                child: Padding(
                                  padding: const EdgeInsets.only(right: 25),
                                  child: GestureDetector(
                                    onTap: () {
                                      showDialog(
                                          context: context,
                                          barrierDismissible: true,
                                          builder: (dialogContext) =>
                                              forgotPasswordDialog(
                                                  dialogContext));
                                    },
                                    child: Text(
                                      AppLocalizations.of(context)
                                          .translate("Forget_Password_String"),
                                      style: TextStyle(
                                          fontSize: 18.0, color: Colors.white),
                                    ),
                                  ),
                                ))),
                      ]),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 10.0),
                      child: Material(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                        color: Color(CustomColors().mainColorHard()),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                            width: 370,
                            height: 60,
                            child: RaisedButton(
                              shape: RoundedRectangleBorder(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(33.0))),
                              onPressed: () {
                                SessionManager().setUserInfo(
                                    0,
                                    AppLocalizations.of(context)
                                        .translate("guest_String"),
                                    ' ',
                                    'guest',
                                    'guest@guest.com',
                                    0,
                                    0,
                                    0,
                                    0,
                                    false);

                                Navigator.pushReplacement(context,
                                    SlideRightRoute(page: DashboardScreen()));
                              },
                              color: Colors.white,
                              textColor:
                                  Color(CustomColors().mainColorMedium()),
                              child: Text(
                                AppLocalizations.of(context)
                                    .translate("Continue_as_Guest_String"),
                                style: TextStyle(fontSize: 22.0),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget forgotPasswordDialog(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return AlertDialog(
      backgroundColor: Colors.transparent,
      contentPadding: EdgeInsets.all(0.0),
      content: Container(
        height: 250.0,
        width: screenSize.width * 0.9,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(20.0),
          color: Colors.white,
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(vertical: 10.0, horizontal: 10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                width: MediaQuery.of(context).size.width,
                child: Align(
                  alignment: Alignment.centerRight,
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                      emailController.clear();
                    },
                    child: Container(
                      height: 25.0,
                      width: 25.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color(CustomColors().mainColorHard()),
                      ),
                      child: Icon(
                        Icons.clear,
                        color: Colors.white,
                        size: 22.0,
                      ),
                    ),
                  ),
                ),
              ),
              Text(
                "Enter an email to send reset password link.",
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                    fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 5.0),
              Container(
                height: 55.0,
                width: screenSize.width * 0.7,
                decoration: BoxDecoration(
                  color: Color(0xffdadada),
                  borderRadius: BorderRadius.circular(20.0),
                ),
                child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 15.0, vertical: 10.0),
                  child: TextField(
                    controller: emailController,
                    keyboardType: TextInputType.emailAddress,
                    style: TextStyle(color: Colors.black.withOpacity(0.7)),
                    decoration: InputDecoration(
                      border: InputBorder.none,
                      hintText: "Enter an Email",
                      hintStyle: TextStyle(color: Colors.grey, fontSize: 14.0),
                      // errorText: _validate ? 'Check your email' : null,
                      // filled: true,
                    ),
                    onChanged: (value) {
                      setState(() {
                        _validate = emailController.text.isValidEmail();
                      });
                    },
                  ),
                ),
              ),
              SizedBox(height: 5.0),
              FlatButton(
                onPressed: () {
                  FocusScope.of(context).requestFocus(FocusNode());
                  String _email = emailController.text;

                  if (_validate) {
                    forgotPasswordMethod(context, _email);
                  } else {
                    Fluttertoast.showToast(
                        msg: "Email is invalid!",
                        toastLength: Toast.LENGTH_LONG,
                        gravity: ToastGravity.BOTTOM,
                        timeInSecForIos: 1);
                  }
                },
                child: Container(
                  height: 45.0,
                  width: screenSize.width * 0.4,
                  decoration: BoxDecoration(
                      color: Color(CustomColors().mainColorHard()),
                      border: Border.all(color: Colors.grey.withOpacity(0.3)),
                      borderRadius: BorderRadius.circular(50.0)),
                  child: Center(
                    child: Text(
                      "Done",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        letterSpacing: 0.8,
                        color: Colors.white,
                        fontSize: 16.0,
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(height: 5.0),
            ],
          ),
        ),
      ),
    );
  }

  forgotPasswordMethod(BuildContext context, String email) async {
    progressShow(context);
    try {
      var params = {'email': email, 'hash': Constant.KEY};
      await HttpRequest()
          .fetchPost(Constant.forgot_password, params)
          .then((response) {
        progressHide(context);
        print(response.statusCode);
        if (response.statusCode == 200) {
          try {
            emailController.clear();
            Fluttertoast.showToast(
                msg: "Email sent on your email.",
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 2);

            Navigator.pop(context);
            // print(response.body);
            // UserDTO userDTO = UserDTO.fromJson(json.jsonDecode(response.body));
          } catch (e) {
            Fluttertoast.showToast(
                msg: e.toString(),
                toastLength: Toast.LENGTH_LONG,
                gravity: ToastGravity.BOTTOM,
                timeInSecForIos: 2);
            // showSnackBar(context, e.toString());
          }
        } else {
          Fluttertoast.showToast(
              msg: "Email not found!",
              toastLength: Toast.LENGTH_LONG,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIos: 1);
        }
      });
    } catch (e) {
      Fluttertoast.showToast(
          msg: e.toString(),
          toastLength: Toast.LENGTH_LONG,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1);
      // showSnackBar(context, e.toString());
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    emailTextFieldController.dispose();
    passwordTextFieldController.dispose();
    super.dispose();
  }
}
