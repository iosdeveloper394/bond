import 'dart:io';
import 'package:bond_flutter/Constant/Constant.dart';
import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:bond_flutter/SessionManager/SessionManager.dart';
import 'package:bond_flutter/Transitions/ScreenPageTransitions.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import '../UI/CustomColors.dart';
import '../Http/HttpRequest.dart';
import 'dart:convert' as json;
import '../UI/SnackBar.dart';

class ShowUserProfileScreen extends StatefulWidget {
  @override
  String profileId;

  ShowUserProfileScreen(this.profileId);

  State createState() => ShowUserProfileState(profileId);
}
class ShowUserProfileState extends State<ShowUserProfileScreen> {
  List list = new List();
  bool isLoading = false;
  var currentProfile ;

  String profileId;

  ShowUserProfileState(this.profileId);



  Future<int> getCurrentUserProfile() async {
    int statuscode = 401;
    setState(() {
      isLoading = true;
    });
    try {
      var fetchUserId = await SessionManager().getUserId();
      var  userId = fetchUserId.toString();
      var params = {
        'hash': Constant.KEY,
        'userId': userId,
        'profileId': profileId
      };
      await HttpRequest().fetchPost(Constant.get_user_profile,params).then((response) {
        setState(() {
          isLoading = false;
        });
        print (response.statusCode);
        if (response.statusCode == 200) {
          try {
            print(response.body);
            setState(() {
              var convertDataToJson = json.jsonDecode(response.body);
              currentProfile = convertDataToJson['result'];
              statuscode =  200;
            });
          }
          catch (e){
            print(e);
            statuscode =  401;
          }
        }else {
          print(response.statusCode);
          statuscode =  401;
        }
      });

    }catch(e){
      print(e);
      statuscode =  401;
    }
    return statuscode ;
  }

  Future<int> addFriend() async {
    int statuscode = 401;
    setState(() {
      isLoading = true;
    });
    try {
      var fetchUserId = await SessionManager().getUserId();
      var  userId = fetchUserId.toString();
      var params = {
        'hash': Constant.KEY,
        'userId': userId,
        'profileId': profileId
      };
      await HttpRequest().fetchPost(Constant.add_friend_for_user,params).then((response) {
        setState(() {
          isLoading = false;
        });
        print (response.statusCode);
        if (response.statusCode == 200) {
          try {
            print(response.body);
            setState(() {
              var convertDataToJson = json.jsonDecode(response.body);
              currentProfile = convertDataToJson['result'];
              statuscode =  200;
            });
          }
          catch (e){
            print(e);
            statuscode =  401;
          }
        }else {
          print(response.statusCode);
          statuscode =  401;
        }
      });

    }catch(e){
      print(e);
      statuscode =  401;
    }
    return statuscode ;
  }

  Future<int> deleteFriend() async {
    int statuscode = 401;
    setState(() {
      isLoading = true;
    });
    try {
      var fetchUserId = await SessionManager().getUserId();
      var  userId = fetchUserId.toString();
      var params = {
        'hash': Constant.KEY,
        'userId': userId,
        'profileId': profileId
      };
      await HttpRequest().fetchPost(Constant.delete_user_friend,params).then((response) {
        setState(() {
          isLoading = false;
        });
        print (response.statusCode);
        if (response.statusCode == 200) {
          try {
            print(response.body);
            setState(() {
              var convertDataToJson = json.jsonDecode(response.body);
              currentProfile = convertDataToJson['result'];
              statuscode =  200;
            });
          }
          catch (e){
            print(e);
            statuscode =  401;
          }
        }else {
          print(response.statusCode);
          statuscode =  401;
        }
      });

    }catch(e){
      print(e);
      statuscode =  401;
    }
    return statuscode ;
  }
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getCurrentUserProfile();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar:  new AppBar(
          title: new Text(AppLocalizations.of(context).translate("Profile_String"),
            style: TextStyle(
                fontSize: 22
            ),),
          backgroundColor: Color(CustomColors().mainColorHard()),
          automaticallyImplyLeading: true,
        ),
        body: Builder(
          builder: (context)=>
              Center(
                child: Container(
                    child: Stack(
                      children: <Widget>[

                        Container(
                          child: Material(
                            child: SingleChildScrollView(
                              child: ConstrainedBox(
                                constraints: BoxConstraints(
                                  minHeight: 400,
                                ),
                                child: Column(
                                  children: <Widget>[
                                    Stack(
                                      children: <Widget>[
                                        Column(
                                          children: <Widget>[
                                            Container(
                                              height: 250,
                                              color: Color(CustomColors().mainColorHard()),
                                            ),
                                            Container(
                                              height: 150,
                                              color: Color(CustomColors().dimWhiteColor()),
                                            )
                                          ],
                                        ),
                                        Positioned(
                                          child: Center(
                                            child: Padding(
                                              padding: const EdgeInsets.only(top:130.0),
                                              child:currentProfile != null
                                                  ?  Container(
                                                child: Material(
                                                  child:  currentProfile['profileImage'] != null
                                                      ? CachedNetworkImage(
                                                    placeholder: (context, url) => Container(
                                                      child: CircularProgressIndicator(
                                                        strokeWidth: 1.0,
                                                        valueColor: AlwaysStoppedAnimation<Color>(themeColor),
                                                      ),
                                                      width: 160.0,
                                                      height: 160.0,
                                                      padding: EdgeInsets.all(15.0),
                                                    ),
                                                    imageUrl: currentProfile['profileImage'],
                                                    width: 160.0,
                                                    height: 160.0,
                                                    fit: BoxFit.cover,
                                                  )
                                                      :Icon(
                                                    Icons.account_circle,
                                                    size: 160.0,
                                                    color: Colors.white,
                                                  ),
                                                  borderRadius: BorderRadius.all(Radius.circular(35.0)),
                                                  clipBehavior: Clip.hardEdge,
                                                  color: Color(CustomColors().mainColorHard()),
                                                ),
                                              ): Icon(
                                                Icons.account_circle,
                                                size: 160.0,
                                                color: Colors.white,
                                              ),

                                            ),
                                          ),
                                        ),

                                      ],
                                    ),
                                    Center(
                                      child: currentProfile != null
                                          ?  Container(
                                        child: Material(
                                          child:  currentProfile['firstName'] != null
                                              ? Text(currentProfile['firstName']+" "+currentProfile['lastName'],
                                            style: TextStyle(
                                              fontSize: 28,
                                              color: Color(CustomColors().mainColorHard())
                                          ),)
                                              :Container(),
                                        ),
                                      ): Container(),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top:50.0),
                                      child: Row(
                                        mainAxisAlignment:  MainAxisAlignment.spaceEvenly,
                                        children: currentProfile != null? <Widget>[
                                          Column(
                                            children: <Widget>[
                                              Text(currentProfile['noOfGeneratedQR'],
                                                style: TextStyle(
                                                  fontSize: 25,
                                                  color: Color(CustomColors().mainColorHard())
                                                ),),
                                              Text(AppLocalizations.of(context).translate("Generated_QR_String"),
                                                  style: TextStyle(
                                                      fontSize: 20,
                                                      color: Color(CustomColors().mainColorHard())
                                                  ))
                                            ],
                                          ),
                                          Container(
                                            height: 20,
                                            width: 2,
                                            color: Color(CustomColors().mainColorHard()),
                                          ),
                                          Column(
                                            children: <Widget>[
                                              Text(currentProfile['noOfFriends'],
                                                style: TextStyle(
                                                    fontSize: 25,
                                                    color: Color(CustomColors().mainColorHard())
                                                ),),
                                              Text(AppLocalizations.of(context).translate("Friends_String"),
                                                  style: TextStyle(
                                                      fontSize: 20,
                                                      color: Color(CustomColors().mainColorHard())
                                                  ))
                                            ],
                                          ),

                                        ]:<Widget>[
                                          Container()
                                        ],
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(top:30.0),
                                      child: Container(
                                        child:
                                          currentProfile != null?
                                          Center(
                                            child:
                                            currentProfile['isFriends'] ==false?
                                            RaisedButton(child: Text(AppLocalizations.of(context).translate("Add_Friend_String")),
                                              onPressed: (){
                                                addFriend();
                                              },
                                              color: Color(CustomColors().mainColorHard()),
                                              textColor: Colors.white,
                                              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                              splashColor: Colors.grey,
                                            ): RaisedButton(child: Text(AppLocalizations.of(context).translate("Remove_Friend_String")),
                                              onPressed: (){
                                                  deleteFriend(); 
                                              },
                                              color: Color(CustomColors().mainColorHard()),
                                              textColor: Colors.white,
                                              padding: EdgeInsets.fromLTRB(10, 10, 10, 10),
                                              splashColor: Colors.grey,
                                            ),
                                          ): Container()
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),),
                        Positioned(
                          child: isLoading
                              ? Container(
                            child: Center(
                              child: CircularProgressIndicator(valueColor: AlwaysStoppedAnimation<Color>(themeColor)),
                            ),
                            color: Colors.white.withOpacity(0.8),
                          )
                              : Container(),
                        )
                      ],
                    )
                ),
              ),
        )
    );
  }

}
