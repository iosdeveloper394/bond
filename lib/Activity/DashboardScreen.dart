import 'dart:async';
import 'dart:io';
import 'dart:convert';
import 'package:bond_flutter/Activity/CurrentLocation.dart';
import 'package:bond_flutter/Activity/ScanQRCodeScreen.dart';
import 'package:bond_flutter/Activity/SelectPackage.dart';
import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:location/location.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:url_launcher/url_launcher.dart';
import '../UI/CustomColors.dart';
import '../Transitions/ScreenPageTransitions.dart';
import '../SessionManager/SessionManager.dart';
import 'MyQrListScreen.dart';
import 'LoginScreen.dart';
import 'PremiumScreen.dart';
import 'QrResultScreen.dart';
import 'SettingScreen.dart';
import 'UserListScreen.dart';
import 'ChooseOptionScreen.dart';
import 'ChatUserListScreen.dart';
import 'MyFriendsScreen.dart';
import '../DTOs/SendPaymentRequest.dart';
import '../Http/HttpRequest.dart';
import '../UI/ProgressBar.dart';
import '../UI/SnackBar.dart';
import 'package:bond_flutter/Constant/Constant.dart';

//import 'package:qrcode_reader/qrcode_reader.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/widgets.dart';

class DashboardScreen extends StatefulWidget {
  DashboardScreenState createState() => DashboardScreenState();
}

class DashboardScreenState extends State<DashboardScreen>
    with WidgetsBindingObserver, SingleTickerProviderStateMixin {
  bool isTOSChecked = false, isPremium = false;
  String fullName = "";
  String email = "";
  int userId = 0;
  String profileImage = "";
  String lat = "";
  String long = "";

  Animation animation;
  AnimationController animationController;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  Barcode result;
  QRViewController controller;

  Location location = Location();
  static bool _status;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if (state == AppLifecycleState.resumed) {
      SessionManager().getUserImage().then((onValue) {
        debugPrint(onValue);
        profileImage = onValue;
        setState(() {});
      });
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    getUserdataFromCache();

    animationController =
        AnimationController(duration: Duration(seconds: 5), vsync: this);

    animation = Tween(begin: -0.1, end: 0.0).animate(
      CurvedAnimation(parent: animationController, curve: Curves.fastOutSlowIn),
    );
    animationController.forward();
  }

  @override
  void dispose() {
    controller?.dispose();
    animationController?.dispose();
    super.dispose();
  }

  getUserdataFromCache() async {
    fullName = await SessionManager().getUserFullname();
    email = await SessionManager().getUserEmail();
    userId = await SessionManager().getUserId();
    profileImage = await SessionManager().getUserImage();
    isPremium = await SessionManager().getisPremium();
    setState(() {});

    debugPrint(fullName);
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return AnimatedBuilder(
      animation: animationController,
      builder: (BuildContext context, Widget child) {
        return Scaffold(
          // resizeToAvoidBottomPadding: false,
          key: _scaffoldKey,
          appBar: new AppBar(
            title: new Text(
              AppLocalizations.of(context).translate("Dashboard_String"),
              style: TextStyle(fontSize: 22),
            ),
            backgroundColor: Color(CustomColors().mainColorHard()),
            automaticallyImplyLeading: true,
          ),
          body: Builder(
            builder: (context) => Center(
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Material(
                  child: Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: SingleChildScrollView(
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                          minHeight: 400,
                        ),
                        child: Column(
                          children: <Widget>[
                            Container(
                              margin: EdgeInsets.only(
                                  top: 10.0,
                                  bottom: 10.0,
                                  left: 20.0,
                                  right: 10.0),
                              // width: 320.0,
                              // decoration: BoxDecoration(
                              //   color: Colors.white,
                              //   borderRadius: BorderRadius.circular(15.0),
                              //   border: Border.all(
                              //     width: 1.0,
                              //     color: Colors.grey[200],
                              //   ),
                              // ),
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Expanded(
                                    child: Row(
                                      children: <Widget>[
                                        ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                          child: CachedNetworkImage(
                                              imageUrl: "$profileImage",
                                              placeholder: (context, url) =>
                                                  CircularProgressIndicator(),
                                              errorWidget:
                                                  (context, url, error) => Icon(
                                                        Icons.account_circle,
                                                        size: 50.0,
                                                      ),
                                              height: 80,
                                              width: 80),
                                        ),
                                        Expanded(
                                          child: Container(
                                            margin: EdgeInsets.all(12.0),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text(
                                                  fullName.toUpperCase(),
                                                  style: TextStyle(
                                                    fontSize: 18.0,
                                                    fontWeight: FontWeight.bold,
                                                    color: Color(CustomColors()
                                                        .mainColorHard()),
                                                  ),
                                                  overflow:
                                                      TextOverflow.ellipsis,
                                                ),
                                                SizedBox(height: 4.0),
                                                // Text(
                                                //   order.restaurant.name,
                                                //   style: TextStyle(
                                                //     fontSize: 16.0,
                                                //     fontWeight: FontWeight.w600,
                                                //   ),
                                                //   overflow:
                                                //       TextOverflow.ellipsis,
                                                // ),
                                                // SizedBox(height: 4.0),
                                                // Text(
                                                //   order.date,
                                                //   style: TextStyle(
                                                //     fontSize: 16.0,
                                                //     fontWeight: FontWeight.w600,
                                                //   ),
                                                //   overflow:
                                                //       TextOverflow.ellipsis,
                                                // ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(right: 20.0),
                                    width: 40.0,
                                    height: 40.0,
                                    decoration: BoxDecoration(
                                      color:
                                          Color(CustomColors().mainColorHard()),
                                      borderRadius: BorderRadius.circular(20.0),
                                    ),
                                    child: IconButton(
                                      icon: Icon(Icons.logout),
                                      iconSize: 20.0,
                                      color: Colors.white,
                                      onPressed: () {
                                        SessionManager().Logout();
                                        Navigator.pop(context);
                                        Navigator.push(
                                            context,
                                            SlideRightRoute(
                                                page: LoginScreen()));
                                      },
                                    ),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(right: 20.0),
                                    width: 40.0,
                                    height: 40.0,
                                    decoration: BoxDecoration(
                                      color:
                                          Color(CustomColors().mainColorHard()),
                                      borderRadius: BorderRadius.circular(20.0),
                                    ),
                                    child: IconButton(
                                      icon: Icon(Icons.settings),
                                      iconSize: 20.0,
                                      color: Colors.white,
                                      onPressed: () async {
                                        if (userId == 0) {
                                          showSnackBar(
                                              context,
                                              AppLocalizations.of(context)
                                                  .translate(
                                                      "Login_To_Enter_String"));
                                          return;
                                        }
                                        await Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    SettingScreen()));
                                        setState(() {
                                          SessionManager()
                                              .getUserImage()
                                              .then((onValue) {
                                            debugPrint(onValue);
                                            profileImage = onValue;
                                          });
                                        });
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 16.0),
                              child: Container(
                                color: Color(CustomColors().mainColorHard()),
                                child: Padding(
                                  padding: const EdgeInsets.all(20.0),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Material(
                                        child: InkWell(
                                          onTap: () {
                                            if (userId == 0) {
                                              showSnackBar(
                                                  context,
                                                  AppLocalizations.of(context)
                                                      .translate(
                                                          "Login_To_Enter_String"));
                                              return;
                                            }
                                            Navigator.push(
                                                context,
                                                SlideRightRoute(
                                                    page: UserListScreen()));
                                          },
                                          child: Container(
                                            height: 70,
                                            width: 85,
                                            color: Color(
                                                CustomColors().dimWhiteColor()),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                //                                            Text("12",  style: TextStyle(
                                                //                                                fontSize: 17
                                                //                                            ),),
                                                Text(
                                                  AppLocalizations.of(context)
                                                      .translate(
                                                          "Users_String"),
                                                  style:
                                                      TextStyle(fontSize: 15),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Material(
                                        child: InkWell(
                                          onTap: () {
                                            if (userId == 0) {
                                              showSnackBar(
                                                  context,
                                                  AppLocalizations.of(context)
                                                      .translate(
                                                          "Login_To_Enter_String"));
                                              return;
                                            }
                                            Navigator.push(
                                                context,
                                                SlideRightRoute(
                                                    page: MyFriendScreen()));
                                          },
                                          child: Container(
                                            height: 70,
                                            width: 85,
                                            color: Color(
                                                CustomColors().dimWhiteColor()),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                //                                          Text("103",  style: TextStyle(
                                                //                                              fontSize: 17
                                                //                                          ),),
                                                Text(
                                                  AppLocalizations.of(context)
                                                      .translate(
                                                          "Friends_String"),
                                                  style:
                                                      TextStyle(fontSize: 15),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Material(
                                        child: InkWell(
                                          onTap: () {
                                            if (userId == 0) {
                                              showSnackBar(
                                                  context,
                                                  AppLocalizations.of(context)
                                                      .translate(
                                                          "Login_To_Enter_String"));
                                              return;
                                            }
                                            Navigator.push(
                                                context,
                                                SlideRightRoute(
                                                    page: MyQrListScreen()));
                                          },
                                          child: Container(
                                            height: 70,
                                            width: 85,
                                            color: Color(
                                                CustomColors().dimWhiteColor()),
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                //                                            Text("12",  style: TextStyle(
                                                //                                                fontSize: 17
                                                //                                            ),),
                                                Text(
                                                  AppLocalizations.of(context)
                                                      .translate(
                                                          "Generated_String"),
                                                  style:
                                                      TextStyle(fontSize: 15),
                                                ),
                                                Text(
                                                  AppLocalizations.of(context)
                                                      .translate("QR_String"),
                                                  style:
                                                      TextStyle(fontSize: 15),
                                                )
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: 15, right: 15, top: 20),
                              child: ClipRRect(
                                borderRadius: BorderRadius.circular(25.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Color(
                                            CustomColors().dimGreyColor()),
                                        width: 2.0),
                                    color: Color(CustomColors().dimGreyColor()),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(20),
                                    ),
                                    boxShadow: <BoxShadow>[
                                      new BoxShadow(blurRadius: 0.5),
                                    ],
                                  ),
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    children: <Widget>[
                                      Visibility(
                                        visible:
                                            isPremium == true ? true : false,
                                        child: Padding(
                                          padding: const EdgeInsets.all(0.0),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: Container(
                                              height: 65,
                                              width: 75,
                                              child: Material(
                                                child: InkWell(
                                                  splashColor: Color(
                                                      CustomColors()
                                                          .mainColorHard()),
                                                  onTap: () {
                                                    if (userId == 0) {
                                                      showSnackBar(
                                                          context,
                                                          AppLocalizations.of(
                                                                  context)
                                                              .translate(
                                                                  "Login_To_Enter_String"));
                                                      return;
                                                    }
                                                    navigateToSelectpackage();
                                                  },
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Container(
                                                        child: Image.asset(
                                                          'images/buy_premium.png',
                                                          width: 30,
                                                          height: 30,
                                                          color: Color(
                                                              CustomColors()
                                                                  .mainColorHard()),
                                                        ),
                                                      ),
                                                      Text(
                                                        AppLocalizations.of(
                                                                context)
                                                            .translate(
                                                                "Buy_String"),
                                                        style: TextStyle(
                                                            fontSize: 12),
                                                      ),
                                                      Text(
                                                        AppLocalizations.of(
                                                                context)
                                                            .translate(
                                                                "Premium_String"),
                                                        style: TextStyle(
                                                            fontSize: 12),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      // Visibility(
                                      //     visible: isPremium == true ? false : true,
                                      //     child: Padding(
                                      //       padding: const EdgeInsets.all(0.0),
                                      //       child: ClipRRect(
                                      //         borderRadius:
                                      //             BorderRadius.circular(10),
                                      //         child: Container(
                                      //           height: 75,
                                      //           width: 85,
                                      //           child: Material(
                                      //             child: InkWell(
                                      //               splashColor: Color(
                                      //                   CustomColors()
                                      //                       .mainColorHard()),
                                      //               onTap: () {
                                      //                 if (userId == 0) {
                                      //                   showSnackBar(
                                      //                       context,
                                      //                       AppLocalizations.of(
                                      //                               context)
                                      //                           .translate(
                                      //                               "Login_To_Enter_String"));
                                      //                   return;
                                      //                 }
                                      //                 navigateToCurrentLocation(
                                      //                     context);
                                      //               },
                                      //               child: Column(
                                      //                 mainAxisAlignment:
                                      //                     MainAxisAlignment.center,
                                      //                 children: <Widget>[
                                      //                   Container(
                                      //                     child: Image.asset(
                                      //                       'images/location_pin.png',
                                      //                       width: 30,
                                      //                       height: 30,
                                      //                       color: Color(
                                      //                           CustomColors()
                                      //                               .mainColorHard()),
                                      //                     ),
                                      //                   ),
                                      //                   Text(
                                      //                     AppLocalizations.of(
                                      //                             context)
                                      //                         .translate(
                                      //                             "Current_String"),
                                      //                     style: TextStyle(
                                      //                         fontSize: 12),
                                      //                   ),
                                      //                   Text(
                                      //                     AppLocalizations.of(
                                      //                             context)
                                      //                         .translate(
                                      //                             "Location_String"),
                                      //                     style: TextStyle(
                                      //                         fontSize: 12),
                                      //                   )
                                      //                 ],
                                      //               ),
                                      //             ),
                                      //           ),
                                      //         ),
                                      //       ),
                                      //     )),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            bottom: 60.0, top: 10),
                                        child: ClipRRect(
                                          borderRadius:
                                              BorderRadius.circular(10),
                                          child: Container(
                                            height: 65,
                                            width: 75,
                                            child: Material(
                                              child: InkWell(
                                                splashColor: Color(
                                                    CustomColors()
                                                        .mainColorHard()),
                                                onTap: () {
                                                  Navigator.push(
                                                      context,
                                                      SlideRightRoute(
                                                          page:
                                                              ChooseOptionScreen()));
                                                },
                                                child: Column(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment.center,
                                                  children: <Widget>[
                                                    Container(
                                                      child: Image.asset(
                                                        'images/qr_icon2.png',
                                                        width: 30,
                                                        height: 30,
                                                        color: Color(
                                                            CustomColors()
                                                                .mainColorHard()),
                                                      ),
                                                    ),
                                                    Text(
                                                      AppLocalizations.of(
                                                              context)
                                                          .translate(
                                                              "Generate_String"),
                                                      style: TextStyle(
                                                          fontSize: 12),
                                                    ),
                                                    Text(
                                                      AppLocalizations.of(
                                                              context)
                                                          .translate(
                                                              "QR_String"),
                                                      style: TextStyle(
                                                          fontSize: 12),
                                                    )
                                                  ],
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: Container(
                                          height: 65,
                                          width: 75,
                                          color: Colors.white,
                                          child: InkWell(
                                            onTap: () async {
                                              if (userId == 0) {
                                                showSnackBar(
                                                    context,
                                                    AppLocalizations.of(context)
                                                        .translate(
                                                            "Login_To_Enter_String"));
                                                return;
                                              }
                                              var fetchUserId =
                                                  await SessionManager()
                                                      .getUserId();
                                              String currentUserId =
                                                  fetchUserId.toString();
                                              Navigator.push(
                                                  context,
                                                  SlideRightRoute(
                                                      page: ChatUserListScreen(
                                                          currentUserId:
                                                              currentUserId)));
                                            },
                                            child: Column(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                              children: <Widget>[
                                                Container(
                                                  child: Image.asset(
                                                    'images/chat_icon.png',
                                                    width: 50,
                                                    height: 50,
                                                    color: Color(CustomColors()
                                                        .mainColorHard()),
                                                  ),
                                                ),
                                                Text(
                                                  AppLocalizations.of(context)
                                                      .translate("Chat_String"),
                                                  style:
                                                      TextStyle(fontSize: 12),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      Transform(
                                        transform: Matrix4.translationValues(
                                            animation.value, 0.0, 0.0),
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              bottom: 60.0, top: 10),
                                          child: ClipRRect(
                                            borderRadius:
                                                BorderRadius.circular(10),
                                            child: Container(
                                              height: 65,
                                              width: 75,
                                              child: Material(
                                                child: InkWell(
                                                  splashColor: Color(
                                                      CustomColors()
                                                          .mainColorHard()),
                                                  onTap: () async {
                                                    if (userId == 0) {
                                                      showSnackBar(
                                                          context,
                                                          AppLocalizations.of(
                                                                  context)
                                                              .translate(
                                                                  "Login_To_Enter_String"));
                                                      return;
                                                    }
                                                    if (_status == true) {
                                                      _getLocation(context);
                                                    } else {
                                                      alertDialogue();
                                                    }
                                                  },
                                                  child: Column(
                                                    mainAxisAlignment:
                                                        MainAxisAlignment
                                                            .center,
                                                    children: <Widget>[
                                                      Container(
                                                        child: Image.asset(
                                                          'images/qr_icon2.png',
                                                          width: 30,
                                                          height: 30,
                                                          color: Color(
                                                              CustomColors()
                                                                  .mainColorHard()),
                                                        ),
                                                      ),
                                                      Text(
                                                        AppLocalizations.of(
                                                                context)
                                                            .translate(
                                                                "Read_String"),
                                                        style: TextStyle(
                                                            fontSize: 12),
                                                      ),
                                                      Text(
                                                        AppLocalizations.of(
                                                                context)
                                                            .translate(
                                                                "QR_String"),
                                                        style: TextStyle(
                                                            fontSize: 12),
                                                      )
                                                    ],
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return null;
  }

  navigateToSelectpackage() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => SelectPackage()),
    );
  }

  void navigateToCurrentLocation(BuildContext context) async {
    if (await location.hasPermission() != null) {
      final pos = await location.getLocation();
      setState(() {
        lat = pos.latitude.toString();
        long = pos.longitude.toString();

        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => CurrentLocationScreen(lat, long)),
        );
      });
    } else {
      await location.requestPermission();
    }
  }

  void alertDialogue() {
    Alert(
      context: context,
      type: AlertType.warning,
      title: "ALERT",
      desc:
          "To continue you need to give location permission. The system needs your location to identify from where the QR scanned.",
      buttons: [
        DialogButton(
          child: Text(
            "Cancel",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () => Navigator.pop(context),
          color: Color.fromRGBO(0, 179, 134, 1.0),
        ),
        DialogButton(
          child: Text(
            "OK",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          onPressed: () {
            Navigator.pop(context);
            _getLocation(context);
          },
          color: Color(CustomColors().mainColorHard()),
        )
      ],
    ).show();
  }

  void _getLocation(BuildContext context) async {
    if (await location.hasPermission() != null) {
      final pos = await location.getLocation();
      setState(() {
        _status = true;
        lat = pos.latitude.toString();
        long = pos.longitude.toString();

        print("$lat, $long");
        moveToScanScreen(context);
      });
    } else {
      await location.requestPermission();
      _status = true;
      print(_status);
    }
  }

  readQR(String result) {
    if (Platform.isAndroid) {
      result = result + '&&platform=android' + "&&location=$lat,$long";
    } else if (Platform.isIOS) {
      result = result + '&&platform=ios' + "&&location=$lat,$long";
    }

    _launchURL(result);

    // Navigator.push(
    //   context,
    //   MaterialPageRoute(builder: (context) => QrResultScreen(result)),
    // );
  }

  _launchURL(link) async {
    if (await canLaunch(link)) {
      await launch(link);
    } else {
      throw 'Could not launch $link';
    }
  }

  moveToScanScreen(BuildContext context) async {
    final scannedResult = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => ScanQRCodeScreen(),
      ),
    );

    if (scannedResult != null) {
      // print("result is: $scannedResult");

      if (scannedResult
          .toString()
          .contains(new RegExp(r'bondscode', caseSensitive: false))) {
        readQR(scannedResult);
      } else if (scannedResult
          .toString()
          .contains(new RegExp(r'VQR.TW', caseSensitive: false))) {
          var link = Constant.qr_url + scannedResult;
          readQR(link);
      } else {
        Fluttertoast.showToast(
          msg: "Sorry! This QR doesn't exist in Bond system.",
          toastLength: Toast.LENGTH_LONG,
        );
      }
    } else {
      print("result is: nullll");
    }
  }
}
