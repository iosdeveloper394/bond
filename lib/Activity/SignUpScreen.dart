import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:flutter/material.dart';
import '../UI/CustomSimpleInputTextField.dart';
import '../UI/CustomColors.dart';
import '../Validator/Validator.dart';
import '../UI/SnackBar.dart';
import '../Http/HttpRequest.dart';
import '../UI/ProgressBar.dart';
import '../Constant/Constant.dart';

class SignUpScreen extends StatefulWidget{
  SignUpScreenState createState()=> SignUpScreenState();
}

class SignUpScreenState extends State<SignUpScreen> {
  bool isTOSChecked = false ;

  final firstNameTextFieldController = TextEditingController();
  final lastNameTextFieldController = TextEditingController();
  final emailTextFieldController = TextEditingController();
  final userNameTextFieldController = TextEditingController();
  final passwordTextFieldController = TextEditingController();
  final confirmPasswordTextFieldController = TextEditingController();

  Validator validator;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
     // resizeToAvoidBottomPadding: false,
      appBar:  new AppBar(
        title: new Text(AppLocalizations.of(context).translate("Sign_Up_String"),
        style: TextStyle(
          fontSize: 22
        ),),
        backgroundColor: Color(CustomColors().mainColorHard()),
      ),
      body: Builder(
        builder: (context)=>
        Center(
        child: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Color(CustomColors().dimWhiteColor()),
          child: Material(
            child: Padding(
              padding: const EdgeInsets.only(top: 20.0),
              child: SingleChildScrollView(
              child: ConstrainedBox(
              constraints: BoxConstraints(
                      minHeight: 400,
                      ),
                                child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                                Padding(
                                padding: const EdgeInsets.all(15.0),
                child: Image.asset(
                     'images/signup_icon.png',
                  width: 300,
                  height: 120,
                ),
              ), CustomSimpleInputTextField(
                        AppLocalizations.of(context).translate("First_Name_String"),
                        false,TextInputType.text,firstNameTextFieldController),
                    CustomSimpleInputTextField(
                        AppLocalizations.of(context).translate("Last_Name_String"),
                        false,TextInputType.text,lastNameTextFieldController),
                    CustomSimpleInputTextField(
                        AppLocalizations.of(context).translate("Email_String"),
                        false,TextInputType.emailAddress,emailTextFieldController),
                    CustomSimpleInputTextField(
                        AppLocalizations.of(context).translate("Username_String"),
                        false,TextInputType.text,userNameTextFieldController),
                    CustomSimpleInputTextField(
                        AppLocalizations.of(context).translate("Password_String"),
                        true,TextInputType.text,passwordTextFieldController),
                    CustomSimpleInputTextField(
                        AppLocalizations.of(context).translate("Confirm_Password_String"),
                        true,TextInputType.text,confirmPasswordTextFieldController),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        children: <Widget>[
                          Checkbox(
                            value: isTOSChecked ,
                            onChanged: (value) {
                              setState(() {
                                isTOSChecked = value;
                              });
                            },
                          ),
                          Text(AppLocalizations.of(context).translate("Agree_String"),
                            style: TextStyle(
                                fontSize: 16
                            ),),
                          Text(AppLocalizations.of(context).translate("Term_Services_String"),
                              style: TextStyle(
                              fontSize: 16,
                                color: Color(CustomColors().mainColorHard())
                          )),
                        ],
                      ),
                    ),
                    Material(
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),

                      child: Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Container(
                          width: 170,
                          height: 60,
                          child: RaisedButton(
                            shape: RoundedRectangleBorder(
                                borderRadius:
                                BorderRadius.all(Radius.circular(33.0))),

                            onPressed: ()async {

                              if (!isTOSChecked){
                                showSnackBar(context, AppLocalizations.of(context).translate("Accept_Conditions_String"));
                                return;
                              }
                              String _firstName = firstNameTextFieldController.text;
                              String _lastName = lastNameTextFieldController.text;
                              String _userName = userNameTextFieldController.text;
                              String _email = emailTextFieldController.text;
                              String _password = passwordTextFieldController.text;
                              String _confirmPassword = confirmPasswordTextFieldController.text;

                              validator = Validator(context);

                              if (validator.isFirstNameTrue(_firstName)&&
                                  validator.isLastNameTrue(_lastName) &&
                                  validator.isEmailTrue(_email) &&
                                  validator.isUserNameTrue(_userName) &&
                                  validator.isPasswordTrue(_password)){

                                if (_password != _confirmPassword){
                                  showSnackBar(context, AppLocalizations.of(context).translate("Confirm_Password_Not_Matched_String"));
                                  return;
                                }else {

                                  progressShow(context);
                                  try {
                                    var params = {
                                      'fname': _firstName,
                                      'lname': _lastName,
                                      'username': _userName,
                                      'email': _email,
                                      'password': _password,
                                      'hash': Constant.KEY
                                    };
                                    await HttpRequest().fetchPost(Constant.registration,params).then((response) {
                                      if (response.statusCode == 200) {
                                        try {
                                          showSnackBar(context
                                              , AppLocalizations.of(context).translate("Registered_String"));
                                          Navigator.pop(context);
                                        }
                                        catch (e){
                                          showSnackBar(context, e.toString());
                                        }
                                      }else if (response.statusCode == 410) {
                                        showSnackBar(context, AppLocalizations.of(context).translate("User_Exist_String"));
                                      }else if (response.statusCode == 409){
                                        showSnackBar(context, AppLocalizations.of(context).translate("Email_Exist_String"));
                                      }else {
                                        showSnackBar(context,AppLocalizations.of(context).translate("Failed_Registration_String"));
                                      }
                                    });
                                  }catch(e){
                                    showSnackBar(context, e.toString());
                                  }finally{
                                    progressHide(context);
                                  }
                                }
                              }
                            },
                            color: Color(CustomColors().mainColorHard()),
                            textColor: Colors.white,
                            child: Text(
                              AppLocalizations.of(context).translate("Sign_Up_String"),
                              style: TextStyle(fontSize: 22.0),
                            ),
                          ),
                        ),
                      ),
                    ),

                  ],
                ),
              ),
            ),
          ),
        ),)),
      ),
    );
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return null;
  }
}
