import 'dart:io';
import 'package:bond_flutter/Constant/Constant.dart';
import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:bond_flutter/SessionManager/SessionManager.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:qr_flutter/qr_flutter.dart';
import '../UI/CustomColors.dart';
import '../Http/HttpRequest.dart';
import 'dart:convert' as json;
import 'package:intl/intl.dart';
import 'QrStatsScreen.dart';

class MyQrListScreen extends StatefulWidget {
  @override
  State createState() => MyQrListState();
}

class MyQrListState extends State<MyQrListScreen> {
  List list = new List();
  bool isLoading = false;
  var searchTextController = new TextEditingController();
  DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");

  DateFormat dateFormat2 = DateFormat("dd MMMM yyyy");
  ImageProvider _imageProvider;
  bool reload = false;

  Future<int> getAllQr() async {
    int statuscode = 401;
    setState(() {
      isLoading = true;
    });
    try {
      var fetchUserId = await SessionManager().getUserId();
      var userId = fetchUserId.toString();
      var params = {'hash': Constant.KEY, 'userId': userId};
      await HttpRequest()
          .fetchPost(Constant.get_my_qr, params)
          .then((response) {
        setState(() {
          isLoading = false;
        });
        print(response.statusCode);
        if (response.statusCode == 200) {
          try {
            print(response.body);
            setState(() {
              var convertDataToJson = json.jsonDecode(response.body);
              list = convertDataToJson['result'];
              statuscode = 200;
            });
          } catch (e) {
            print(e);
            statuscode = 401;
          }
        } else {
          print(response.statusCode);
          statuscode = 401;
        }
      });
    } catch (e) {
      print(e);
      statuscode = 401;
    }
    return statuscode;
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getAllQr();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: new AppBar(
          title: new Text(
            AppLocalizations.of(context).translate("Generated_QR_String"),
            style: TextStyle(fontSize: 22),
          ),
          backgroundColor: Color(CustomColors().mainColorHard()),
          automaticallyImplyLeading: true,
        ),
        body: Builder(
          builder: (context) => Center(
            child: Container(
                child: Stack(
              children: <Widget>[
                Container(
                  color: greyColor2,
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: (list.length > 0 && list.length != null)
                        ? ListView.builder(
                            itemCount: list.length == null ? 0 : list.length,
                            itemBuilder: (BuildContext context, int index) {
                              print("list ${list.length}");
                              return Material(
                                child: InkWell(
                                  onTap: () {
                                    _awaitReturnValueFromSecondScreen(
                                      context,
                                      Constant.qr_url + list[index]['qrId'],
                                      list[index]['backgroundColor'],
                                      list[index]['foregroundColor'],
                                      list[index]['bg_image'],
                                      list[index]['back_image'],
                                      list[index]['name'],
                                      list[index]['visual_leads_bgimage'],
                                    );
                                  },
                                  child: new Container(
                                    child: Center(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.stretch,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.all(8.0),
                                            child: Card(
                                              shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(20.0),
                                              ),
                                              child: Container(
                                                child: Row(
                                                  mainAxisAlignment:
                                                      MainAxisAlignment
                                                          .spaceBetween,
                                                  children: <Widget>[
                                                    Row(
                                                      children: <Widget>[
                                                        Stack(
                                                          children: <Widget>[
                                                            Container(
                                                              //  color: Colors.pink,
                                                              height: 100,
                                                              width: 100,

                                                              child: list[index]
                                                                          [
                                                                          'visual_leads_bgimage'] ==
                                                                      ""
                                                                  ? Container(
                                                                      child:
                                                                          QrImage(
                                                                        gapless:
                                                                            true,
                                                                        version:
                                                                            QrVersions.auto,
                                                                        // embeddedImage: list[index]['bg_image'] ==
                                                                        //         ""
                                                                        //     ? AssetImage(
                                                                        //         'images/logo_main.png',
                                                                        //       )
                                                                        //     : _imageProvider =
                                                                        //         NetworkImage(
                                                                        //         list[index]['bg_image'],
                                                                        //       ),
                                                                        embeddedImageStyle:
                                                                            QrEmbeddedImageStyle(
                                                                          size: Size(
                                                                              30,
                                                                              30),
                                                                        ),
                                                                        foregroundColor:
                                                                            Color(
                                                                          int.parse(
                                                                            list[index]['backgroundColor'],
                                                                          ),
                                                                        ),
                                                                        backgroundColor:
                                                                            Color(
                                                                          int.parse(
                                                                            list[index]['foregroundColor'],
                                                                          ),
                                                                        ),
                                                                        data: Constant.qr_url +
                                                                            list[index]['qrId'],
                                                                        size:
                                                                            100.0,
                                                                      ),
                                                                    )
                                                                  : Container(
                                                                      width:
                                                                          100,
                                                                      height:
                                                                          100,
                                                                      child:
                                                                          CachedNetworkImage(
                                                                        imageUrl:
                                                                            list[index]['visual_leads_bgimage'],
                                                                        placeholder:
                                                                            (context, url) =>
                                                                                new CircularProgressIndicator(),
                                                                        errorWidget: (context,
                                                                                url,
                                                                                error) =>
                                                                            new Icon(Icons.error),
                                                                      ),
                                                                    ),
                                                            ),
                                                          ],
                                                        ),
                                                        Padding(
                                                          padding:
                                                              const EdgeInsets
                                                                      .only(
                                                                  left: 12.0),
                                                          child: Text(
                                                            dateFormat2.format(
                                                                dateFormat.parse(
                                                                    list[index][
                                                                        'timestamp'])),
                                                            style: TextStyle(
                                                                color: Color(
                                                                    CustomColors()
                                                                        .mainColorHard()),
                                                                fontSize: 18),
                                                          ),
                                                        ),
                                                      ],
                                                    ),
                                                    Icon(
                                                      Icons.arrow_forward,
                                                      size: 20.0,
                                                      color: Colors.grey,
                                                    )
                                                  ],
                                                ),
                                                margin:
                                                    const EdgeInsets.all(10.0),
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              );
                            })
                        : Material(
                            child: Center(
                              child: Container(
                                child: Text(
                                  "No QR generated yet.",
                                  style: TextStyle(
                                      color: Color(CustomColors().mainColorHard()),
                                      fontSize: 20.0,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                            ),
                          ),
                  ),
                ),
                Positioned(
                  child: isLoading
                      ? Container(
                          child: Center(
                            child: CircularProgressIndicator(
                                valueColor:
                                    AlwaysStoppedAnimation<Color>(themeColor)),
                          ),
                          color: Colors.white.withOpacity(0.8),
                        )
                      : Container(),
                )
              ],
            )),
          ),
        ));
  }

  void _awaitReturnValueFromSecondScreen(
      BuildContext context,
      String id,
      String bgColor,
      String fgColor,
      String bgImage,
      String backImage,
      String name,
      String visualLeadBgImage) async {
    final result = await Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => QrStatsScreen(id, bgColor, fgColor, bgImage,
                name, backImage, visualLeadBgImage)));

    print("maaaaaaz" + backImage);

    // after the SecondScreen result comes back update the Text widget with it
    setState(() {
      reload = result;
      if (reload == true) {
        getAllQr();
      }
    });
  }
}
