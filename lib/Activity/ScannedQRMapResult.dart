import 'package:bond_flutter/UI/CustomColors.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class ScanedQRMapResult extends StatefulWidget {
  List list = new List();
  ScanedQRMapResult(this.list);
  ScanedQRMapState createState() => ScanedQRMapState();
}

class ScanedQRMapState extends State<ScanedQRMapResult> {

  var initialLatlong;
  Set<Marker> markers;

  // static final CameraPosition _kGooglePlex = CameraPosition(
  //   target: LatLng(-8.591204, 116.116208),
  //   zoom: 14.4746,
  // );
  
  @override
  void initState() {
    super.initState();
    markers = Set.from([]);
  }

  @override
  Widget build(BuildContext context) {

    initialLatlong = widget.list[0]['location'].toString().split(',');
    createMarker();

    return Scaffold(
      appBar: new AppBar(
        title: new Text(
          "QR Statics",
          style: TextStyle(fontSize: 22),
        ),
        backgroundColor: Color(CustomColors().mainColorHard()),
        automaticallyImplyLeading: true,
      ),
      body: GoogleMap(
        myLocationButtonEnabled: false,
        myLocationEnabled: false,
        onMapCreated: (GoogleMapController controller) {},
        initialCameraPosition:
            CameraPosition(target: LatLng(double.parse(initialLatlong[0]), double.parse(initialLatlong[1])), zoom: 12),
            markers: markers,
      ),
    );
  }

  createMarker() {
    
    var count = widget.list.length;

    for (var i = 0; i < count; i++){
      var listOfLatLong = widget.list[i]['location'].toString().split(',');
      var point = Marker(markerId: MarkerId(i.toString()),position: LatLng(double.parse(listOfLatLong[0]), double.parse(listOfLatLong[1])));
      markers.add(point);
    }

  }
}
