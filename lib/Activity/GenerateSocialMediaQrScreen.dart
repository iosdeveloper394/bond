import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:bond_flutter/Model/Item.dart';
import 'package:bond_flutter/UI/ProgressBar.dart';
import 'package:flutter/material.dart';
import 'package:bond_flutter/Transitions/ScreenPageTransitions.dart';
import '../UI/CustomColors.dart';
import '../Validator/Validator.dart';
import 'GenerateQRScreen.dart';
import '../UI/SnackBar.dart';
import '../Model/DataModel.dart';
import 'dart:collection';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart'; // For File Upload To Firestore
import 'package:image_picker/image_picker.dart'; // For Image Picker
import 'package:path/path.dart' as Path;
import 'package:bond_flutter/SessionManager/SessionManager.dart';
import 'SelectPackage.dart';

class GenerateSocialMediaQrScreen extends StatefulWidget {
  GenerateSocialMediaQrState createState() => GenerateSocialMediaQrState();
}

class GenerateSocialMediaQrState extends State<GenerateSocialMediaQrScreen> {
  File  _image;
  String _uploadedFileURL = "No File Selected";

  Item selectedItem1,
      selectedItem2,
      selectedItem3,
      selectedItem4,
      selectedItem5,
      selectedItem6,
      selectedItem7;
  List<Item> users = <Item>[
    const Item(
        'Facebook',
        Icon(
          FontAwesomeIcons.facebook,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Twitter',
        Icon(
          FontAwesomeIcons.twitter,
          color: const Color(0xFF167F67),
        )),
    const Item(
        'Snapchat',
        Icon(
          FontAwesomeIcons.snapchat,
          color: const Color(0xFF167F67),
        )),
    const Item(
      'Youtube',
      Icon(
        FontAwesomeIcons.youtube,
        color: const Color(0xFF167F67),
      ),
    ),
    const Item(
      'Instagram',
      Icon(
        FontAwesomeIcons.instagram,
        color: const Color(0xFF167F67),
      ),
    ),
    const Item(
      'Telegram',
      Icon(
        FontAwesomeIcons.telegram,
        color: const Color(0xFF167F67),
      ),
    ),
    const Item(
      'Tiktok',
      Icon(
        FontAwesomeIcons.tiktok,
        color: const Color(0xFF167F67),
      ),
    ),
  ];

  Validator validator;
  final bioController = TextEditingController();
  final selectedItem1Controller = TextEditingController();
  final selectedItem2Controller = TextEditingController();
  final selectedItem3Controller = TextEditingController();
  final selectedItem4Controller = TextEditingController();
  final selectedItem5Controller = TextEditingController();
  final selectedItem6Controller = TextEditingController();
  final selectedItem7Controller = TextEditingController();
  final customDomainController = TextEditingController();
  final customDomainItemController = TextEditingController();

  bool isFile = false;

  @override
  void initState() {
    super.initState();
    // Create an empty document or load existing if you have one.
    // Here we create an empty document:
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        title: new Text(
          AppLocalizations.of(context)
              .translate("Generate_Social_Media_QR_String"),
          style: TextStyle(fontSize: 22),
        ),
        backgroundColor: Color(CustomColors().mainColorHard()),
      ),
      body: Builder(
        builder: (context) => Center(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Material(
              child: Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: SingleChildScrollView(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: 400,
                    ),
                    child: Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: Image.asset(
                            'images/generate_text_qr_icon.png',
                            width: 300,
                            height: 120,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(top: 8.0),
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border.all(
                                  color: Color(CustomColors().dimGreyColor()),
                                  width: 2.0),
                              color: Color(CustomColors().lightGreyBG()),
                              borderRadius: BorderRadius.all(
                                Radius.circular(10),
                              ),
                              boxShadow: <BoxShadow>[
                                new BoxShadow(blurRadius: 0.5),
                              ],
                            ),
                            height: 230,
                            width: MediaQuery.of(context).size.width / 1.1,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 5.0, left: 8),
                              child: TextField(
                                maxLength: 450,
                                maxLines: 15,
                                textInputAction: TextInputAction.go,
                                controller: bioController,
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: AppLocalizations.of(context)
                                        .translate("Bio_String")),
                                style: TextStyle(
                                  fontSize: 12,
                                ),
                              ),
                            ),
                          ),
                        ),

                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                              child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              DropdownButton<Item>(
                                hint: Text(AppLocalizations.of(context)
                                    .translate("Select_item_String")),
                                value: selectedItem1,
                                onChanged: (Item Value) {
                                  FocusScope.of(context).requestFocus(new FocusNode());
                                  setState(() {
                                    selectedItem1 = Value;
                                  });
                                },
                                items: users.map((Item user) {
                                  return DropdownMenuItem<Item>(
                                    value: user,
                                    child: Row(
                                      children: <Widget>[
                                        user.icon,
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          user.name,
                                          style: TextStyle(color: Colors.black),
                                        ),
                                      ],
                                    ),
                                  );
                                }).toList(),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 0.0),
                                child: Container(

                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Color(
                                            CustomColors().dimGreyColor()),
                                        width: 2.0),
                                    color: Color(CustomColors().lightGreyBG()),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                    boxShadow: <BoxShadow>[
                                      new BoxShadow(blurRadius: 0.5),
                                    ],
                                  ),
                                  height: 40,
                                  width: 200,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      bottom: 0.0,
                                      left: 8,
                                    ),
                                    child: TextField(
                                      maxLines: 1,
                                      controller: selectedItem1Controller,
                                      decoration: InputDecoration(
                                          contentPadding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 0),
                                          border: InputBorder.none,
                                          hintText: "@abdulsamad"),
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                              child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              DropdownButton<Item>(
                                hint: Text(AppLocalizations.of(context)
                                    .translate("Select_item_String")),
                                value: selectedItem2,
                                onChanged: (Item Value) {
                                  FocusScope.of(context).requestFocus(new FocusNode());
                                  setState(() {
                                    selectedItem2 = Value;
                                  });
                                },
                                items: users.map((Item user) {
                                  return DropdownMenuItem<Item>(
                                    value: user,
                                    child: Row(
                                      children: <Widget>[
                                        user.icon,
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          user.name,
                                          style: TextStyle(color: Colors.black),
                                        ),
                                      ],
                                    ),
                                  );
                                }).toList(),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Color(
                                            CustomColors().dimGreyColor()),
                                        width: 2.0),
                                    color: Color(CustomColors().lightGreyBG()),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                    boxShadow: <BoxShadow>[
                                      new BoxShadow(blurRadius: 0.5),
                                    ],
                                  ),
                                  height: 40,
                                  width: 200,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      bottom: 0.0,
                                      left: 8,
                                    ),
                                    child: TextField(
                                      maxLines: 1,
                                      controller: selectedItem2Controller,
                                      decoration: InputDecoration(
                                          contentPadding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 0),
                                          border: InputBorder.none,
                                          hintText: "@abdulsamad"),
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                              child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              DropdownButton<Item>(
                                hint: Text(AppLocalizations.of(context)
                                    .translate("Select_item_String")),
                                value: selectedItem3,
                                onChanged: (Item Value) {
                                  FocusScope.of(context).requestFocus(new FocusNode());
                                  setState(() {
                                    selectedItem3 = Value;
                                  });
                                },
                                items: users.map((Item user) {
                                  return DropdownMenuItem<Item>(
                                    value: user,
                                    child: Row(
                                      children: <Widget>[
                                        user.icon,
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          user.name,
                                          style: TextStyle(color: Colors.black),
                                        ),
                                      ],
                                    ),
                                  );
                                }).toList(),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Color(
                                            CustomColors().dimGreyColor()),
                                        width: 2.0),
                                    color: Color(CustomColors().lightGreyBG()),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                    boxShadow: <BoxShadow>[
                                      new BoxShadow(blurRadius: 0.5),
                                    ],
                                  ),
                                  height: 40,
                                  width: 200,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      bottom: 0.0,
                                      left: 8,
                                    ),
                                    child: TextField(
                                      maxLines: 1,
                                      controller: selectedItem3Controller,
                                      decoration: InputDecoration(
                                          contentPadding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 0),
                                          border: InputBorder.none,
                                          hintText: "@abdulsamad"),
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                              child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              DropdownButton<Item>(
                                hint: Text(AppLocalizations.of(context)
                                    .translate("Select_item_String")),
                                value: selectedItem4,
                                onChanged: (Item Value) {
                                  FocusScope.of(context).requestFocus(new FocusNode());
                                  setState(() {
                                    selectedItem4 = Value;
                                  });
                                },
                                items: users.map((Item user) {
                                  return DropdownMenuItem<Item>(
                                    value: user,
                                    child: Row(
                                      children: <Widget>[
                                        user.icon,
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          user.name,
                                          style: TextStyle(color: Colors.black),
                                        ),
                                      ],
                                    ),
                                  );
                                }).toList(),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Color(
                                            CustomColors().dimGreyColor()),
                                        width: 2.0),
                                    color: Color(CustomColors().lightGreyBG()),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                    boxShadow: <BoxShadow>[
                                      new BoxShadow(blurRadius: 0.5),
                                    ],
                                  ),
                                  height: 40,
                                  width: 200,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      bottom: 0.0,
                                      left: 8,
                                    ),
                                    child: TextField(
                                      maxLines: 1,
                                      controller: selectedItem4Controller,
                                      decoration: InputDecoration(
                                          border: InputBorder.none,
                                          hintText: "@abdulsamad"),
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )),
                        ),

                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                              child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              DropdownButton<Item>(
                                hint: Text(AppLocalizations.of(context)
                                    .translate("Select_item_String")),
                                value: selectedItem5,
                                onChanged: (Item Value) {
                                  FocusScope.of(context).requestFocus(new FocusNode());
                                  setState(() {
                                    selectedItem5 = Value;
                                  });
                                },
                                items: users.map((Item user) {
                                  return DropdownMenuItem<Item>(
                                    value: user,
                                    child: Row(
                                      children: <Widget>[
                                        user.icon,
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          user.name,
                                          style: TextStyle(color: Colors.black),
                                        ),
                                      ],
                                    ),
                                  );
                                }).toList(),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Color(
                                            CustomColors().dimGreyColor()),
                                        width: 2.0),
                                    color: Color(CustomColors().lightGreyBG()),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                    boxShadow: <BoxShadow>[
                                      new BoxShadow(blurRadius: 0.5),
                                    ],
                                  ),
                                  height: 40,
                                  width: 200,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      bottom: 0.0,
                                      left: 8,
                                    ),
                                    child: TextField(
                                      maxLines: 1,
                                      controller: selectedItem5Controller,
                                      decoration: InputDecoration(
                                          contentPadding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 0),

                                          border: InputBorder.none,
                                          hintText: "@abdulsamad"),
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                              child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              DropdownButton<Item>(
                                hint: Text(AppLocalizations.of(context)
                                    .translate("Select_item_String")),
                                value: selectedItem6,
                                onChanged: (Item Value) {
                                  FocusScope.of(context).requestFocus(new FocusNode());
                                  setState(() {
                                    selectedItem6 = Value;
                                  });
                                },
                                items: users.map((Item user) {
                                  return DropdownMenuItem<Item>(
                                    value: user,
                                    child: Row(
                                      children: <Widget>[
                                        user.icon,
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          user.name,
                                          style: TextStyle(color: Colors.black),
                                        ),
                                      ],
                                    ),
                                  );
                                }).toList(),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Color(
                                            CustomColors().dimGreyColor()),
                                        width: 2.0),
                                    color: Color(CustomColors().lightGreyBG()),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                    boxShadow: <BoxShadow>[
                                      new BoxShadow(blurRadius: 0.5),
                                    ],
                                  ),
                                  height: 40,
                                  width: 200,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      bottom: 0.0,
                                      left: 8,
                                    ),
                                    child: TextField(
                                      maxLines: 1,
                                      controller: selectedItem6Controller,
                                      decoration: InputDecoration(
                                          contentPadding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 0),
                                          border: InputBorder.none,
                                          hintText: "@abdulsamad"),
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                              child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              DropdownButton<Item>(
                                hint: Text(AppLocalizations.of(context)
                                    .translate("Select_item_String")),
                                value: selectedItem7,
                                onChanged: (Item Value) {
                                  FocusScope.of(context).requestFocus(new FocusNode());
                                  setState(() {
                                    selectedItem7 = Value;
                                  });
                                },
                                items: users.map((Item user) {
                                  return DropdownMenuItem<Item>(
                                    value: user,
                                    child: Row(
                                      children: <Widget>[
                                        user.icon,
                                        SizedBox(
                                          width: 10,
                                        ),
                                        Text(
                                          user.name,
                                          style: TextStyle(color: Colors.black),
                                        ),
                                      ],
                                    ),
                                  );
                                }).toList(),
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 8.0),
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(
                                        color: Color(
                                            CustomColors().dimGreyColor()),
                                        width: 2.0),
                                    color: Color(CustomColors().lightGreyBG()),
                                    borderRadius: BorderRadius.all(
                                      Radius.circular(10),
                                    ),
                                    boxShadow: <BoxShadow>[
                                      new BoxShadow(blurRadius: 0.5),
                                    ],
                                  ),
                                  height: 40,
                                  width: 200,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                      bottom: 0.0,
                                      left: 8,
                                    ),
                                    child: TextField(
                                      maxLines: 1,
                                      controller: selectedItem7Controller,
                                      decoration: InputDecoration(
                                          contentPadding: const EdgeInsets.symmetric(vertical: 12.0, horizontal: 0),
                                          border: InputBorder.none,
                                          hintText: "@abdulsamad"),
                                      style: TextStyle(
                                        fontSize: 12,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          )),
                        ),

                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                              child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: <Widget>[
                              RaisedButton(
                                child: Text(AppLocalizations.of(context)
                                    .translate("Upload_File_String"), style: TextStyle(color: Colors.white),),
                                onPressed: chooseFile,
                                color: Colors.blueAccent,
                              ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8.0, left: 20),
                                  child: Container(
                                    alignment: Alignment.center,
                                    height: 140,
                                    width: 200,
                                    child: Padding(
                                      padding: const EdgeInsets.only(
                                        left: 0,
                                      ),
                                      child:

                                      Container(
                                        child:  _image != null
                                            ? Image.file(File(_image.path), height: 150,)
                                        //     Image.asset(
                                        //   _image.path,
                                        //   height: 150,
                                        //   color: Colors.black,
                                        // )
                                            : Container(height: 150),
                                      ),

                                    ),
                                  ),
                                ),
                              ],
                                )),
                          ),
//                        Text('Selected Image'),
//                        _image != null
//                            ? Image.asset(
//                          _image.path,
//                          height: 150,
//                        )
//                            : Container(height: 150),
//                        _image == null
//                            ? RaisedButton(
//                          child: Text('Choose File'),
//                          onPressed: chooseFile,
//                          color: Colors.cyan,
//                        )
//                            : Container(),
//                        _image != null
//                            ? RaisedButton(
//                          child: Text('Upload File'),
//                          onPressed: uploadFile,
//                          color: Colors.cyan,
//                        )
//                            : Container(),
//                        _image != null
//                            ? RaisedButton(
//                          child: Text('Clear Selection'),
////                          onPressed: clearSelection,
//                        )
//                            : Container(),
//                        Text('Uploaded Image'),
//                        _uploadedFileURL != null
//                            ? Image.network(
//                          _uploadedFileURL,
//                          height: 150,
//                        )
//                            : Container(),
















                        //
                        //
                        //        INSTAGRAM REMOVE START
                        //
                        //


//                        Padding(
//                          padding: const EdgeInsets.all(8.0),
//                          child: Container(
//                              child: Row(
//                            mainAxisSize: MainAxisSize.max,
//                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                            children: <Widget>[
//                              Padding(
//                                padding: const EdgeInsets.only(top: 8.0),
//                                child: Container(
//                                  decoration: BoxDecoration(
//                                    border: Border.all(
//                                        color: Color(
//                                            CustomColors().dimGreyColor()),
//                                        width: 2.0),
//                                    color: Color(CustomColors().lightGreyBG()),
//                                    borderRadius: BorderRadius.all(
//                                      Radius.circular(10),
//                                    ),
//                                    boxShadow: <BoxShadow>[
//                                      new BoxShadow(blurRadius: 0.5),
//                                    ],
//                                  ),
//                                  height: 40,
//                                  width: 100,
//                                  child: Padding(
//                                    padding: const EdgeInsets.only(
//                                      top: 5.0,
//                                      left: 8,
//                                    ),
//                                    child: TextField(
//                                      maxLines: 1,
//                                      controller: customDomainController,
//                                      decoration: InputDecoration(
//                                          border: InputBorder.none,
//                                          hintText: "instagram"),
//                                      style: TextStyle(
//                                        fontSize: 12,
//                                      ),
//                                    ),
//                                  ),
//                                ),
//                              ),
//                              Padding(
//                                padding: const EdgeInsets.only(top: 8.0),
//                                child: Container(
//                                  decoration: BoxDecoration(
//                                    border: Border.all(
//                                        color: Color(
//                                            CustomColors().dimGreyColor()),
//                                        width: 2.0),
//                                    color: Color(CustomColors().lightGreyBG()),
//                                    borderRadius: BorderRadius.all(
//                                      Radius.circular(10),
//                                    ),
//                                    boxShadow: <BoxShadow>[
//                                      new BoxShadow(blurRadius: 0.5),
//                                    ],
//                                  ),
//                                  height: 40,
//                                  width: 200,
//                                  child: Padding(
//                                    padding: const EdgeInsets.only(
//                                      top: 5.0,
//                                      left: 8,
//                                    ),
//                                    child: TextField(
//                                      maxLines: 1,
//                                      controller: customDomainItemController,
//                                      decoration: InputDecoration(
//                                          border: InputBorder.none,
//                                          hintText: "@abdulsamad"),
//                                      style: TextStyle(
//                                        fontSize: 12,
//                                      ),
//                                    ),
//                                  ),
//                                ),
//                              ),
//                            ],
//                          )),
//                        ),




                        //
                        //
                        //        INSTAGRAM REMOVE END
                        //
                        //

                        Padding(
                          padding: const EdgeInsets.all(15.0),
                          child: InkWell(
                            onTap: () async {
                              // int subscribedTwo = await SessionManager().getIsSubscribedTwo();
                              // int subscribedThree = await SessionManager().getIsSubscribedThree();
     //                         if(subscribedTwo == 1 || subscribedThree == 1){
                                validator = new Validator(context);
                                if (bioController.text.isEmpty ||
                                    bioController.text.length < 1 ||
                                    bioController.text.length > 250) {
                                  showSnackBar(
                                      context,
                                      AppLocalizations.of(context)
                                          .translate("Invalid_Bio_String"));
                                  return;
                                }

                                HashMap<String, String> list = new HashMap();
                                list['bio'] = (bioController.text);
                                if (selectedItem1 != null) {
                                  if (selectedItem1Controller.text.isEmpty ||
                                      selectedItem1Controller.text.length < 1 ||
                                      selectedItem1Controller.text.length > 25) {
                                    showSnackBar(
                                        context,
                                        AppLocalizations.of(context).translate(
                                            "Invalid_empty_String") +
                                            selectedItem1.name.toString());
                                    return;
                                  } else
                                    list[selectedItem1.name.toString()] =
                                        selectedItem1Controller.text;
                                }

                                if (selectedItem2 != null) {
                                  if (selectedItem2Controller.text.isEmpty ||
                                      selectedItem2Controller.text.length < 1 ||
                                      selectedItem2Controller.text.length > 25) {
                                    showSnackBar(
                                        context,
                                        AppLocalizations.of(context).translate(
                                            "Invalid_empty_String") +
                                            selectedItem2.name.toString());
                                    return;
                                  } else
                                    list[selectedItem2.name.toString()] =
                                        selectedItem2Controller.text;
                                }

                                if (selectedItem3 != null) {
                                  if (selectedItem3Controller.text.isEmpty ||
                                      selectedItem3Controller.text.length < 1 ||
                                      selectedItem3Controller.text.length > 25) {
                                    showSnackBar(
                                        context,
                                        AppLocalizations.of(context).translate(
                                            "Invalid_empty_String") +
                                            selectedItem3.name.toString());
                                    return;
                                  } else
                                    list[selectedItem3.name.toString()] =
                                        selectedItem3Controller.text;
                                }

                                if (selectedItem4 != null) {
                                  if (selectedItem4Controller.text.isEmpty ||
                                      selectedItem4Controller.text.length < 1 ||
                                      selectedItem4Controller.text.length > 25) {
                                    showSnackBar(
                                        context,
                                        AppLocalizations.of(context).translate(
                                            "Invalid_empty_String") +
                                            selectedItem4.name.toString());
                                    return;
                                  } else
                                    list[selectedItem4.name.toString()] =
                                        selectedItem4Controller.text;
                                }

                                if (selectedItem5 != null) {
                                  if (selectedItem5Controller.text.isEmpty ||
                                      selectedItem5Controller.text.length < 1 ||
                                      selectedItem5Controller.text.length > 25) {
                                    showSnackBar(
                                        context,
                                        AppLocalizations.of(context).translate(
                                            "Invalid_empty_String") +
                                            selectedItem5.name.toString());
                                    return;
                                  } else
                                    list[selectedItem5.name.toString()] =
                                        selectedItem5Controller.text;
                                }

                                if (selectedItem6 != null) {
                                  if (selectedItem6Controller.text.isEmpty ||
                                      selectedItem6Controller.text.length < 1 ||
                                      selectedItem6Controller.text.length > 25) {
                                    showSnackBar(
                                        context,
                                        AppLocalizations.of(context).translate(
                                            "Invalid_empty_String") +
                                            selectedItem6.name.toString());
                                    return;
                                  } else
                                    list[selectedItem6.name.toString()] =
                                        selectedItem6Controller.text;
                                }

                                if (selectedItem7 != null) {
                                  if (selectedItem7Controller.text.isEmpty ||
                                      selectedItem7Controller.text.length < 1 ||
                                      selectedItem7Controller.text.length > 25) {
                                    showSnackBar(
                                        context,
                                        AppLocalizations.of(context).translate(
                                            "Invalid_empty_String") +
                                            selectedItem7.name.toString());
                                    return;
                                  } else
                                    list[selectedItem7.name.toString()] =
                                        selectedItem7Controller.text;
                                }

                                if (((customDomainController.text.isEmpty) &&
                                    (customDomainItemController
                                        .text.isNotEmpty)) ||
                                    ((customDomainController.text.isNotEmpty) &&
                                        (customDomainItemController
                                            .text.isEmpty))) {
                                  showSnackBar(
                                      context,
                                      AppLocalizations.of(context)
                                          .translate("Invalid_empty_String") +
                                          " " +
                                          customDomainController.text);
                                  return;
                                } else if (customDomainItemController
                                    .text.isNotEmpty &&
                                    customDomainController.text.isNotEmpty) {
                                  if (customDomainController.text.length < 1 ||
                                      customDomainController.text.length > 25 ||
                                      customDomainItemController.text.length <
                                          1 ||
                                      customDomainItemController.text.length >
                                          25) {
                                    showSnackBar(
                                        context,
                                        AppLocalizations.of(context).translate(
                                            "Invalid_empty_String") +
                                            customDomainController.text);
                                    return;
                                  } else
                                    list[customDomainController.text] =
                                        customDomainItemController.text;
                                }

                                if (isFile) {
                                  uploadFile().then((onComplete) async {
                                    progressHide(context);
                                    debugPrint(_uploadedFileURL);
                                    list['image'] = _uploadedFileURL;

                                    DataModel dataModel =
                                    new DataModel(7, "socialmedia", list);

                                    Navigator.push(
                                        context,
                                        SlideRightRoute(
                                            page: GenerateQRScreen(dataModel,false)));
                                  });
                                } else {
                                  DataModel dataModel =
                                  new DataModel(7, "socialmedia", list);

                                  Navigator.push(
                                      context,
                                      SlideRightRoute(
                                          page: GenerateQRScreen(dataModel,false)));
                                }
//                              }
//                              else{
//                                showSnackBar(
//                                    context,
//                                    AppLocalizations.of(context)
//                                        .translate("Buy_Any_Plan"));
//                                Navigator.push(context, MaterialPageRoute(builder: (context) => SelectPackage()));
//                              }



                            },
                            child: Image.asset(
                              'images/generate_button_icon.png',
                              width: 50,
                              height: 50,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return null;
  }

  Future chooseFile() async {
    await ImagePicker.pickImage(source: ImageSource.gallery).then((image) {
      setState(() {
        _image = image;
        _uploadedFileURL = Path.basename(_image.path);
        isFile = true;
      });
    });
  }

  Future uploadFile() async {
    progressShow(context);
    StorageReference storageReference = FirebaseStorage.instance
        .ref()
        .child('uploads/${Path.basename(_image.path)}');
    StorageUploadTask uploadTask = storageReference.putFile(_image);
    await uploadTask.onComplete;
    print('File Uploaded');
    storageReference.getDownloadURL().then((fileURL) {
      setState(() {
        _uploadedFileURL = fileURL;
        print(fileURL);
      });
    });
  }
}
