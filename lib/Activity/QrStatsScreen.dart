import 'dart:convert' as json;
import 'dart:convert';

import 'dart:ui' as ui;
import 'dart:io';
import 'dart:typed_data';
import 'package:bond_flutter/Activity/ScannedQRMapResult.dart';
import 'package:bond_flutter/Constant/Constant.dart';
import 'package:bond_flutter/Http/HttpRequest.dart';
import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:screenshot/screenshot.dart';
import 'package:url_launcher/url_launcher.dart';
import '../UI/CustomColors.dart';
import '../SessionManager/SessionManager.dart';
import '../UI/SnackBar.dart';
import '../UI/ProgressBar.dart';
import 'package:intl/intl.dart';

class QrStatsScreen extends StatefulWidget {
  String qrId, bgColor, fgColor, bgImage, name,backImage, visualLeadBgImage;


  QrStatsScreen(this.qrId, this.bgColor, this.fgColor, this.bgImage, this.name, this.backImage, this.visualLeadBgImage);

  QrStatsState createState() => QrStatsState(
      this.qrId, this.bgColor, this.fgColor, this.bgImage, this.name, this.backImage, this.visualLeadBgImage);



}

class QrStatsState extends State<QrStatsScreen> {
  bool isTOSChecked = false;
  bool isLoading = false;
  bool _progressBarActive = true;
  String qrId, bgColor, fgColor, bgImage, name, backImage, visualLeadBgImage;
  String noOfScansAndroid = "Number of scans in Phone";
  String noOfScansIos = "Number of scans in IOS";
  String totalNoOfScans = "Number of total scans";
  int isSubscribedZero = 0;
  int isSubscribedOne = 0;
  int isSubscribedTwo = 0;
  int isSubscribedThree = 0;
  int statuscode = 401;
  var type = "";


 // DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");

  DateFormat dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");



  QrStatsState(this.qrId, this.bgColor, this.fgColor, this.bgImage, this.name, this.backImage, this.visualLeadBgImage);

  static GlobalKey previewContainer = new GlobalKey();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fetchUserDetail();

  }

  Future<int>   getQrStats(BuildContext context) async {

   // print("bgImagecheck" + bgImage);
  //  print("maaaz" + fgColor);
    setState(() {
      isLoading = true;
    });
    try {
      var fetchUserId = await SessionManager().getUserId();
      var userId = fetchUserId.toString();
      var params = {'hash': Constant.KEY, 'userId': userId, 'qrId': qrId};
      progressShow(context);
      await HttpRequest()
          .fetchPost(Constant.show_results, params)
          .then((response) {
        progressHide(context);
        setState(() {
          isLoading = false;
        });
        print(response.statusCode);
        if (response.statusCode == 200) {
          try {
            print(response.body);
            setState(() {
              var convertDataToJson = json.jsonDecode(response.body);
              if (convertDataToJson == null) {
                setState(() {
                  noOfScansIos = "0";
                  noOfScansAndroid = "0";
                  totalNoOfScans = "0";
                });
              } else {
                setState(() {
                  noOfScansIos = convertDataToJson['iosCount'];
                  noOfScansAndroid = convertDataToJson['androidCount'];
                  totalNoOfScans =
                      (int.parse(noOfScansIos) + int.parse(noOfScansAndroid))
                          .toString();
                });
              }
              print(convertDataToJson);
              statuscode = 200;
            });
          } catch (e) {
            print(e);
            statuscode = 401;
          }
        } else if (response.statusCode == 404) {
          showSnackBar(context, "You need to subscribe!");
          statuscode = 404;
          setState(() {});
        } else {
          print(response.statusCode);
          statuscode = 401;
        }
      });
    } catch (e) {
      print(e);
      statuscode = 401;
    }
    return statuscode;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    ImageProvider _imageProvider;

//    GlobalKey _renderObjectKey = new GlobalKey();

    print("hadeed color" + fgColor);
    print("hadeed" + visualLeadBgImage);
    type = widget.name.toUpperCase();

    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      appBar: new AppBar(
        title: new Text(
          "QR Statics",
          style: TextStyle(fontSize: 22),
        ),
        backgroundColor: Color(CustomColors().mainColorHard()),
        automaticallyImplyLeading: true,
      ),
      body: _progressBarActive == true
          ? const Center(child: const CircularProgressIndicator())
          : Builder(
        builder: (context) =>
            Container(
              width: MediaQuery
                  .of(context)
                  .size
                  .width,
              height: MediaQuery
                  .of(context)
                  .size
                  .height,
              child: Material(
                child: Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: SingleChildScrollView(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minHeight: 400,
                      ),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Column(
                              children: <Widget>[
                                Text("$type QR", 
                                style: TextStyle(
                                    color:
                                    Color(CustomColors().mainColorHard()),
                                    fontSize: 20,
                                  ),),
                                Screenshot(
                                  controller: screenshotController,
                                  child: Padding(
                                    padding: const EdgeInsets.all(5.0),
                                    child: Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: <Widget>[
                                        Container(child: visualLeadBgImage ==
                                                                  ""
                                                              ? Container(
                                                                width: 200,
                                                                  height: 200,
                                                                  child:
                                                                      QrImage(
                                                                    gapless:
                                                                        true,
                                                                    version:
                                                                        QrVersions
                                                                            .auto,
                                                                    // embeddedImage: bgImage ==
                                                                    //         ""
                                                                    //     ? AssetImage(
                                                                    //         'images/logo_main.png',
                                                                    //       )
                                                                    //     : _imageProvider =
                                                                    //         NetworkImage(
                                                                    //         bgImage,
                                                                    //       ),
                                                                    embeddedImageStyle:
                                                                        QrEmbeddedImageStyle(
                                                                      size: Size(
                                                                          30,
                                                                          30),
                                                                    ),
                                                                    foregroundColor:
                                                                        Color(
                                                                      int.parse(
                                                                        bgColor,
                                                                      ),
                                                                    ),
                                                                    backgroundColor:
                                                                        Color(
                                                                      int.parse(
                                                                        fgColor,
                                                                      ),
                                                                    ),
                                                                    data: qrId,
                                                                    size: 100.0,
                                                                  ),
                                                                )
                                                              : Container(
                                                                  width: 200,
                                                                  height: 200,
                                                                   child:
                                                                      CachedNetworkImage(
                                                                    imageUrl: visualLeadBgImage,
                                                                    placeholder:
                                                                        (context,
                                                                                url) =>
                                                                            new CircularProgressIndicator(),
                                                                    errorWidget: (context,
                                                                            url,
                                                                            error) =>
                                                                        new Icon(
                                                                            Icons.error),
                                                                  ),
                                                                ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                Text(
                                  totalNoOfScans,
                                  style: TextStyle(
                                    color:
                                    Color(CustomColors().mainColorHard()),
                                    fontSize: 20,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(top: 8.0),
                                  child: CircleAvatar(
                                    backgroundColor:
                                    Color(CustomColors().mainColorHard()),
                                    child: Image.asset(
                                      "images/qr_scanner_icon.png",
                                      height: 33.0,
                                      width: 33.0,
                                      color: Colors.white,
                                    ),
                                    minRadius: 25,
                                    maxRadius: 25,
                                  ),
                                )
                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 30.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    noOfScansAndroid,
                                    style: TextStyle(
                                      color: Color(
                                          CustomColors().mainColorHard()),
                                      fontSize: 20,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 8.0),
                                    child: CircleAvatar(
                                      backgroundColor: Color(
                                          CustomColors().mainColorHard()),
                                      child: Image.asset(
                                        "images/phone_icon.png",
                                        height: 33.0,
                                        width: 33.0,
                                        color: Colors.white,
                                      ),
                                      minRadius: 25,
                                      maxRadius: 25,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 30.0),
                              child: Column(
                                children: <Widget>[
                                  Text(
                                    noOfScansIos,
                                    style: TextStyle(
                                      color: Color(
                                          CustomColors().mainColorHard()),
                                      fontSize: 20,
                                    ),
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.only(top: 8.0),
                                    child: CircleAvatar(
                                      backgroundColor: Color(
                                          CustomColors().mainColorHard()),
                                      child: Image.asset(
                                        "images/ios_icon.png",
                                        height: 33.0,
                                        width: 33.0,
                                        color: Colors.white,
                                      ),
                                      minRadius: 25,
                                      maxRadius: 25,
                                    ),
                                  )
                                ],
                              ),
                            ),
                            Material(
                              child: InkWell(
                                onTap: () {
                                  if (totalNoOfScans == "0") {
                                      showSnackBar(
                                          context, "No location found!");
                                    } else if (totalNoOfScans ==
                                        "Number of total scans") {
                                      showSnackBar(
                                          context, "First check results!");
                                    } else {
                                      _openMap();
                                    }
                                  // if (isSubscribedZero == 1 ||
                                  //     isSubscribedOne == 1 ||
                                  //     isSubscribedTwo == 1 ||
                                  //     isSubscribedThree == 1) {
                                  //   if (totalNoOfScans == "0") {
                                  //     showSnackBar(
                                  //         context, "No location found!");
                                  //   } else if (totalNoOfScans ==
                                  //       "Number of total scans") {
                                  //     showSnackBar(
                                  //         context, "First check results!");
                                  //   } else {
                                  //     _openMap();
                                  //   }
                                  // } else {
                                  //   showSnackBar(
                                  //       context,
                                  //       AppLocalizations.of(context)
                                  //           .translate("Buy_Any_Plan"));
                                  // }
                                },
                                child: Padding(
                                  padding: const EdgeInsets.only(top: 30.0),
                                  child: Column(
                                    children: <Widget>[
                                      Text(
                                        "Locations",
                                        style: TextStyle(
                                          color: Color(
                                              CustomColors().mainColorHard()),
                                          fontSize: 20,
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                        const EdgeInsets.only(top: 8.0),
                                        child: CircleAvatar(
                                          backgroundColor: themeColor,
                                          child: Image.asset(
                                            "images/location_icon.png",
                                            height: 33.0,
                                            width: 33.0,
                                            color: Colors.white,
                                          ),
                                          minRadius: 25,
                                          maxRadius: 25,
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(top: 40.0),
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceEvenly,
                                children: <Widget>[
                                  Material(
                                    child: InkWell(
                                      onTap: () {
                                         getQrStats(context);
                                        // if (isSubscribedZero == 1 &&
                                        //     (name == 'text' ||
                                        //         name == 'sms' ||
                                        //         name == 'email' ||
                                        //         name == 'website' ||
                                        //         name == 'contact')) {
                                        //   getQrStats(context);
                                        // } else if (isSubscribedOne == 1 &&
                                        //     (name == 'text' ||
                                        //         name == 'sms' ||
                                        //         name == 'email' ||
                                        //         name == 'website' ||
                                        //         name == 'contact' ||
                                        //         name == 'vcard')) {
                                        //   getQrStats(context);
                                        // } else if (isSubscribedTwo == 1 &&
                                        //     (name == 'text' ||
                                        //         name == 'sms' ||
                                        //         name == 'email' ||
                                        //         name == 'website' ||
                                        //         name == 'contact' ||
                                        //         name == 'vcard' ||
                                        //         name == 'socialmedia')) {
                                        //   getQrStats(context);
                                        // } else if (isSubscribedThree == 1 &&
                                        //     (name == 'text' ||
                                        //         name == 'sms' ||
                                        //         name == 'email' ||
                                        //         name == 'website' ||
                                        //         name == 'contact' ||
                                        //         name == 'vcard' ||
                                        //         name == 'socialmedia' ||
                                        //         name == 'invite')) {
                                        //   getQrStats(context);
                                        // } else {
                                        //   getQrStats(context);
                                        //   showSnackBar(
                                        //       context,
                                        //       AppLocalizations.of(context)
                                        //           .translate("Buy_Any_Plan"));
                                        // }
                                      },
                                      child: Container(
                                        width: 130,
                                        decoration: BoxDecoration(
                                            borderRadius: new BorderRadius
                                                .all(
                                                new Radius.circular(10.0)),
                                            color: Color(CustomColors()
                                                .mainColorHard())),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Center(
                                            child: Text(
                                              "Show Results",
                                              style: TextStyle(
                                                fontSize: 15,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                  Material(
                                    child: InkWell(
                                      onTap: () {
                                        _deleteQR();
                                      },
                                      child: Container(
                                        width: 130,
                                        decoration: BoxDecoration(
                                            borderRadius: new BorderRadius
                                                .all(
                                                new Radius.circular(10.0)),
                                            color: Color(CustomColors()
                                                .mainColorHard())),
                                        child: Padding(
                                          padding: const EdgeInsets.all(8.0),
                                          child: Text(
                                            "Delete",
                                            style: TextStyle(
                                              fontSize: 17,
                                              color: Colors.white,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),


                            Padding(
                              padding: const EdgeInsets.only(
                                  top: 40.0, bottom: 20),
                              child: Row(
                                mainAxisAlignment:
                                MainAxisAlignment.spaceEvenly,
                                children: <Widget>[

                                  Material(
                                    child: InkWell(
                                      onTap: () async {

                                        await saveImageInGallery();
                                        print("Clicked");
                                      },


                                      child: Container(
                                        width: 130,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            new BorderRadius.all(
                                                new Radius.circular(
                                                    10.0)),
                                            color: Color(CustomColors()
                                                .mainColorHard())),
                                        child: Padding(
                                          padding:
                                          const EdgeInsets.all(8.0),
                                          child: Text(
                                            "Save Image",
                                            style: TextStyle(
                                              fontSize: 17,
                                              color: Colors.white,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),




                                  Material(
                                    child: InkWell(
                                      onTap: () async {
//                                              Share.share(Constant.qr_url +
//                                                  widget.qrId.toString());
                                        _takeScreenshotandShare();
                                      },
                                      child: Container(
                                        width: 130,
                                        decoration: BoxDecoration(
                                            borderRadius:
                                            new BorderRadius.all(
                                                new Radius.circular(
                                                    10.0)),
                                            color: Color(CustomColors()
                                                .mainColorHard())),
                                        child: Padding(
                                          padding:
                                          const EdgeInsets.all(8.0),
                                          child: Text(
                                            "Share",
                                            style: TextStyle(
                                              fontSize: 17,
                                              color: Colors.white,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),

                                ],
                              ),
                            ),


//
//                              Padding(
//                                  padding: const EdgeInsets.only(top: 40.0),
//                                  child: Row(
//                                      mainAxisAlignment:
//                                          MainAxisAlignment.spaceEvenly,
//                                      children: <Widget>[
//                                        Material(
//                                          child: InkWell(
//                                            onTap: () async {
////                                              Share.share(Constant.qr_url +
////                                                  widget.qrId.toString());
//                                              _takeScreenshotandShare();
//                                            },
//                                            child: Container(
//                                              width: 130,
//                                              decoration: BoxDecoration(
//                                                  borderRadius:
//                                                      new BorderRadius.all(
//                                                          new Radius.circular(
//                                                              10.0)),
//                                                  color: Color(CustomColors()
//                                                      .mainColorHard())),
//                                              child: Padding(
//                                                padding:
//                                                    const EdgeInsets.all(8.0),
//                                                child: Text(
//                                                  "Share",
//                                                  style: TextStyle(
//                                                    fontSize: 17,
//                                                    color: Colors.white,
//                                                  ),
//                                                  textAlign: TextAlign.center,
//                                                ),
//                                              ),
//                                            ),
//                                          ),
//                                        ),
//                                      ]))
                          ]),
                    ),
                  ),
                ),
              ),
            ),
      ),
    );
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return null;
  }

  void fetchUserDetail() async {
    try {
      var fetchUserId = await SessionManager().getUserId();
      var userId = fetchUserId.toString();
      var params = {'hash': Constant.KEY, 'userId': userId};
      await HttpRequest()
          .fetchPost(Constant.get_user_detail, params)
          .then((response) {
        print(response.statusCode);
        if (response.statusCode == 200) {
          try {
            print(response.body);
            setState(() {
              var convertDataToJson = jsonDecode(response.body);
              isSubscribedZero =
                  int.parse(convertDataToJson['isSubscribedZero']);
              isSubscribedOne = int.parse(convertDataToJson['isSubscribedOne']);
              isSubscribedTwo = int.parse(convertDataToJson['isSubscribedTwo']);
              isSubscribedThree =
                  int.parse(convertDataToJson['isSubscribedThree']);
              _progressBarActive = !_progressBarActive;
            });
          } catch (e) {
            print(e);
          }
        } else {
          print(response.statusCode);
        }
      });
    } catch (e) {
      print(e);
    }
  }

  _deleteQR() async {
    progressShow(context);

    var params = <String, String>{};
    params = {
      'qrId': qrId,
      'hash': Constant.KEY,
    };

    try {
      await HttpRequest()
          .fetchPost(Constant.delete_qr, params)
          .then((response) {
        progressHide(context);
        print(response.statusCode);
        if (response.statusCode == 200) {
          try {
            print(response.body);
            var convertDataToJson = json.jsonDecode(response.body);
            var result = convertDataToJson['result'];
            if (result == "Success") {
              Navigator.pop(context, true);
            }
          } catch (e) {
            showSnackBar(context, e.toString());
          }
        } else {
          showSnackBar(
              context, AppLocalizations.of(context).translate("Failed_String"));
        }
      });
    } catch (e) {
      progressHide(context);
      showSnackBar(context, e.toString());
    }
  }

  _openMap() async {
    List list = new List();
    progressShow(context);

    var params = <String, String>{};
    params = {
      'qrId': qrId,
      'hash': Constant.KEY,
    };

    try {
      await HttpRequest()
          .fetchPost(Constant.get_qr_scanned_location, params)
          .then((response) {
        progressHide(context);
        print(response.statusCode);
        if (response.statusCode == 200) {
          try {
            print(response.body);
            var convertDataToJson = json.jsonDecode(response.body);
            list = convertDataToJson['result'];
            print(list[0]["location"]);
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => ScanedQRMapResult(list)));
          } catch (e) {
            showSnackBar(context, e.toString());
          }
        } else {
          showSnackBar(
              context, AppLocalizations.of(context).translate("Failed_String"));
        }
      });
    } catch (e) {
      progressHide(context);
      showSnackBar(context, e.toString());
    }

    //   String url = null;
    // if (Platform.isAndroid)
    //  url= 'https://www.google.com/maps/search/?api=1&query='+ll;
    // if (Platform.isIOS)
    //   url = 'http://maps.apple.com/?ll='+ll;
    //   if (await canLaunch(url)) {
    //     await launch(url);
    //   } else {
    //     throw 'Could not launch $url';
    //   }
  }

  ScreenshotController screenshotController = ScreenshotController();
  File _imageFile;


  _takeScreenshotandShare() async {
    _imageFile = null;
    screenshotController
        .capture(delay: Duration(milliseconds: 10), pixelRatio: 2.0)
        .then((File image) async {
      setState(() {
        _imageFile = image;
      });
      final directory = (await getApplicationDocumentsDirectory()).path;
      Uint8List pngBytes = _imageFile.readAsBytesSync();
      File imgFile = new File('$directory/mydata.png');
      imgFile.writeAsBytes(pngBytes);
      print("File Saved to Gallery to: " + directory);
      await Share.file('maz', 'screenshot.png', pngBytes, 'image/png', text: '${Constant.qr_url}' + widget.qrId);
    }).catchError((onError) {
      print(onError);
    });
  }

  

  _requestPermission() async {
    Map<Permission, PermissionStatus> statuses = await [
      Permission.storage,
    ].request();

    final info = statuses[Permission.storage].toString();
    print(info);
  }

  saveImageInGallery() async{
    _imageFile = null;
    screenshotController
        .capture(delay: Duration(milliseconds: 10), pixelRatio: 2.0)
        .then((File image) async {
      setState(() {
        _imageFile = image;
      });
      final result =
                await ImageGallerySaver.saveImage(image.readAsBytesSync()); // Save image to gallery,  Needs plugin  https://pub.dev/packages/image_gallery_saver
            print("File Saved to Gallery");
            print(result.toString());
          Fluttertoast.showToast(
          msg: "QR Image Saved",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1
      );
      }).catchError((onError) {
      print(onError);
    });
  }

  Future<void> _takeScreenShot() async {
    print("i amhere");

    RenderRepaintBoundary boundary = previewContainer.currentContext
        .findRenderObject();
    ui.Image image = await boundary.toImage();
    ByteData byteData = await image.toByteData(format: ui.ImageByteFormat.png);
    Uint8List pngBytes = byteData.buffer.asUint8List();
    Directory d = Directory('/storage/emulated/0');

    if (d.existsSync()) {

      String dateFormatStr = dateFormat.format(DateTime.now());
      print(dateFormatStr);
      Directory(d.path + '/MyApp').createSync();
//      File imgFile = new File(d.path + dateFormatStr + '/MyApp/QR_Image.png');
      File imgFile = new File(d.path + '/MyApp/$dateFormatStr.png');
      print('saving to ${imgFile.path}');
      imgFile.createSync();
      imgFile.writeAsBytes(pngBytes);

      Fluttertoast.showToast(
          msg: "QR Image Saved",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIos: 1
      );
    } else {
      print("Some problem occurs");
    }
  }
}