import 'dart:convert';

import 'package:bond_flutter/Activity/SelectPackage.dart';
import 'package:bond_flutter/Localization/app_localizations.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:bond_flutter/Transitions/ScreenPageTransitions.dart';
import 'package:fluttertoast/fluttertoast.dart';
//import 'package:flutter_colorpicker/block_picker.dart';
import 'package:flutter_colorpicker/flutter_colorpicker.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../UI/CustomColors.dart';
import '../Model/DataModel.dart';
import 'QRPreviewAndLinkGenerateScreen.dart';
import 'dart:convert' as json;
import '../SessionManager/SessionManager.dart';
import '../Constant/Constant.dart';
import '../Http/HttpRequest.dart';
import '../UI/SnackBar.dart';
import '../UI/ProgressBar.dart';
import 'dart:async';
import 'dart:io' as Io;
import 'package:group_radio_button/group_radio_button.dart';
import 'package:image/image.dart' as img;

class GenerateQRScreen extends StatefulWidget {
  DataModel dataModel;
  bool isFromInvite;
  GenerateQRScreen(this.dataModel, this.isFromInvite, {Key key})
      : super(key: key); //add

  GenerateQRState createState() => GenerateQRState();
}

class GenerateQRState extends State<GenerateQRScreen> {
  Color selectedColor = Colors.black;
  Color selectedQrForegroundColor = Colors.blue.shade100;
  bool isImageSet = false;
  int isSubscribedZeroSession,
      isSubscribedOneSession,
      isSubscribedTwoSession,
      isSubscribedThreeSession;
  int isPremiumUser = 121;
//  List<Color> colors = [
//    Color(0xff000000),
//    Color(0xffb2102f),
//    Color(0xff2a3eb1),
//    Color(0xff00897b),
//    Color(0xff802c66),
//    Color(0xff595238),
//    Color(0xff162447),
//    Color(0xff184d47),
//  ];

  Io.File _image;
  Io.File _backgroundImg;

  int isSubscribedZero = 0,
      isSubscribedOne = 0,
      isSubscribedTwo = 0,
      isSubscribedThree = 0;
  List<String> _status = ["Color", "Image"];

  String _verticalGroupValue = "Color";
  // String stringValue;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // getDataFromCache();

    print(widget.dataModel.list.toString());

    fetchUserDetail();
  }
  // getDataFromCache() async {
  //   stringValue = await getValue();
  //   setState(() {
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        actions: <Widget>[
          FlatButton(
            textColor: Colors.white,
            onPressed: () async {
              if (!widget.isFromInvite) {
                var fetchUserId = await SessionManager().getUserId();
                String currentUserId = fetchUserId.toString();

                if (currentUserId == "0") {
                  _showDialog();
                  return;
                }

                if (selectedQrForegroundColor != null)
                  widget.dataModel.foregroundColor =
                      selectedQrForegroundColor.value;

                widget.dataModel.backgroundColor = selectedColor.value;

                var data = await generateQrFromModel();

                if (data == null) {
                  showSnackBar(context,
                      AppLocalizations.of(context).translate("Failed_String"));
                  return;
                } else {
                  Map _data = json.jsonDecode(data);
                  print(_data);

                  Navigator.push(
                    context,
                    SlideRightRoute(
                      page: QRPreviewAndLinkGenerateScreen(
                        widget.dataModel,
                        isPremiumUser,
                        _data,
                        _image,
                        _backgroundImg,
                      ),
                    ),
                  );
                }
              } else {
                print("else");
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SelectPackage(),
                  ),
                );

                // showDialog(context: context, builder: (dialogContext) => _openPopup2(dialogContext),);

              }
            },
            child:
                Text(AppLocalizations.of(context).translate("Generate_String")),
            shape: CircleBorder(side: BorderSide(color: Colors.transparent)),
          ),
        ],
        backgroundColor: Color(CustomColors().mainColorHard()),
      ),
      body: Builder(
        builder: (context) => Center(
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Material(
              child: Padding(
                padding: const EdgeInsets.only(top: 50.0),
                child: SingleChildScrollView(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: 400,
                    ),
                    child: Column(
                      children: <Widget>[
                        GestureDetector(
                          onTap: () => print(
                              "Selected background $selectedQrForegroundColor${widget.dataModel.list}  and selectforeground $selectedColor"),
                          child: Stack(
                            children: <Widget>[
                              Container(
                                height: 300,
                                width: 300,
                                child: FittedBox(
                                  child: _backgroundImg != null
                                      ? Image.asset(_backgroundImg.path)
                                      : Container(),
                                  fit: BoxFit.fill,
                                ),
                              ),
                              Container(
                                child: QrImage(
                                  gapless: false,
                                  version: QrVersions.auto,
//                                  embeddedImage: _image == null
//                                      ? AssetImage('images/logo_main.png')
//                                      : FileImage(_image),
                                  embeddedImageStyle: QrEmbeddedImageStyle(
                                    size: Size(40, 40),
                                  ),
                                  //  backgroundImage: selectedQrForegroundColor,
                                  backgroundColor: isImageSet
                                      ? Colors.transparent
                                      : selectedQrForegroundColor,
                                  // is the outer color
                                  foregroundColor: selectedColor,
                                  data: json.jsonEncode(widget.dataModel.list),
                                  size: 300.0,
//                              errorCorrectionLevel: QrErrorCorrectLevel.L,
                                ),
                              ),
                            ],
                          ),
                        ),
//                            Container(
//                              child: Material(
//                                borderRadius:
//                                BorderRadius.all(Radius.circular(10.0)),
//                                color: Color(CustomColors().mainColorHard()),
//                                child: Padding(
//                                  padding: const EdgeInsets.all(8.0),
//                                  child: Text(
//                                    AppLocalizations.of(context)
////                                    .translate("Background_Color_String"),
//                                        .translate("Body_Color_String"),
//                                    style: TextStyle(color: Colors.white),
//                                  ),
//                                ),
//                              ),
//                            ),
//                            Padding(
//                              padding: const EdgeInsets.all(8.0),
//                              child: Container(
//                                  child: FloatingActionButton(
//                                    onPressed: () {
//                                      if (widget.dataModel.name == 'text' ||
//                                          widget.dataModel.name == 'sms' ||
//                                          widget.dataModel.name == 'email' ||
//                                          widget.dataModel.name == 'website' ||
//                                          widget.dataModel.name == 'contact' ||
//                                          widget.dataModel.name == 'vcard' ||
//                                          widget.dataModel.name ==
//                                              'socialmedia' ||
//                                          widget.dataModel.name == 'invite') {
//                                        showDialog(
//                                          context: context,
//                                          builder: (BuildContext context) {
//                                            return AlertDialog(
//                                              titlePadding: const EdgeInsets
//                                                  .all(0.0),
//                                              contentPadding:
//                                              const EdgeInsets.all(20.0),
//                                              content: Container(
//                                                height: 180.0,
////                                        child: ColorPicker(
////                                          pickerColor: selectedColor,
////                                          onColorChanged: changeColor,
////                                          pickerAreaHeightPercent: 0.3,
////                                          enableAlpha: false,
////                                        ),
//                                                child: BlockPicker(
//                                                  pickerColor: selectedColor,
//                                                  onColorChanged: changeColor,
//                                                  availableColors: colors,
//                                                ),
//                                              ),
//                                            );
//                                          },
//                                        );
//                                      }
//                                    },
//                                    backgroundColor: selectedColor,
//                                  )),
//                            ),
                        isPremiumUser == 121
                            ? Container(
                                height: 40,
                              )
                            : isPremiumUser == 99
                                ? Container(
                                    child: Column(
                                      children: [
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 10.0),
                                          child: Container(
                                            height: 30,
                                            child: Material(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10.0)),
                                              color: Color(CustomColors()
                                                  .mainColorHard()),
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 17, right: 17),
                                                child: FlatButton(
                                                    child: Text(
                                                      AppLocalizations.of(
                                                              context)
                                                          .translate(
                                                              "Change_Image"),
                                                      style: TextStyle(
                                                          color: Colors.white),
                                                    ),
                                                    onPressed: () {
                                                      //  showDialog(context: context, builder: (dialogContext) => _openPopup2(dialogContext),);

                                                      Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                          builder: (context) =>
                                                              SelectPackage(),
                                                        ),
                                                      );
                                                    }),
                                              ),
                                            ),
                                          ),
                                        ),
                                        Padding(
                                          padding:
                                              const EdgeInsets.only(top: 10.0),
                                          child: Container(
                                            height: 30,
                                            child: Material(
                                              borderRadius: BorderRadius.all(
                                                  Radius.circular(10.0)),
                                              color: Color(CustomColors()
                                                  .mainColorHard()),
                                              child: Padding(
                                                padding: const EdgeInsets.only(
                                                    left: 17, right: 17),
                                                child: FlatButton(
                                                    child: Text(
                                                      "View Premier QR",
                                                      style: TextStyle(
                                                          color: Colors.white),
                                                    ),
                                                    onPressed: () {
                                                      showDialog(
                                                        context: context,
                                                        builder: (dialogContext) =>
                                                            _openPopup2(
                                                                dialogContext),
                                                      );
                                                      //
                                                      // Navigator.push(
                                                      //   context,
                                                      //   MaterialPageRoute(
                                                      //     builder: (context) => SelectPackage(),
                                                      //   ),
                                                      // );
                                                    }),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )
                                : Container(
                                    alignment: Alignment.center,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        RadioGroup<String>.builder(
                                          direction: Axis.horizontal,
                                          groupValue: _verticalGroupValue,
                                          onChanged: (value) => setState(() {
                                            _verticalGroupValue = value;
                                            if (_verticalGroupValue ==
                                                "Color") {
                                              _image = null;
                                            } else {
                                              widget.dataModel.foregroundColor =
                                                  null;
                                              setState(() {
                                                this.selectedQrForegroundColor =
                                                    Colors.white;
                                              });
                                            }
                                            print(value);
                                          }),
                                          items: _status,
                                          itemBuilder: (item) =>
                                              RadioButtonBuilder(
                                            item,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),

                        _verticalGroupValue == "Color"
                            ? Container(
                                child: Column(
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(top: 10.0),
                                      child: Container(
                                        child: Padding(
                                          padding: const EdgeInsets.only(
                                              left: 17, right: 17),
                                          child: Text(
                                            'Select Background Color',
                                            style: TextStyle(
                                              color: Colors.black,
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold,
                                              fontStyle: FontStyle.italic,
                                              decoration:
                                                  TextDecoration.underline,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(3.0),
                                      child: Container(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(4.0),
                                              child: InkWell(
                                                onTap: () {
                                                  if (!widget.isFromInvite) {
                                                    if (widget.dataModel.name ==
                                                            'text' ||
                                                        widget.dataModel.name ==
                                                            'sms' ||
                                                        widget.dataModel.name ==
                                                            'email' ||
                                                        widget.dataModel.name ==
                                                            'website' ||
                                                        widget.dataModel.name ==
                                                            'contact' ||
                                                        widget.dataModel.name ==
                                                            'vcard' ||
                                                        widget.dataModel.name ==
                                                            'socialmedia' ||
                                                        widget.dataModel.name ==
                                                            'invite') {
                                                      setState(() {
                                                        isImageSet = false;
                                                        _backgroundImg = null;
                                                        this.selectedQrForegroundColor =
                                                            Colors.red.shade100;
                                                      });
                                                    }
                                                  } else {
                                                    showSnackBar(
                                                        context,
                                                        AppLocalizations.of(
                                                                context)
                                                            .translate(
                                                                "Buy_Any_Plan"));
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            SelectPackage(),
                                                      ),
                                                    );
                                                  }
                                                },
                                                child: Material(
                                                  child: Container(
                                                    child: Image.asset(
                                                      'images/orignal_qr_icon.png',
                                                      width: 40,
                                                      height: 40,
                                                      color:
                                                          Colors.red.shade100,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(4.0),
                                              child: InkWell(
                                                onTap: () {
                                                  if (!widget.isFromInvite) {
                                                    if (widget.dataModel.name ==
                                                            'text' ||
                                                        widget.dataModel.name ==
                                                            'sms' ||
                                                        widget.dataModel.name ==
                                                            'email' ||
                                                        widget.dataModel.name ==
                                                            'website' ||
                                                        widget.dataModel.name ==
                                                            'contact' ||
                                                        widget.dataModel.name ==
                                                            'vcard' ||
                                                        widget.dataModel.name ==
                                                            'socialmedia' ||
                                                        widget.dataModel.name ==
                                                            'invite') {
                                                      setState(() {
                                                        _backgroundImg = null;
                                                        isImageSet = false;
                                                        this.selectedQrForegroundColor =
                                                            Colors.orange
                                                                .shade100;
                                                      });
                                                    }
                                                  } else {
                                                    showSnackBar(
                                                        context,
                                                        AppLocalizations.of(
                                                                context)
                                                            .translate(
                                                                "Buy_Any_Plan"));
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            SelectPackage(),
                                                      ),
                                                    );
                                                  }
                                                },
                                                child: Material(
                                                  child: Container(
                                                    child: Image.asset(
                                                      'images/orignal_qr_icon.png',
                                                      width: 40,
                                                      height: 40,
                                                      color: Colors
                                                          .orange.shade100,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(4.0),
                                              child: InkWell(
                                                onTap: () {
                                                  if (!widget.isFromInvite) {
                                                    if (widget.dataModel.name ==
                                                            'text' ||
                                                        widget.dataModel.name ==
                                                            'sms' ||
                                                        widget.dataModel.name ==
                                                            'email' ||
                                                        widget.dataModel.name ==
                                                            'website' ||
                                                        widget.dataModel.name ==
                                                            'contact' ||
                                                        widget.dataModel.name ==
                                                            'vcard' ||
                                                        widget.dataModel.name ==
                                                            'socialmedia' ||
                                                        widget.dataModel.name ==
                                                            'invite') {
                                                      setState(() {
                                                        _backgroundImg = null;
                                                        isImageSet = false;
                                                        this.selectedQrForegroundColor =
                                                            Colors.yellow
                                                                .shade100;
                                                      });
                                                    }
                                                  } else {
                                                    showSnackBar(
                                                        context,
                                                        AppLocalizations.of(
                                                                context)
                                                            .translate(
                                                                "Buy_Any_Plan"));
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            SelectPackage(),
                                                      ),
                                                    );
                                                  }
                                                },
                                                child: Material(
                                                  child: Container(
                                                    child: Image.asset(
                                                      'images/orignal_qr_icon.png',
                                                      width: 40,
                                                      height: 40,
                                                      color: Colors
                                                          .yellow.shade100,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(4.0),
                                              child: InkWell(
                                                onTap: () {
                                                  if (!widget.isFromInvite) {
                                                    if (widget.dataModel.name ==
                                                            'text' ||
                                                        widget.dataModel.name ==
                                                            'sms' ||
                                                        widget.dataModel.name ==
                                                            'email' ||
                                                        widget.dataModel.name ==
                                                            'website' ||
                                                        widget.dataModel.name ==
                                                            'contact' ||
                                                        widget.dataModel.name ==
                                                            'vcard' ||
                                                        widget.dataModel.name ==
                                                            'socialmedia' ||
                                                        widget.dataModel.name ==
                                                            'invite') {
                                                      setState(() {
                                                        _backgroundImg = null;
                                                        isImageSet = false;
                                                        this.selectedQrForegroundColor =
                                                            Colors
                                                                .blue.shade100;
                                                      });
                                                    }
                                                  } else {
                                                    showSnackBar(
                                                        context,
                                                        AppLocalizations.of(
                                                                context)
                                                            .translate(
                                                                "Buy_Any_Plan"));
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            SelectPackage(),
                                                      ),
                                                    );
                                                  }
                                                },
                                                child: Material(
                                                  child: Container(
                                                    child: Image.asset(
                                                      'images/orignal_qr_icon.png',
                                                      width: 40,
                                                      height: 40,
                                                      color:
                                                          Colors.blue.shade100,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.all(0.0),
                                      child: Container(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.center,
                                          children: <Widget>[
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(4.0),
                                              child: InkWell(
                                                onTap: () {
                                                  if (!widget.isFromInvite) {
                                                    if (widget.dataModel.name ==
                                                            'text' ||
                                                        widget.dataModel.name ==
                                                            'sms' ||
                                                        widget.dataModel.name ==
                                                            'email' ||
                                                        widget.dataModel.name ==
                                                            'website' ||
                                                        widget.dataModel.name ==
                                                            'contact' ||
                                                        widget.dataModel.name ==
                                                            'vcard' ||
                                                        widget.dataModel.name ==
                                                            'socialmedia' ||
                                                        widget.dataModel.name ==
                                                            'invite') {
                                                      setState(() {
                                                        _backgroundImg = null;
                                                        isImageSet = false;
                                                        this.selectedQrForegroundColor =
                                                            Colors
                                                                .green.shade100;
                                                      });
                                                    }
                                                  } else {
                                                    showDialog(
                                                      context: context,
                                                      builder: (dialogContext) =>
                                                          _openPopup2(
                                                              dialogContext),
                                                    );

                                                    showSnackBar(
                                                        context,
                                                        AppLocalizations.of(
                                                                context)
                                                            .translate(
                                                                "Buy_Any_Plan"));
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            SelectPackage(),
                                                      ),
                                                    );
                                                  }
                                                },
                                                child: Material(
                                                  child: Container(
                                                    child: Image.asset(
                                                      'images/orignal_qr_icon.png',
                                                      width: 40,
                                                      height: 40,
                                                      color:
                                                          Colors.green.shade100,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(4.0),
                                              child: InkWell(
                                                onTap: () {
                                                  if (!widget.isFromInvite) {
                                                    if (widget.dataModel.name ==
                                                            'text' ||
                                                        widget.dataModel.name ==
                                                            'sms' ||
                                                        widget.dataModel.name ==
                                                            'email' ||
                                                        widget.dataModel.name ==
                                                            'website' ||
                                                        widget.dataModel.name ==
                                                            'contact' ||
                                                        widget.dataModel.name ==
                                                            'vcard' ||
                                                        widget.dataModel.name ==
                                                            'socialmedia' ||
                                                        widget.dataModel.name ==
                                                            'invite') {
                                                      setState(() {
                                                        _backgroundImg = null;
                                                        isImageSet = false;
                                                        this.selectedQrForegroundColor =
                                                            Colors.purple
                                                                .shade100;
                                                      });
                                                    }
                                                  } else {
                                                    showSnackBar(
                                                        context,
                                                        AppLocalizations.of(
                                                                context)
                                                            .translate(
                                                                "Buy_Any_Plan"));
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            SelectPackage(),
                                                      ),
                                                    );
                                                  }
                                                },
                                                child: Material(
                                                  child: Container(
                                                    child: Image.asset(
                                                      'images/orignal_qr_icon.png',
                                                      width: 40,
                                                      height: 40,
                                                      color: Colors
                                                          .purple.shade100,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(4.0),
                                              child: InkWell(
                                                onTap: () {
                                                  if (!widget.isFromInvite) {
                                                    if (widget.dataModel.name ==
                                                            'text' ||
                                                        widget.dataModel.name ==
                                                            'sms' ||
                                                        widget.dataModel.name ==
                                                            'email' ||
                                                        widget.dataModel.name ==
                                                            'website' ||
                                                        widget.dataModel.name ==
                                                            'contact' ||
                                                        widget.dataModel.name ==
                                                            'vcard' ||
                                                        widget.dataModel.name ==
                                                            'socialmedia' ||
                                                        widget.dataModel.name ==
                                                            'invite') {
                                                      setState(() {
                                                        _backgroundImg = null;
                                                        isImageSet = false;
                                                        this.selectedQrForegroundColor =
                                                            Colors.grey;
                                                      });
                                                    }
                                                  } else {
                                                    showSnackBar(
                                                        context,
                                                        AppLocalizations.of(
                                                                context)
                                                            .translate(
                                                                "Buy_Any_Plan"));
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            SelectPackage(),
                                                      ),
                                                    );
                                                  }
                                                },
                                                child: Material(
                                                  child: Container(
                                                    child: Image.asset(
                                                      'images/orignal_qr_icon.png',
                                                      width: 40,
                                                      height: 40,
                                                      color: Colors.grey,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(4.0),
                                              child: InkWell(
                                                onTap: () {
                                                  if (!widget.isFromInvite) {
                                                    if (widget.dataModel.name ==
                                                            'text' ||
                                                        widget.dataModel.name ==
                                                            'sms' ||
                                                        widget.dataModel.name ==
                                                            'email' ||
                                                        widget.dataModel.name ==
                                                            'website' ||
                                                        widget.dataModel.name ==
                                                            'contact' ||
                                                        widget.dataModel.name ==
                                                            'vcard' ||
                                                        widget.dataModel.name ==
                                                            'socialmedia' ||
                                                        widget.dataModel.name ==
                                                            'invite') {
                                                      setState(() {
                                                        _backgroundImg = null;
                                                        isImageSet = false;
                                                        this.selectedQrForegroundColor =
                                                            Colors
                                                                .brown.shade100;
                                                      });
                                                    }
                                                  } else {
                                                    showSnackBar(
                                                        context,
                                                        AppLocalizations.of(
                                                                context)
                                                            .translate(
                                                                "Buy_Any_Plan"));
                                                    Navigator.push(
                                                      context,
                                                      MaterialPageRoute(
                                                        builder: (context) =>
                                                            SelectPackage(),
                                                      ),
                                                    );
                                                  }
                                                },
                                                child: Material(
                                                  child: Container(
                                                    child: Image.asset(
                                                      'images/orignal_qr_icon.png',
                                                      width: 40,
                                                      height: 40,
                                                      color:
                                                          Colors.brown.shade100,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            : Container(
                                child: Center(
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      isPremiumUser == 0 ||
                                              isPremiumUser == 1 ||
                                              isPremiumUser == 2 ||
                                              isPremiumUser == 3
                                          ? Container(
                                              child: Column(
                                                children: [
                                                  RaisedButton(
                                                    child: Text(AppLocalizations
                                                            .of(context)
                                                        .translate(
                                                            "Change_Image")),
                                                    onPressed: () {
                                                      if (isSubscribedZero == 1 &&
                                                          (widget.dataModel.name ==
                                                                  'text' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'sms' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'email' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'website' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'contact')) {
                                                        getImage();
                                                      } else if (isSubscribedOne == 1 &&
                                                          (widget.dataModel.name ==
                                                                  'text' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'sms' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'email' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'website' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'contact' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'vcard')) {
                                                        getImage();
                                                      } else if (isSubscribedTwo == 1 &&
                                                          (widget.dataModel.name == 'text' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'sms' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'email' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'website' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'contact' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'vcard' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'socialmedia')) {
                                                        getImage();
                                                      } else if (isSubscribedThree == 1 &&
                                                          (widget.dataModel.name == 'text' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'sms' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'email' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'website' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'contact' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'vcard' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'socialmedia' ||
                                                              widget.dataModel
                                                                      .name ==
                                                                  'invite')) {
                                                        getImage();
                                                      } else {
                                                        showSnackBar(
                                                            context,
                                                            AppLocalizations.of(
                                                                    context)
                                                                .translate(
                                                                    "Buy_Any_Plan"));
                                                        Navigator.push(
                                                          context,
                                                          MaterialPageRoute(
                                                            builder: (context) =>
                                                                SelectPackage(),
                                                          ),
                                                        );
                                                      }
                                                      // getImage();
                                                    },
                                                    color: Color(
                                                      CustomColors()
                                                          .mainColorHard(),
                                                    ),
                                                    textColor: Colors.white,
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            10, 10, 10, 10),
                                                    splashColor: Colors.grey,
                                                  ),
                                                  Padding(
                                                    padding:
                                                        const EdgeInsets.all(
                                                            20),

                                                    child: _image != null
                                                        ? Image.file(
                                                            _image,
                                                            height: 150,
                                                            width: 150,
                                                          )
                                                        : Container(
                                                            height: 250),

                                                    // child: _image == null ? Image(image:  AssetImage('images/popup_image1.png'))  : Image.file(File(_image.path))
                                                  ),
                                                ],
                                              ),
                                            )
                                          : Container(
                                              // child:
                                              // RaisedButton(
                                              //     child: Text(AppLocalizations.of(context)
                                              //         .translate("Show_demo")),
                                              //     onPressed: (){
                                              //       showDialog(context: context, builder: (dialogContext) => _openPopup2(dialogContext),);
                                              //
                                              //     }),
                                              //

                                              ),
                                    ],
                                  ),
                                ),
                              ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return null;
  }

  void fetchUserDetail() async {
    var fetchUserId = await SessionManager().getUserId();
    if (fetchUserId == 0) {
      setState(() {
        isSubscribedZero = 0;
        isSubscribedOne = 0;
        isSubscribedTwo = 0;
        isSubscribedThree = 0;
      });
    } else {
      try {
        var fetchUserId = await SessionManager().getUserId();
        var userId = fetchUserId.toString();
        var params = {'hash': Constant.KEY, 'userId': userId};
        await HttpRequest()
            .fetchPost(Constant.get_user_detail, params)
            .then((response) {
          if (response.statusCode == 200) {
            try {
              print(response.body);
              setState(() {
                var convertDataToJson = jsonDecode(response.body);
                isSubscribedZero =
                    int.parse(convertDataToJson['isSubscribedZero']);
                isSubscribedOne =
                    int.parse(convertDataToJson['isSubscribedOne']);
                isSubscribedTwo =
                    int.parse(convertDataToJson['isSubscribedTwo']);
                isSubscribedThree =
                    int.parse(convertDataToJson['isSubscribedThree']);

                if (isSubscribedZero == 1) {
                  SessionManager().setIsSubscribedZero(isSubscribedZero);
                  // isPremiumUser = 0;
                }
                if (isSubscribedOne == 1) {
                  SessionManager().setIsSubscribedOne(isSubscribedOne);
                  // isPremiumUser = 1;
                }
                if (isSubscribedTwo == 1) {
                  SessionManager().setIsSubscribedTwo(isSubscribedTwo);
                  // isPremiumUser = 2;
                }
                if (isSubscribedThree == 1) {
                  SessionManager().setIsSubscribedThree(isSubscribedThree);
                  // isPremiumUser = 3;
                }

                if (isSubscribedZero == 0 &&
                    isSubscribedOne == 0 &&
                    isSubscribedTwo == 0 &&
                    isSubscribedThree == 0) {
                  isPremiumUser = 99;
                }
                //
                if (isSubscribedZero == 1 ||
                    isSubscribedOne == 1 ||
                    isSubscribedTwo == 1 ||
                    isSubscribedThree == 1)
                  setState(() {
                    widget.isFromInvite = false;
                  });

                setIsPremiumValue(widget.dataModel.name);

                // if ((isSubscribedZero == 1) &&
                // (widget.dataModel.name == 'vcard') ||
                // (widget.dataModel.name == 'socialmedia') ||
                // (widget.dataModel.name == 'invite')){
                //   isPremiumUser = 99;
                // } else if ((isSubscribedOne == 1) &&
                // (widget.dataModel.name == 'socialmedia') ||
                // (widget.dataModel.name == 'invite')){
                //   isPremiumUser = 99;
                // } else if ((isSubscribedTwo == 1) &&
                // (widget.dataModel.name == 'invite')){
                //   isPremiumUser = 99;
                // }

                var name = widget.dataModel.name;

                print("iePremium $isPremiumUser $name");
              });
            } catch (e) {
              print(e);
            }
          } else {
            print(response.statusCode);
          }
        });
      } catch (e) {
        print(e);
      }
    }
  }

  setIsPremiumValue(String name) {
    if ((name == "text") ||
        (name == "sms") ||
        (name == "email") ||
        (name == "website") ||
        (name == "contact")) {
      if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 0;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 0) &&
          (isSubscribedTwo == 0) &&
          (isSubscribedThree == 0)) {
        isPremiumUser = 0;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 0) &&
          (isSubscribedTwo == 0) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 0;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 0) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 0)) {
        isPremiumUser = 0;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 0) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 0;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 0) &&
          (isSubscribedThree == 0)) {
        isPremiumUser = 0;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 0) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 0;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 0)) {
        isPremiumUser = 0;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 0)) {
        isPremiumUser = 0;
      }
      // else if ((isSubscribedZero == 0) && (isSubscribedOne == 1) && (isSubscribedTwo == 0) && (isSubscribedThree == 0)) {
      //   isPremiumUser = 1;
      // } else if ((isSubscribedZero == 0) && (isSubscribedOne == 1) && (isSubscribedTwo == 0) && (isSubscribedThree == 1)){
      //   isPremiumUser = 1;
      // } else if ((isSubscribedZero == 0) && (isSubscribedOne == 1) && (isSubscribedTwo == 1) && (isSubscribedThree == 0)){
      //   isPremiumUser = 1;
      // } else if ((isSubscribedZero == 0) && (isSubscribedOne == 1) && (isSubscribedTwo == 1) && (isSubscribedThree == 1)){
      //   isPremiumUser = 1;
      // } else if ((isSubscribedZero == 0) && (isSubscribedOne == 0) && (isSubscribedTwo == 1) && (isSubscribedThree == 0)){
      //   isPremiumUser = 2;
      // } else if ((isSubscribedZero == 0) && (isSubscribedOne == 0) && (isSubscribedTwo == 1) && (isSubscribedThree == 1)){
      //   isPremiumUser = 2;
      // } else if ((isSubscribedZero == 0) && (isSubscribedOne == 0) && (isSubscribedTwo == 0) && (isSubscribedThree == 1)) {
      //   isPremiumUser = 3;
      // }
      else {
        isPremiumUser = 99;
      }
    } else if (name == "vcard") {
      if ((isSubscribedZero == 0) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 0) &&
          (isSubscribedThree == 0)) {
        isPremiumUser = 1;
      } else if ((isSubscribedZero == 0) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 0) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 1;
      } else if ((isSubscribedZero == 0) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 0)) {
        isPremiumUser = 1;
      } else if ((isSubscribedZero == 0) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 1;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 0) &&
          (isSubscribedThree == 0)) {
        isPremiumUser = 1;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 0) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 1;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 0)) {
        isPremiumUser = 1;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 1;
      }
      // else if((isSubscribedZero == 0) && (isSubscribedOne == 0) && (isSubscribedTwo == 1) && (isSubscribedThree == 0)){
      //   isPremiumUser = 2;
      // } else if((isSubscribedZero == 0) && (isSubscribedOne == 0) && (isSubscribedTwo == 1) && (isSubscribedThree == 1)){
      //   isPremiumUser = 2;
      // } else if((isSubscribedZero == 1) && (isSubscribedOne == 0) && (isSubscribedTwo == 1) && (isSubscribedThree == 0)){
      //   isPremiumUser = 2;
      // } else if ((isSubscribedZero == 1) && (isSubscribedOne == 0) && (isSubscribedTwo == 1) && (isSubscribedThree == 1)){
      //   isPremiumUser = 2;
      // } else if((isSubscribedZero == 0) && (isSubscribedOne == 0) && (isSubscribedTwo == 0) && (isSubscribedThree == 1)){
      //   isPremiumUser = 3;
      // } else if((isSubscribedZero == 1) && (isSubscribedOne == 0) && (isSubscribedTwo == 0) && (isSubscribedThree == 1)){
      //   isPremiumUser = 3;
      // }
      else {
        isPremiumUser = 99;
      }
    } else if (name == "socialmedia") {
      if ((isSubscribedZero == 0) &&
          (isSubscribedOne == 0) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 0)) {
        isPremiumUser = 2;
      } else if ((isSubscribedZero == 0) &&
          (isSubscribedOne == 0) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 2;
      } else if ((isSubscribedZero == 0) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 0)) {
        isPremiumUser = 2;
      } else if ((isSubscribedZero == 0) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 2;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 0) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 0)) {
        isPremiumUser = 2;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 0) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 2;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 0)) {
        isPremiumUser = 2;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 2;
      }
      // else if((isSubscribedZero == 0) && (isSubscribedOne == 0) && (isSubscribedTwo == 0) && (isSubscribedThree == 1)){
      //   isPremiumUser = 3;
      // } else if((isSubscribedZero == 0) && (isSubscribedOne == 1) && (isSubscribedTwo == 0) && (isSubscribedThree == 1)){
      //   isPremiumUser = 3;
      // } else if((isSubscribedZero == 1) && (isSubscribedOne == 0) && (isSubscribedTwo == 0) && (isSubscribedThree == 1)){
      //   isPremiumUser = 3;
      // } else if((isSubscribedZero == 1) && (isSubscribedOne == 1) && (isSubscribedTwo == 0) && (isSubscribedThree == 1)){
      //   isPremiumUser = 3;
      // }
      else {
        isPremiumUser = 99;
      }
    } else if (name == "invite") {
      if ((isSubscribedZero == 0) &&
          (isSubscribedOne == 0) &&
          (isSubscribedTwo == 0) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 3;
      } else if ((isSubscribedZero == 0) &&
          (isSubscribedOne == 0) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 3;
      } else if ((isSubscribedZero == 0) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 0) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 3;
      } else if ((isSubscribedZero == 0) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 3;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 0) &&
          (isSubscribedTwo == 0) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 3;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 0) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 3;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 0) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 3;
      } else if ((isSubscribedZero == 1) &&
          (isSubscribedOne == 1) &&
          (isSubscribedTwo == 1) &&
          (isSubscribedThree == 1)) {
        isPremiumUser = 3;
      } else {
        isPremiumUser = 99;
      }
    } else {
      isPremiumUser = 99;
    }
  }

  Future<String> generateQrFromModel() async {
    String data;
    var params = <String, String>{};
    var imageURl = "";
    var backImageURL = "";
    var bgColor = "";

    var payLoad = json.jsonEncode(widget.dataModel.list);
    var fetchUserId = await SessionManager().getUserId();

    progressShow(context);

    if (isPremiumUser != 99) {
      String valueString = selectedQrForegroundColor
          .toString()
          .split('Color(0xff')[1]
          .split(')')[0];
      bgColor = valueString;
    }

    if (_image != null) {
      print("A");
      // Read a jpeg image from file.
      img.Image image =
          img.decodeImage(new Io.File(_image.path).readAsBytesSync());
      print("B");
      // Resize the image to a 150x? thumbnail (maintaining the aspect ratio).
      img.Image thumbnail = img.copyResize(image, width: 150, height: 150);
      print("C");
      // Save the thumbnail as a PNG.
      var thumb = new Io.File(_image.path)
        ..writeAsBytesSync(img.encodePng(thumbnail));
      print("D");
      Io.File file = new Io.File(thumb.path.toString());
      String fileName = file.path.split('/').last;
      print("$fileName");
      StorageReference firebaseStorageRef =
          FirebaseStorage.instance.ref().child(fileName);
      StorageUploadTask uploadTask = firebaseStorageRef.putFile(_image);

      // uploadTask.onComplete.then((value) async {
      //   try {
      //     var imageUrl = await firebaseStorageRef.getDownloadURL();
      //     print(imageURl);
      //   } catch (onError) {
      //     print("Error: ${onError.toString()}");
      //   }
      // });
      StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;
      String url = (await taskSnapshot.ref.getDownloadURL());
      setState(() {
        imageURl = url.toString();
      });
    }

    if (_backgroundImg != null) {
      Io.File file = new Io.File(_backgroundImg.path.toString());
      String fileName = file.path.split('/').last;

      print("asd" + file.path.split('/').last);

      StorageReference firebaseStorageRef =
          FirebaseStorage.instance.ref().child(fileName);
      StorageUploadTask uploadTask = firebaseStorageRef.putFile(_backgroundImg);
      StorageTaskSnapshot taskSnapshot = await uploadTask.onComplete;

      String url = (await taskSnapshot.ref.getDownloadURL());

      setState(() {
        backImageURL = url.toString();
      });
    }

    params = {
      'userId': fetchUserId.toString(),
      'qrTypeId': widget.dataModel.id.toString(),
      'hash': Constant.KEY,
      'name': widget.dataModel.name,
      'backgroundColor': (isPremiumUser != 99)
          ? bgColor
          : widget.dataModel.backgroundColor.toString(),
      'foregroundColor': widget.dataModel.foregroundColor.toString(),
      'list': payLoad,
      'bg_image': backImageURL,
      'back_image': imageURl,
      'isPremiumUser': isPremiumUser.toString(),
    };

    print(params);

    try {
      await HttpRequest()
          .fetchPost(Constant.generate_qr_link, params)
          .then((response) {
        progressHide(context);
        print(response.statusCode);
        if (response.statusCode == 200) {
          try {
            print(response.body);
            data = response.body;
          } catch (e) {
            showSnackBar(context, e.toString());
          }
        } else {
          showSnackBar(
              context, AppLocalizations.of(context).translate("Failed_String"));
        }
      });
    } catch (e) {
      progressHide(context);
      showSnackBar(context, e.toString());
    }

    return data;
  }

//  void changeColor(Color color) {
//    setState(() => selectedColor = color);
//  }

  Future getImage() async {
    await ImagePicker.pickImage(source: ImageSource.gallery).then((image) {
      setState(() {
        print(image);
        _image = image;
      });
    });

    // var image = await ImagePicker.pickImage(
    //     source: ImageSource.gallery, maxHeight: 150.0, maxWidth: 150.0);

    // setState(() {
    //   print(image);
    //   _image = image; //image;
    // });
  }

  Future getBackgroundImage() async {
    var backImage = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      isImageSet = true;
      selectedQrForegroundColor = null;
      _backgroundImg = backImage;
    });
  }

  _openPopup2(BuildContext context) {
    return AlertDialog(
      backgroundColor: Colors.transparent,
      contentPadding: EdgeInsets.all(0.0),
      content: Container(
        height: 450.0,
        width: 400.0,
        decoration: BoxDecoration(
            color: Colors.white,
            shape: BoxShape.rectangle,
            borderRadius: BorderRadius.circular(20.0)),
        child: Column(
          children: <Widget>[
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                height: 30,
                child: Align(
                  alignment: Alignment.topRight,
                  child: IconButton(
                    icon: Icon(
                      Icons.clear,
                      size: 22.0,
                    ),
                  ),
                ),
              ),
            ),
            Align(
                alignment: Alignment.topCenter,
                child: Text(
                  "Design your QR Code",
                  style: TextStyle(
                      color: Colors.deepPurple,
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold),
                )),
            Text(
              "And Get all Traffic reports",
              style: TextStyle(color: Colors.deepPurple, fontSize: 16.0),
            ),
            Text(
              "and statistics",
              style: TextStyle(color: Colors.deepPurple, fontSize: 16.0),
            ),
            GestureDetector(
              onTap: () {
                Fluttertoast.showToast(
                    msg: AppLocalizations.of(context)
                        .translate("File_Not_Image_String"));
              },
              child: Container(
                height: 250,
                width: 250,
                child: Image(
                  image: AssetImage('images/popup_image1.png'),
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => SelectPackage(),
                  ),
                );
              },
              child: Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: Image(
                  height: 70,
                  width: 240,
                  image: AssetImage('images/popUpButton.png'),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  _openPopup(context) {
    Alert(
        context: context,
        title: "",
        content: Column(
          children: <Widget>[
            Text(
              "Design Your Qr Code",
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Image(
              height: 200,
              width: 200,
              image: AssetImage('images/popup_image.png'),
            ),
          ],
        ),
        buttons: [
          DialogButton(
            onPressed: () => Navigator.pop(context),
            child: Text(
              "LOGIN",
              style: TextStyle(color: Colors.white, fontSize: 20),
            ),
          )
        ]).show();
  }

// user defined function
  void _showDialog() {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: new Text(
              AppLocalizations.of(context).translate("Attention_String")),
          content: new Text(AppLocalizations.of(context)
              .translate("Login_To_Continue_String")),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            new FlatButton(
              child: new Text(
                  AppLocalizations.of(context).translate("Close_String")),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  void getSubscribedData() async {
    progressShow(context);
    try {
      var fetchUserId = await SessionManager().getUserId();
      var userId = fetchUserId.toString();
      var params = {'hash': Constant.KEY, 'userId': userId};
      await HttpRequest()
          .fetchPost(Constant.get_user_detail, params)
          .then((response) {
        if (response.statusCode == 200) {
          try {
            print(response.body);
            setState(() async {
              var convertDataToJson = jsonDecode(response.body);
              int isSubscribedZero,
                  isSubscribedTwo,
                  isSubscribedOne,
                  isSubscribedThree;

              isSubscribedZero =
                  int.parse(convertDataToJson['isSubscribedZero']);
              isSubscribedOne = int.parse(convertDataToJson['isSubscribedOne']);
              isSubscribedTwo = int.parse(convertDataToJson['isSubscribedTwo']);
              isSubscribedThree =
                  int.parse(convertDataToJson['isSubscribedThree']);

              isSubscribedZeroSession = isSubscribedZero;
              isSubscribedOneSession = isSubscribedOne;
              isSubscribedTwoSession = isSubscribedTwo;
              isSubscribedThreeSession = isSubscribedThree;

              if (isSubscribedZero == 1) {
                SessionManager().setIsSubscribedZero(isSubscribedZero);
                isPremiumUser = 0;
              }
              if (isSubscribedOne == 1) {
                SessionManager().setIsSubscribedOne(isSubscribedOne);
                isPremiumUser = 1;
              }
              if (isSubscribedTwo == 1) {
                SessionManager().setIsSubscribedTwo(isSubscribedTwo);
                isPremiumUser = 2;
              }
              if (isSubscribedThree == 1) {
                SessionManager().setIsSubscribedThree(isSubscribedThree);
                isPremiumUser = 3;
              }

              setState(() {});
              progressHide(context);
            });
          } catch (e) {
            print(e);
            progressHide(context);
          }
        } else {
          print(response.statusCode);
          progressHide(context);
        }
      });
    } catch (e) {
      print(e);
      progressHide(context);
    }
  }

  void showDialogPremium() {
    showGeneralDialog(
      barrierLabel: "Barrier",
      barrierDismissible: true,
      barrierColor: Colors.black.withOpacity(0.5),
      transitionDuration: Duration(milliseconds: 700),
      context: context,
      pageBuilder: (_, __, ___) {
        return Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            height: 250,
            width: 250,
            child: FittedBox(
              child: _image != null ? Image.asset(_image.path) : Container(),
              fit: BoxFit.fill,
            ),
          ),
        );
      },
      transitionBuilder: (_, anim, __, child) {
        return SlideTransition(
          position: Tween(begin: Offset(0, 1), end: Offset(0, 0)).animate(anim),
          child: child,
        );
      },
    );
  }

  Future<String> getValue() async {
    String myVal;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    //Return String
    myVal = prefs.getString('key');
    return myVal;
  }
}
